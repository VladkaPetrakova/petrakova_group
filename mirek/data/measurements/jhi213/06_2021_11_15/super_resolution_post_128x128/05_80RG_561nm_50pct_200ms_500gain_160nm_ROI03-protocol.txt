{
  "version": "ThunderSTORM (dev-2016-09-10-b1)",
  "imageInfo": {
    "title": "05_80RG_561nm_50pct_200ms_500gain_160nm_ROI03.tiff"
  },
  "cameraSettings": {
    "readoutNoise": 0.0,
    "offset": 65.0,
    "quantumEfficiency": 0.95,
    "isEmGain": true,
    "photons2ADU": 12.05,
    "pixelSize": 160.0,
    "gain": 500.0
  },
  "analysisFilter": {
    "name": "Wavelet filter (B-Spline)",
    "scale": 2.0,
    "order": 3
  },
  "analysisDetector": {
    "name": "Local maximum",
    "connectivity": 8,
    "threshold": "std(Wave.F1)"
  },
  "analysisEstimator": {
    "name": "PSF: Integrated Gaussian",
    "fittingRadius": 3,
    "method": "Weighted Least squares",
    "initialSigma": 1.5,
    "fullImageFitting": false,
    "crowdedField": {
      "name": "Multi-emitter fitting analysis",
      "mfaEnabled": false,
      "nMax": 0,
      "pValue": 0.0,
      "keepSameIntensity": false,
      "intensityInRange": false
    }
  },
  "postProcessing": [
    {
      "name": "Filter",
      "options": "formula=[(sigma > 30) & (sigma < 200)]"
    },
    {
      "name": "Drift correction",
      "options": "path=c:\\\\Users\\\\miros\\\\home\\\\work\\\\jhi\\\\projects\\\\plasmonic_microscopy\\\\data\\\\measurements\\\\jhi213\\\\06_2021_11_15\\\\super_resolution_post_128x128\\\\05_80RG_561nm_50pct_200ms_500gain_160nm_ROI03.json magnification=4.0 method=[Cross correlation] ccsmoothingbandwidth=0.5 save=true steps=8 showcorrelations=false"
    }
  ],
  "is3d": false,
  "isSet3d": true
}