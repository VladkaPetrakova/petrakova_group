{
  "version": "ThunderSTORM (dev-2016-09-10-b1)",
  "imageInfo": {
    "title": "04_80RG_640nm_100ms.tiff"
  },
  "cameraSettings": {
    "readoutNoise": 0.0,
    "offset": 65.0,
    "quantumEfficiency": 0.95,
    "isEmGain": true,
    "photons2ADU": 12.05,
    "pixelSize": 100.0,
    "gain": 300.0
  },
  "analysisFilter": {
    "name": "Wavelet filter (B-Spline)",
    "scale": 2.0,
    "order": 3
  },
  "analysisDetector": {
    "name": "Local maximum",
    "connectivity": 8,
    "threshold": "std(Wave.F1)"
  },
  "analysisEstimator": {
    "name": "PSF: Integrated Gaussian",
    "fittingRadius": 3,
    "method": "Weighted Least squares",
    "initialSigma": 1.5,
    "fullImageFitting": false,
    "crowdedField": {
      "name": "Multi-emitter fitting analysis",
      "mfaEnabled": false,
      "nMax": 0,
      "pValue": 0.0,
      "keepSameIntensity": false,
      "intensityInRange": false
    }
  },
  "postProcessing": [],
  "is3d": false,
  "isSet3d": true
}