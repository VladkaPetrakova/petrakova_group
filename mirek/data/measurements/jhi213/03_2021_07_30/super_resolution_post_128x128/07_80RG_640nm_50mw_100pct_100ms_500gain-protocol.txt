{
  "version": "ThunderSTORM (dev-2016-09-10-b1)",
  "imageInfo": {
    "title": "07_80RG_640nm_50mw_100pct_100ms_500gain_100nm.tiff"
  },
  "cameraSettings": {
    "readoutNoise": 0.0,
    "offset": 65.0,
    "quantumEfficiency": 0.95,
    "isEmGain": true,
    "photons2ADU": 12.05,
    "pixelSize": 100.0,
    "gain": 500.0
  },
  "analysisFilter": {
    "name": "Wavelet filter (B-Spline)",
    "scale": 2.0,
    "order": 3
  },
  "analysisDetector": {
    "name": "Local maximum",
    "connectivity": 8,
    "threshold": "std(Wave.F1)"
  },
  "analysisEstimator": {
    "name": "PSF: Integrated Gaussian",
    "fittingRadius": 3,
    "method": "Weighted Least squares",
    "initialSigma": 1.5,
    "fullImageFitting": false,
    "crowdedField": {
      "name": "Multi-emitter fitting analysis",
      "mfaEnabled": false,
      "nMax": 0,
      "pValue": 0.0,
      "keepSameIntensity": false,
      "intensityInRange": false
    }
  },
  "postProcessing": [
    {
      "name": "Filter",
      "options": "formula=[(sigma < 170)]"
    },
    {
      "name": "Drift correction",
      "options": "path=c:\\\\Users\\\\miros\\\\home\\\\work\\\\jhi\\\\projects\\\\plasmonic_microscopy\\\\data\\\\measurements\\\\jhi\\\\2021_07_30\\\\super_resolution_post_128x128\\\\07_80RG_640nm_50mw_100pct_100ms_500gain_100nm.json magnification=4.0 method=[Cross correlation] ccsmoothingbandwidth=0.25 save=true steps=8 showcorrelations=false"
    },
    {
      "name": "Merging",
      "options": "zcoordweight=0.1 offframes=1 dist=20.0 framespermolecule=0"
    }
  ],
  "is3d": false,
  "isSet3d": true
}