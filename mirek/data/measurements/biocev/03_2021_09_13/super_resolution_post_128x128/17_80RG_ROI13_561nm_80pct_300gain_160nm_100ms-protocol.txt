{
  "version": "ThunderSTORM (dev-2016-09-10-b1)",
  "imageInfo": {
    "title": "17_80RG_ROI13_561nm_80pct_300gain_160nm_100ms.tiff"
  },
  "cameraSettings": {
    "readoutNoise": 0.0,
    "offset": 100.0,
    "quantumEfficiency": 0.95,
    "isEmGain": true,
    "photons2ADU": 62.5,
    "pixelSize": 160.9586,
    "gain": 300.0
  },
  "analysisFilter": {
    "name": "Wavelet filter (B-Spline)",
    "scale": 2.0,
    "order": 3
  },
  "analysisDetector": {
    "name": "Local maximum",
    "connectivity": 8,
    "threshold": "std(Wave.F1)"
  },
  "analysisEstimator": {
    "name": "PSF: Integrated Gaussian",
    "fittingRadius": 3,
    "method": "Weighted Least squares",
    "initialSigma": 1.5,
    "fullImageFitting": false,
    "crowdedField": {
      "name": "Multi-emitter fitting analysis",
      "mfaEnabled": true,
      "nMax": 5,
      "pValue": 1.0E-6,
      "keepSameIntensity": false,
      "intensityInRange": true,
      "intensityRange": "500:2500"
    }
  },
  "postProcessing": [
    {
      "name": "Remove duplicates",
      "options": "distformula=uncertainty_xy"
    },
    {
      "name": "Filter",
      "options": "formula=[(sigma > 30) & (sigma < 200)]"
    },
    {
      "name": "Drift correction",
      "options": "path=c:\\\\Users\\\\miros\\\\home\\\\work\\\\jhi\\\\projects\\\\plasmonic_microscopy\\\\data\\\\measurements\\\\biocev\\\\03_2021_09_13\\\\super_resolution_post_128x128\\\\17_80RG_ROI13_561nm_80pct_300gain_160nm_100ms.json magnification=16.0 method=[Cross correlation] ccsmoothingbandwidth=0.5 save=true steps=8 showcorrelations=false"
    }
  ],
  "is3d": false,
  "isSet3d": true
}