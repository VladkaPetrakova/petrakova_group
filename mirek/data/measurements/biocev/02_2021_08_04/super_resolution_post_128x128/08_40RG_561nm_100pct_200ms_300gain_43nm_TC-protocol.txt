{
  "version": "ThunderSTORM (dev-2016-09-10-b1)",
  "imageInfo": {
    "title": "08_40RG_561nm_100pct_200ms_300gain_43nm_TC.tiff"
  },
  "cameraSettings": {
    "readoutNoise": 0.0,
    "offset": 90.0,
    "quantumEfficiency": 0.95,
    "isEmGain": true,
    "photons2ADU": 62.5,
    "pixelSize": 42.9223,
    "gain": 300.0
  },
  "analysisFilter": {
    "name": "Wavelet filter (B-Spline)",
    "scale": 2.0,
    "order": 3
  },
  "analysisDetector": {
    "name": "Local maximum",
    "connectivity": 8,
    "threshold": "std(Wave.F1)"
  },
  "analysisEstimator": {
    "name": "PSF: Integrated Gaussian",
    "fittingRadius": 7,
    "method": "Weighted Least squares",
    "initialSigma": 3.0,
    "fullImageFitting": false,
    "crowdedField": {
      "name": "Multi-emitter fitting analysis",
      "mfaEnabled": true,
      "nMax": 5,
      "pValue": 1.0E-6,
      "keepSameIntensity": false,
      "intensityInRange": true,
      "intensityRange": "500:2500"
    }
  },
  "postProcessing": [
    {
      "name": "Remove duplicates",
      "options": "distformula=uncertainty_xy"
    },
    {
      "name": "Filter",
      "options": "formula=[(sigma > 30) & (sigma < 200)]"
    },
    {
      "name": "Density filter",
      "options": "neighbors=5 radius=50.0 dimensions=2D"
    }
  ],
  "is3d": false,
  "isSet3d": true
}