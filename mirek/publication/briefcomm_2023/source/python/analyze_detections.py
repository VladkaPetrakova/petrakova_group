# MH, v1.1, 2023_11_22

#%% lib
from IPython import get_ipython
from pathlib import Path
from skimage import io
from matplotlib import pyplot as plt
import numpy as np
import matplotlib as mpl
from show import show_image, show_histogram
import pandas as pd
from numpy import min, mean, max, median, std, var, sqrt, pi, floor, round

#%% init
mpl.rc('image', cmap='plasma')
mpl.rcParams['figure.figsize'] = (10,8)
mpl.rcParams['font.size'] = 16

#%% EM-CCD conversion of ADU number to photon count
def emccd_photon_conversion(adu, baseline, sensitivity, qe, em_gain):
    return 1/qe * sensitivity / em_gain * (adu-baseline)

#%% return positions [nm] from localization data frame
def get_positions_locs_nm(locs, i_frame):
    positions_nm = locs.loc[ 
      (locs['frame']==(i_frame+1)), ['y [nm]','x [nm]']].to_numpy()
    return positions_nm 

#%% conversion of positions [nm] to positions [pixels]
def nm2pxs(positions_nm, pixel_size_nm):
    return floor(positions_nm/pixel_size_nm).astype('int')
  
#%% main
if(__name__ == "__main__"):
  
    #%% clear, close all
    get_ipython().magic('reset -sf') 
    get_ipython().magic('clear') 
    plt.close('all')

    #%% control
    # export = True
    export = False

    #%% image stack path
    stack_path = Path(
        'c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy' +
        '/data/cfar_detector/measurement/2023_10_03/stack_64x64/' +
        'nolamp.tiff')
    
    #%% load data
    image_stack_raw = io.imread(stack_path)
    
    n_frames = image_stack_raw.shape[0]
    size = image_stack_raw.shape[1]
    
    #%% show raw image
    show_image(image_stack_raw[2], title='image [ADU]')
    
    #%%
    parameters = {
        'pixel_size_nm': 94,
        'quantum_efficiency': 0.9,
        'sensitivity': 12.05,
        'baseline': 100,
        'em_gain': 300,
        }
  
    #%% photon conversion
    image_stack = emccd_photon_conversion(
        image_stack_raw, 
        parameters['baseline'], 
        parameters['sensitivity'], 
        parameters['quantum_efficiency'],
        parameters['em_gain']
        )
    
    #%%
    print(min(image_stack), mean(image_stack), max(image_stack))
    
    #%% show image
    show_image(image_stack[0], title='image [photons]')
    
    #%% noise analysis
    noise = image_stack[0]
    
    show_histogram(noise.ravel(), gauss_fit=True,
                   text=f'mean: {round(mean(noise),1)}' + '\n'
                   f'std: {round(std(noise),1)}')
    
    #%% signal analysis
    signal = image_stack[2][32:41,19:28].copy()
    
    show_image(signal)
    
    #%% 1D signal
    signal_1D = signal[5,:]
    
    plt.figure()
    plt.plot(signal_1D)
    plt.show()
    
    #%% 1D signal parameter estimates
    signal_1D -= min(signal_1D)
    
    p = signal_1D/sum(signal_1D)
    
    x = list(range(len(p)))
    
    x_mean = np.dot(x,p)
    
    sigma_signal_1D = sqrt(np.dot((x-x_mean)**2,p))
    
    a_est = max(signal_1D)*(2*pi*sigma_signal_1D**2)
    
    #%% locs path
    locs_path = Path(
        'c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/'+ 
        'data/cfar_detector/measurement/2023_10_03/locs/nolamp.csv')
          
    #%% load locs
    # locs = np.genfromtxt(locs_path, delimiter=',', skip_header=1)
    locs = pd.read_csv(locs_path)  
    
    header = list(locs.columns)
    
    
    #%% show detections in single frame
    # i_frame = 0
    i_frame = 2
    
    show_image(
        image_stack[i_frame], 
            points=nm2pxs(
                get_positions_locs_nm(
                  locs,i_frame), parameters['pixel_size_nm']),
            title=i_frame
            )
          
    #%% show detections in multiple frames
    n_plots = 4
    offset = 1000
    
    for i_frame in range(offset, offset + n_plots):
        
        show_image(
          image_stack[i_frame], 
          points=nm2pxs(
              get_positions_locs_nm(
                  locs,i_frame), parameters['pixel_size_nm']),
          title=i_frame
          )
    

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
