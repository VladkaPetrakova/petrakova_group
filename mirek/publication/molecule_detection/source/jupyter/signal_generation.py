# MH, v1.1, 2024_10_07

#%% lib
import numpy as np
from numpy import pi, exp, min, max, mean, sqrt, log
from scipy.special import erf

#%%
def poisson_noise(image: np.ndarray, seed: int=None) -> np.ndarray:     
    # poisson_noise: Returns added Poisson noise to the image
    # Parameters:
    # seed = seed of the random number generator
    
    if seed is not None:
        np.random.seed(seed)        
    return np.random.poisson(image, image.shape)  

#%%
def awgn_noise(sigma: float, size, seed: int=None) -> np.ndarray:
    # awgn_noise: Returns zero-mean Gaussian noise.
    # Parameters:
    # sigma = sigma of Gaussian distribution
    # size = output shape
    # seed = seed of the random number generator
    
    if seed is not None:
        np.random.seed(seed)        
    return np.random.normal(loc=0, scale=sigma, size=size)

#%%
def get_uniform_background(
        background_dict: dict, shape: float=None, b: float=None) -> np.ndarray:
    # get_uniform_background: Returns constant background image 
    # Parameters:
    # background_dict = dictionary with all background generation parameters
    # shape = shape of the output
    # b = mean background value
    
    if shape is None:
        shape = background_dict['shape']
    if b is None:
        b = background_dict['mean']
        
    return np.full(shape, b) 

#%%
def get_uniform_background_poisson(
        background_dict: dict, seed: int=None, **kwargs) -> np.ndarray:               
    # get_uniform_background_poisson: Returns constant background image with 
    # added Poisson noise
    # Parameters:
    # background_dict = dictionary with all background generation parameters
    # seed = seed of the random number generator
    # **kwargs = key word agruments for function 'get_uniform_background'
        
    return poisson_noise(
        get_uniform_background(background_dict, **kwargs), seed=seed)

#%%
def get_uniform_background_poisson_awgn(
        background_dict: dict, sigma_n: float=None, 
        shape: tuple=None, seed: int=None, **kwargs) -> np.ndarray:    
    # get_uniform_background_poisson_awgn: Returns constant background image
    # with added Poisson and zero-mean Gaussian noise
    # Parameters:
    # background_dict = dictionary with all background generation parameters
    # sigma_n = sigma of zero-mean Gaussian noise
    # shape = shape of the output    
    # seed = seed of the random number generator
    # **kwargs = key word agruments for function 'get_uniform_background'    
    
    if sigma_n is None:        
        sigma_n = background_dict['sigma_n']
    if shape is None:
        shape = background_dict['shape']    
    
    uniform_background_poisson = get_uniform_background_poisson(
        background_dict, **kwargs, seed=seed)    
        
    awgn = awgn_noise(sigma_n, shape, seed=seed) 
    
    return uniform_background_poisson + awgn

#%%
def parabola2d(
        x: float, y: float, x0: float, y0: float, a: float) -> float:
    # parabola2d: Returns value of a 2D parabola 
    # Parameters:
    # x = x-coordinate of the parabola        
    # y = y-coordinate of the parabola        
    # x0 = x-coordinate of the top of the parabola
    # y0 = y-coordinate of the top of the parabola
    # a = parameter affecting the parabola pitch
    
    return 1/a**2 * ( (x-x0)**2 + (y-y0)**2 )

#%%
def parabola_signal(x0: float, y0: float, a: float, size: int) -> np.ndarray:
    # parabola_signal: Returns parabola signal of given size
    # Parameters:
    # x0 = x-coordinate of the top of the parabola
    # y0 = y-coordinate of the top of the parabola
    # a = parameter affecting the parabola pitch
    # size = one-sided size of the output array
    
    signal = np.zeros((size,size))    
    for i in range(size):
        for j in range(size):
            signal[i,j] = parabola2d(i,j,x0,y0,a)
            
    return signal

#%%
def set_array_mean(array: np.ndarray, m: float) -> np.ndarray:
    # set_array_mean: Set arbitrary mean value of the input nupy array
    # Parameters:
    # array = input array to which we change the mean value
    # m = new array mean value
    
    return array-mean(array)+m

#%%
def get_nonuniform_background(
        background_dict: dict, size: int=None, b: float=None, 
        curve: float=None, center: tuple=None) -> np.ndarray:     
    # get_nonuniform_background: Returns parabolic background image
    # Parameters:
    # background_dict = dictionary with all background generation parameters
    # size = one-sided size of the output array
    # b = mean background value
    # curve = parameter affecting the parabola pitch
    # center = tuple of x and y-coordinate of the top of the parabola

    if size is None:
        size = background_dict['size']
    if b is None:
        b = background_dict['mean']
    if curve is None:
        curve = background_dict['curve']
    if center is None:
        center = background_dict['center']
    
    nonuniform_background = parabola_signal(
        center[0], center[1], curve, size)

    nonuniform_background = set_array_mean(nonuniform_background, b)
    
    return nonuniform_background

#%%
def get_nonuniform_background_poisson(
        background_dict: dict, seed: int=None, **kwargs) -> np.ndarray: 

    # get_nonuniform_background_poisson: Returns parabolic background image with 
    # added Poisson noise
    # Parameters:
    # background_dict = dictionary with all background generation parameters
    # seed = seed of the random number generator
    # **kwargs = key word agruments for function 'get_nonuniform_background'    

    return poisson_noise(
        get_nonuniform_background(background_dict, **kwargs), seed=seed)

#%%
def get_nonuniform_background_poisson_awgn(
        background_dict: dict, seed: int=0) -> np.ndarray:    

    # get_nonuniform_background_poisson_awgn: Returns parabolic background
    # image with added Poisson noise and zero-mean Gaussian noise   
    # Parameters:
    # background_dict = dictionary with all background generation parameters
    # seed = seed of the random number generator
        
    nonuniform_background_poisson = get_nonuniform_background_poisson(
        background_dict, seed=seed)
    
    awgn = awgn_noise(
        background_dict['sigma_n'], background_dict['shape'],
        seed=seed) 
    
    return nonuniform_background_poisson + awgn

#%%
def gauss2d(x: float, y: float, x0: float, y0: float, 
            sigma: float, a: float) -> float:
    # gauss2d: Returns 2D gaussian value
    # Parameters:
    # x = x-coordinate of the gaussian        
    # y = y-coordinate of the gaussian        
    # x0 = x-coordinate of the center of the gaussian
    # y0 = y-coordinate of the center of the gaussian
    # sigma = standard deviation of the gaussian
    # a = photon count
    
    return a * 1/(2*pi*sigma**2)*exp(-((x-x0)**2+(y-y0)**2)/(2*sigma**2))

#%%
def integrated_gauss2d(
        x: float, y: float, x0: float, y0: float, 
        sigma: float, a: float) -> float:
    # integrated_gauss2d: Returns value of integrated 2D gaussian
    # Parameters:
    # x = x-coordinate of the integrated gaussian        
    # y = y-coordinate of the integrated gaussian        
    # x0 = x-coordinate of the center of the integrated gaussian
    # y0 = y-coordinate of the center of the integrated gaussian
    # sigma = standard deviation of the integrated gaussian
    # a = photon count
    
    Ex = 1/2*erf((x+1/2-x0)/(sqrt(2)*sigma))-1/2*erf(
        (x-1/2-x0)/(sqrt(2)*sigma))
    Ey = 1/2*erf((y+1/2-y0)/(sqrt(2)*sigma))-1/2*erf(
        (y-1/2-y0)/(sqrt(2)*sigma))
    
    return a * Ex*Ey

#%%
def get_single_spot(
        x0: float, y0: float, psf_fun, sigma: float, a: float,
        size: int, sigma_range: int=8) -> np.ndarray:
    # get_single_spot: Returns simulated 2D image with a single fluorescence 
    # molecule
    # Parameters:
    # x0 = x-coordinate of the center of the molecule
    # y0 = y-coordinate of the center of the molecule
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # sigma = standard deviation of the psf model
    # a = photon count
    # size = one-sided size of the output array
    # sigma_range = integer multiple of sigma where psf is considered as non-zero

    x_min = int(max([round(x0-sigma*sigma_range),0]))
    x_max = int(min([round(x0+sigma*sigma_range)+1,size]))

    y_min = int(max([round(y0-sigma*sigma_range),0]))
    y_max = int(min([round(y0+sigma*sigma_range)+1,size]))
    
    signal = np.zeros((size,size))    
    for i in range(x_min,x_max):
        for j in range(y_min,y_max):
            signal[i,j] = psf_fun(i,j,x0,y0,sigma,a)
            
    return signal

#%%
def random_coordinates(multispot_margin: float, size: int) -> tuple: 
    # random_coordinates: Returns a pair of uniformly random 2D molecule 
    # coordinates of the given size
    # Parameters:
    # multispot_margin = margin where coordinates are not allowed
    # size = one-sided size of the output array

    return (
        np.random.uniform(multispot_margin, size-1-multispot_margin), 
        np.random.uniform(multispot_margin, size-1-multispot_margin)
        )

#%%
def random_int_coordinates(multispot_margin: float, size: int) -> tuple:   
    # random_int_coordinates: Returns a pair of uniformly random integer
    # 2D molecule coordinates of the given size
    # Parameters:
    # multispot_margin = margin where coordinates are not allowed
    # size = one-sided size of the output array
    
    return (np.random.randint(multispot_margin,size-multispot_margin), 
            np.random.randint(multispot_margin,size-multispot_margin))

#%%
def make_multiple_spot(
        spot_coordinates: list, psf_fun, size: int, 
        multispot_sigma: list, multispot_a: list) -> np.ndarray:
    # make_multiple_spot: Returns simulated 2D image with a multiple fluorescence 
    # molecules with give coordinates.
    # Parameters:
    # spot_coordinates = list of molecule coordinates
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # size = one-sided size of the output array
    # multispot_sigma = list of standard deviations of poinspred function of 
    # individual molecules
    # multispot_a = list of photon counts of individual molecules
    
    n_spots = len(spot_coordinates)
    
    signal = np.zeros((size,size))    

    for i in range(n_spots):
        signal += get_single_spot(
            spot_coordinates[i][0], spot_coordinates[i][1],
            psf_fun, multispot_sigma[i], multispot_a[i], size)
        
    return signal

#%%
def get_multi_spot(
        signal_dict: dict, psf_fun, seed: int=0, int_coordinates: bool=False,
        multispot_margin: float=None, size: int=None, n_spots: int=None,
        multispot_sigma_mean: float=None, multispot_sigma_std: float=None,
        multispot_a_mean: float=None, multispot_a_std: float=None) -> np.ndarray:
    # get_multi_spot: Returns simulated 2D image with multiple fluorescence 
    # molecules 
    # Parameters:
    # signal_dict = dictionary with all signal generation parameters   
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # seed = seed of the random number generator
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # multispot_margin = margin where coordinates are not allowed
    # size = one-sided size of the output array
    # n_spots = number of molecules
    # multispot_sigma_mean = mean value of psf standard deviations of the molecules
    # multispot_sigma_std = standard deviation of psf standard deviations of the molecules
    # multispot_a_mean = mean value of photoncounts of the molecules
    # multispot_a_std = standard deviation of photoncounts of the molecules

    np.random.seed(seed)     

    if multispot_margin is None:
        multispot_margin = signal_dict['multispot_margin']
    if size is None:
        size = signal_dict['size']
    if n_spots is None:
        n_spots = signal_dict['n_spots']
    if multispot_sigma_mean is None:
        multispot_sigma_mean = signal_dict['multispot_sigma_mean']
    if multispot_sigma_std is None:
        multispot_sigma_std = signal_dict['multispot_sigma_std']
    if multispot_a_std is None:
        multispot_a_std = signal_dict['multispot_a_std']
    if multispot_a_mean is None:
        multispot_a_mean = signal_dict['multispot_a_mean']            
            
    if int_coordinates:
        spot_coordinates = [
            random_int_coordinates(
                multispot_margin, size) for i in range(n_spots)]
    else:
        spot_coordinates = [
            random_coordinates(
                multispot_margin, size) for i in range(n_spots)]
        
    multispot_sigma = np.random.normal(
        multispot_sigma_mean, multispot_sigma_std, size=(n_spots,))

    multispot_a = np.random.normal(
        multispot_a_mean, multispot_a_std, size=(n_spots,))

    multi_spot = make_multiple_spot(
        spot_coordinates, psf_fun, size, multispot_sigma, multispot_a)
        
    return multi_spot

#%%
def get_multi_spot_uniform_background_poisson(
    signal_dict: dict, psf_fun, background_dict: dict, seed: int=0, 
    bkg_kwargs=None, **kwargs) -> np.ndarray:    
    # get_multi_spot_uniform_background_poisson: Returns simulated 2D image 
    # with multiple fluorescence molecules on constant background with
    # added Poisson noise 
    # Parameters:
    # signal_dict = dictionary with all signal generation parameters   
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # background_dict = dictionary with all background generation parameters
    # seed = seed of the random number generator
    # bkg_kwargs = key word agruments for function 'get_uniform_background'    
    # **kwargs = key word agruments for function 'get_multi_spot'    

    multi_spot = get_multi_spot(
        signal_dict, psf_fun, seed=seed, **kwargs)
    
    if bkg_kwargs is None:
        uniform_background = get_uniform_background(background_dict)
    else:
        uniform_background = get_uniform_background(
            background_dict, **bkg_kwargs)
        
    multi_spot_uniform_background_noise = poisson_noise(
        multi_spot + uniform_background, seed=seed)
    
    return multi_spot_uniform_background_noise

#%%
def get_multi_spot_uniform_background_poisson_awgn(
    signal_dict, psf_fun, background_dict: dict, seed=0) -> np.ndarray:    
    # get_multi_spot_uniform_background_poisson_awgn: Returns simulated 2D image 
    # with multiple fluorescence molecules on constant background with
    # added Poisson noise and zero-mean Gaussian noise
    # Parameters:
    # signal_dict = dictionary with all signal generation parameters   
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'

    multi_spot_uniform_background_poisson = get_multi_spot_uniform_background_poisson(
        signal_dict, psf_fun, background_dict, seed=seed)
    
    awgn = awgn_noise(
        background_dict['sigma_n'], background_dict['shape'],
        seed=seed) 
    
    return multi_spot_uniform_background_poisson + awgn

#%%
def get_multi_spot_nonuniform_background_poisson(
        signal_dict, psf_fun, background_dict: dict, 
        seed=0, bkg_kwargs=None, **kwargs) -> np.ndarray:    
    # get_multi_spot_nonuniform_background_poisson: Returns simulated 2D image 
    # with multiple fluorescence molecules on parabolic background with
    # added Poisson noise 
    # Parameters:
    # signal_dict = dictionary with all signal generation parameters   
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # background_dict = dictionary with all background generation parameters
    # seed = seed of the random number generator
    # bkg_kwargs = key word agruments for function 'get_uniform_background'    
    # **kwargs = key word agruments for function 'get_multi_spot' 
    
    multi_spot = get_multi_spot(signal_dict, psf_fun, 
                                seed=seed, **kwargs)
    
    if bkg_kwargs is None:
        nonuniform_background = get_nonuniform_background(
            background_dict)
    else:
        nonuniform_background = get_nonuniform_background(
            background_dict, **bkg_kwargs)

    multi_spot_nonuniform_background_noise = poisson_noise(
        multi_spot + nonuniform_background, seed=seed)
    
    return multi_spot_nonuniform_background_noise

#%%
def get_multi_spot_nonuniform_background_poisson_awgn(
    signal_dict, psf_fun, background_dict: dict, seed=0) -> np.ndarray:    
    # get_multi_spot_nonuniform_background_poisson_awgn: Returns simulated 2D image 
    # with multiple fluorescence molecules on parabolic background with
    # added Poisson noise and zero-mean Gaussian noise
    # Parameters:
    # signal_dict = dictionary with all signal generation parameters   
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # background_dict = dictionary with all background generation parameters
    # seed = seed of the random number generator

    multi_spot_nonuniform_background_poisson = get_multi_spot_nonuniform_background_poisson(
        signal_dict, psf_fun, background_dict, seed=seed)

    awgn = awgn_noise(
        background_dict['sigma_n'], background_dict['shape'],
        seed=seed) 
    
    return multi_spot_nonuniform_background_poisson + awgn

#%%
def get_mf(psf_fun, mf_sigma: float, mf_range: int) -> np.ndarray:
    # get_mf: Returns matched filter with PSF function given by parameter 
    # 'psf_fun'
    # Parameters:
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # mf_sigma = standard deviation of the matched filter psf model
    # mf_range = one-sided half size of the filter kernel
    
    mf_size = 2*mf_range + 1
    
    mf = get_single_spot(x0=mf_range, y0=mf_range,
        psf_fun=psf_fun, sigma=mf_sigma, a=1, size=mf_size)  
    
    return mf  

#%%
def get_pmf(psf_fun, pmf_sigma: float, pmf_range: int, a: float, b: float) -> np.ndarray:
    # get_pmf: Returns Poisson matched filter with PSF function given by 
    # parameter 'psf_fun'
    # Parameters:
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # pmf_sigma = standard deviation of the Poisson matched filter psf model
    # pmf_range = one-sided half size of the filter kernel
    # a = molecule photon count
    # b = mean background value
    
    pmf_size = 2*pmf_range + 1    
    
    psf = get_single_spot(x0=pmf_range, y0=pmf_range,
        psf_fun=psf_fun, sigma=pmf_sigma, a=1, size=pmf_size)  
    
    pmf = log(1 + a*psf/b)
    
    return pmf



















































