# MH, v1.1, 2024_10_08

#%%
import pandas as pd
from numpy import floor
import numpy as np
from tqdm import tqdm
import pathlib

from linking import lsa

from show import show_image
from detection_tools import get_detection_points
from performance import evaluate_points

#%%
def get_positions_nm(path: pathlib) -> np.ndarray:   
    # get_positions_nm: Load ground true molecule position coordinates 
    # in nanometers
    # Parameters:
    # path = path to the *.csv data file
    
    df = pd.read_csv(path)       
    positions_nm = df[['ynano','xnano']].to_numpy()
    return positions_nm

#%%
def nm2pxs(positions_nm: np.ndarray, pixel_size_nm: float) -> np.ndarray:
    # nm2pxs: Converts list of molecule positions in nanometers into pixels
    # Parameters:
    # positions_nm = list of molecule positions in nanometers 
    # pixel_size_nm = size of one pixel in nanometers
    
    return floor(positions_nm/pixel_size_nm).astype('int')

#%%
def get_positions(path: pathlib, pixel_size_nm: float) -> np.ndarray:  
    # get_positions: Load ground true molecule position coordinates 
    # in pixelss
    # Parameters:
    # path = path to the *.csv data file
    # pixel_size_nm = size of one pixel in nanometers

    positions_nm = get_positions_nm(path)
    positions = nm2pxs(positions_nm, pixel_size_nm)    
    return positions

#%%
def get_intensity(path: pathlib) -> np.ndarray:
    # get_intensity: Load molecule photon counts
    # Parameters:
    # path = path to the *.csv data file
    
    df = pd.read_csv(path)       
    intensity = df['intensity'].to_numpy()
    return intensity

#%%
def evaluate_frame(
    image: np.ndarray,
    position_path: pathlib,
    detector_type, 
    pfa: float, 
    local_max_range: int, 
    tp_threshold: float, 
    dataset_dict: dict, 
    kernel: np.ndarray=None,
    ) -> tuple:
    # evaluate_frame: Evaluate predicted molecule detections in the ith frame
    # Parameters:
    # image = input image
    # position_path = path to the *.csv data file
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    
    gt_points = get_positions(position_path, dataset_dict['pixel_size_nm']) 
    pr_points = get_detection_points(
        image, detector_type, pfa, local_max_range, kernel=kernel)
    
    # assignment
    gt_points_assigned, pr_points_assigned = lsa(gt_points, pr_points)
    
    # evaluation
    tp,fp,tn,fn = evaluate_points(
        gt_points, pr_points, gt_points_assigned, pr_points_assigned, 
        tp_threshold, dataset_dict['shape'])
    
    return tp,fp,tn,fn    

#%%
def show_frame_performance_btls(
    i_frame: int, 
    images: np.ndarray, 
    gt_position_paths: list,
    detector_type, 
    pfa: float, 
    local_max_range: int, 
    dataset_dict: dict, 
    kernel: np.ndarray=None,
    ) -> None:
    # show_frame_performance_btls: Show predicted and ground true molecule 
    # positions over input image
    # Parameters:
    # i_frame = index of the frame to be shown
    # images = datacube of all frames
    # gt_position_paths = list of paths to ground true positions
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    image = images[i_frame]
    gt_position_path = gt_position_paths[i_frame]
    
    gt_points = get_positions(gt_position_path, dataset_dict['pixel_size_nm'])  
    pr_points = get_detection_points(
        image, detector_type, pfa, local_max_range, kernel=kernel)
    
    show_image(image, points=gt_points, 
               title='ground true', figsize=(5,5))
    show_image(image, points=pr_points, 
               title='predictions', figsize=(5,5))
    
#%%
def evaluate_all_frames(
    images: np.ndarray, 
    gt_position_paths: list,
    detector_type, 
    pfa: float, 
    local_max_range: int, 
    tp_threshold: float, 
    n_frames: int, 
    dataset_dict: dict,
    kernel: np.ndarray=None,
    ) -> tuple:
    
    # evaluate_all_frames: Evaluate predicted molecule detections in all frames
    # Parameters:
    # images = datacube of all frames
    # gt_position_paths = list of paths to ground true positions
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # n_frames = number of frames to be evaluated
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    
    tp_vec = np.zeros(n_frames)
    fp_vec = np.zeros(n_frames)
    tn_vec = np.zeros(n_frames)
    fn_vec = np.zeros(n_frames)    
    
    for i in tqdm(range(n_frames)):
    
        tp_vec[i], fp_vec[i], tn_vec[i], fn_vec[i] = evaluate_frame(
            images[i], gt_position_paths[i],  detector_type,
            pfa, local_max_range, tp_threshold, dataset_dict, kernel=kernel)   
    
    return  tp_vec, fp_vec, tn_vec, fn_vec

    
    
    
    
