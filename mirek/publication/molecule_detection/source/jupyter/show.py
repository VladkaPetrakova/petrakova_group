# MH, v1.2, 2024_10_07

#%% lib
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
import pathlib

from numpy import pi, exp, min, max, sum, mean, median, std, sqrt, var
from scipy.stats import iqr

#%%
def show_image(
    image: np.ndarray, 
    figsize: tuple=(6,6),  
    title: str=None, 
    title_size: int=15,
    show_colorbar: bool=True, 
    colorbar_fraction: float=0.045,
    hide_axis: bool=True, 
    text: str=None, 
    text_x: int=4, 
    text_y: int=4, 
    text_size: int=12, 
    points: list=None, 
    detection_size: int=5, 
    detection_color: str='yellow', 
    detection_width: int=1,
    output_file_path: pathlib=None, 
    export: bool=False,
    dpi: int=300,
    close_fig: bool=False, 
    **kwargs,
    ) -> None :
    # show_image: Helper function for visualization of a 2D image
    # Parameters:
    # points = list of 2D detection coordinates. If parameter 'points' is not 
    # None, than detections are marked by bounding boxes.
    
    fig, ax = plt.subplots(figsize=figsize)
    im = ax.imshow(image, **kwargs)    
    plt.title(title, fontdict={'fontsize': title_size})

    if show_colorbar:
        plt.colorbar(im, ax=ax, fraction=colorbar_fraction)
    
    if hide_axis:
        plt.axis('off')

    if text is not None:                       
        plt.text(text_x, text_y, 
            text, backgroundcolor='w', va ='top', size=text_size)   
    
    if points is not None:
        
        def rectangle_patch(x0,y0,w,c):
            return mpl.patches.Rectangle(
                (x0-0.5-(w-1)/2,y0-0.5-(w-1)/2),w,w,
                    linewidth=detection_width, 
                    edgecolor=c, facecolor='none')
        
        for p in points:
            ax.add_patch(
                rectangle_patch(p[1],p[0],
                                detection_size,detection_color))
            
    if output_file_path is not None and export:
        plt.savefig(output_file_path, 
                    dpi=dpi, facecolor='w', bbox_inches='tight')

    if close_fig:
        plt.close(fig)
    else:
        plt.show() 
        
#%%
def show_histogram(
    data: np.ndarray, 
    figsize: tuple=(6,4),      
    bins: int=30, 
    label: str='Histogram',
    alpha: float=0.5,
    density: bool=True, 
    title: str=None, 
    xlabel: int=None, 
    ylabel: int=None, 
    xlim=None,
    gauss_fit: bool=False, 
    robust_gauss_fit: bool=False,
    gauss_fit_points: int=100,
    text: str=None, 
    text_x: int=0, 
    text_y: int=0,     
    show_legend: bool=False,
    output_file_path: pathlib=None, 
    export: bool=True,
    dpi: int=300,    
    close_fig: bool=False, 
    **kwargs,
    )-> None :
    
    # show_histogram: Helper function for visualization of a 1D data histogram
    # Parameters:
    # gauss_fit = boolean parameter whether to fit histogram by Gaussian pdf. 
    # The fit is based on method of moments, where mean and variance are 
    # computed as arithmetic mean and arithmetic variance. The Gaussian is fited 
    # by robust estimates (median and inter-quantile range), if boolean parameter 
    # 'robust_gauss_fit' is True.
    
    fig, ax = plt.subplots(figsize=figsize)

    plt.hist(data, 
             alpha=alpha, 
             bins=bins, 
             label=label, 
             density=density, 
             **kwargs)        
    plt.title(title)        
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim(xlim)
    
    if gauss_fit:
        
        def gauss1d(x, mu, sigma):
            return 1/(sqrt(2*pi)*sigma)*exp(-(x-mu)**2/(2*sigma**2))
            
        if robust_gauss_fit:
            mu = median(data)            
            sigma = iqr(data, scale='normal')        
        else:
            mu = mean(data)
            sigma = std(data)
        
        xn = np.linspace(min(data), max(data), num=gauss_fit_points)
        yn = [gauss1d(x, mu, sigma) for x in xn]
        
        plt.plot(xn, yn, 'k', label='Gauss Model')
    
    if text is not None:
        
        _, ymax = plt.ylim()
        
        plt.text(min(data)+text_x, 0.95*ymax+text_y, text, 
                 backgroundcolor='w', va='top')       
    
    if show_legend:
        plt.legend()

    if output_file_path is not None and export:
        plt.savefig(
            output_file_path, dpi=dpi, facecolor='w', bbox_inches='tight')

    if close_fig:
        plt.close(fig)
    else:
        plt.show() 
        