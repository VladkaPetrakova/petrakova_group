# MH, v1.0, 2024_02_05

import numpy as np
from numpy import median

from scipy.stats import iqr
from gauss_nofilter import isf_threshold
from detection_tools import remove_nonlocal_maxima

#%%
def cfar_test_mean_estimate(T: np.ndarray)-> float:
    # cfar_test_mean_estimate: Returns global mean level 
    # estimate given by median 
    # Parameters:
    # T = input image with test statistics from which the mean is estimated

    return median(T)

#%%
def cfar_test_std_estimate(T: np.ndarray)-> float:
    # cfar_test_std_estimate: Returns global standard deviation level 
    # estimate given by inter-quantile range 
    # Parameters:
    # T = input image with test statistics from which the std is estimated

    return iqr(T.ravel(), scale='normal')

#%%
def cfar_segmentation(T: np.ndarray, pfa: float)-> np.ndarray:  
    # cfar_segmentation: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold
    # Parameters:
    # T = input image with test statistics 
    # pfa = probability of false alarm 
       
    mean_T = cfar_test_mean_estimate(T)
    std_T = cfar_test_std_estimate(T)    
    tau = isf_threshold(pfa, mean_T, std_T)    
    mask = T > tau
    return mask

#%%
def cfar(T: np.ndarray, pfa: float, local_max_range: int) -> np.ndarray:  
    # cfar: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold and which 
    # form local maximum
    # Parameters:
    # T = input image with test statistics 
    # pfa = probability of false alarm
    # local_max_range = range over which local maximum is searched

    segmentation = cfar_segmentation(T, pfa)    
    mask = remove_nonlocal_maxima(segmentation, T, local_max_range)    
    return mask