# MH, v1.0, 2024_10_09

#%% lib
from numpy import pi, exp, log, sqrt, sum, mean, round, max, min, abs
import numpy as np
from tqdm import tqdm
from scipy.signal import convolve
from scipy.stats import norm

from detection_tools import mask2points
from poisson_pmf import cfar_segmentation
from performance import sem

#%%
def gauss3d(
        x: float, y: float, z: float,
        x0: float, y0: float, z0: float,
        sigma_xy: float, sigma_z: float, a: float
        )-> float:
    
    # gauss3d: Returns 3D gaussian value
    # Parameters:
    # x = x-coordinate of the gaussian        
    # y = y-coordinate of the gaussian        
    # z = z-coordinate of the gaussian        
    # x0 = x-coordinate of the center of the gaussian
    # y0 = y-coordinate of the center of the gaussian
    # z0 = z-coordinate of the center of the gaussian
    # sigma_xy = standard deviation of the gaussian in x-y plane
    # sigma_z = standard deviation of the gaussian in z plane
    # a = photon count

    f_xy = 1/(2*pi*sigma_xy**2)*exp(-((x-x0)**2+(y-y0)**2)/(2*sigma_xy**2))
    f_z = 1/sqrt(2*pi*sigma_z**2)*exp(-((z-z0)**2)/(2*sigma_z**2))
    
    return a * f_xy * f_z

#%%
def get_singlespot3d(
        x0: float,
        y0: float,
        z0: float,
        psf_fun,
        sigma_xy: float,
        sigma_z: float,
        a: float,
        size_xy: int,
        size_z: int,
        sigma_range: int=8
        ) -> np.ndarray:
    # get_singlespot3d: Returns simulated 3D image with a single fluorescence 
    # molecule
    # Parameters:
    # x0 = x-coordinate of the center of the molecule
    # y0 = y-coordinate of the center of the molecule
    # z0 = z-coordinate of the center of the molecule
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # sigma_xy = standard deviation of the molecule in x-y plane
    # sigma_z = standard deviation of the molecule in z plane
    # a = photon count
    # size_xy = one-sided size of the output array in x-y plane
    # size_z = one-sided size of the output array in z plane
    # sigma_range = integer multiple of sigma where psf is considered as non-zero

    x_min = int(max([round(x0-sigma_xy*sigma_range),0]))
    x_max = int(min([round(x0+sigma_xy*sigma_range)+1,size_xy]))

    y_min = int(max([round(y0-sigma_xy*sigma_range),0]))
    y_max = int(min([round(y0+sigma_xy*sigma_range)+1,size_xy]))

    z_min = int(max([round(z0-sigma_z*sigma_range),0]))
    z_max = int(min([round(z0+sigma_z*sigma_range)+1,size_z]))   
    
    signal = np.zeros((size_z,size_xy,size_xy))    
    for i in range(x_min,x_max):
        for j in range(y_min,y_max):
            for k in range(z_min,z_max):
                signal[k,j,i] = psf_fun(i,j,k,x0,y0,z0,sigma_xy,sigma_z,a)
            
    return signal

#%%
def random_coordinates3d(
        margin_xy: float, 
        margin_z: float, 
        size_xy: int,
        size_z: int,
        ) -> tuple:    
    # random_coordinates3d: Returns 3 uniformly random 3D molecule 
    # coordinates of the given size
    # Parameters:
    # margin_xy = margin where coordinates are not allowed in x-y plane
    # margin_z = margin where coordinates are not allowed in z plane
    # size_xy = one-sided size of the output array in x-y plane
    # size_z = one-sided size of the output array in z plane

    x0 = np.random.uniform(margin_xy, size_xy-1-margin_xy)
    y0 = np.random.uniform(margin_xy, size_xy-1-margin_xy)
    z0 = np.random.uniform(margin_z, size_z-1-margin_z)
    
    return (z0,y0,x0)

#%%
def get_multispot3d(
        multispot_coordinates: np.ndarray, 
        multispot_sigma_xy: np.ndarray, 
        multispot_sigma_z: np.ndarray, 
        multispot_a: np.ndarray, 
        psf_fun, 
        size_xy: int,
        size_z: int,
        ) -> np.ndarray:
    # get_multispot3d: Returns simulated 3D image with a multiple fluorescence 
    # molecules with give coordinates.
    # Parameters:
    # multispot_coordinates = list of molecule coordinates
    # multispot_sigma_xy = list of stds of psf in x-y
    # multispot_sigma_z = list of stds of psf in z
    # multispot_a = list of photon counts of individual molecules
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # size_xy = one-sided size of the output array in x-y plane
    # size_z = one-sided size of the output array in z plane
    
    n_spots = len(multispot_coordinates)
    
    signal = np.zeros((size_z,size_xy,size_xy))    

    for i in tqdm(range(n_spots)):
        
        signal += get_singlespot3d(
            multispot_coordinates[i,2],
            multispot_coordinates[i,1],
            multispot_coordinates[i,0],            
            psf_fun, 
            multispot_sigma_xy[i], 
            multispot_sigma_z[i], 
            multispot_a[i], 
            size_xy,
            size_z)
        
    return signal

#%%
def round_tuple(p: tuple) -> tuple:
    # round_tuple: Returns rounded tuple of numbers
    # Parameters:
    # p = tuple of real numbers
    
    return tuple([int(round(i)) for i in p])

#%%
def get_psf3d(
        psf_fun, 
        psf_size_xy: int, 
        psf_size_z: int, 
        sigma_xy: float,
        sigma_z: float,
        )-> np.ndarray:
    # get_psf3d: Returns 3D Poisson matched filter with PSF function 
    # given by parameter 'psf_fun'
    # Parameters:
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # psf_size_xy = size of the filter kernel in x-y
    # psf_size_z = size of the filter kernel in z
    # sigma_xy = standard deviation of the molecule in x-y plane
    # sigma_z = standard deviation of the molecule in z plane
    
    psf_range_xy = (psf_size_xy-1)//2
    psf_range_z = (psf_size_z-1)//2
    
    psf = get_singlespot3d(
        x0=psf_range_xy, 
        y0=psf_range_xy,
        z0=psf_range_z,
        psf_fun=psf_fun, 
        sigma_xy=sigma_xy, 
        sigma_z=sigma_z, 
        a=1, 
        size_xy=psf_size_xy,
        size_z=psf_size_z,    
    )
    
    return psf

#%%
def test_statistic3d(r: np.ndarray, w: np.ndarray) -> np.ndarray:
    # test_statistic3d: Returns filtered raw signal 'r' by filter kernel 'w'
    # Parameters:
    # r = received 3D signal
    # w = filter kernel

    w_size_xy = w.shape[1]
    w_size_z = w.shape[0]

    w_range_xy = (w_size_xy-1)//2
    w_range_z = (w_size_z-1)//2

    pad_width = ((w_range_z,w_range_z),(w_range_xy,w_range_xy),(w_range_xy,w_range_xy))
    # r_pad = np.pad(r, pad_width, mode='reflect')
    r_pad = np.pad(r, pad_width, mode='symmetric')
    T = convolve(r_pad, w, mode='valid')    
    
    return T

#%%
def neigborhood3d(
        T: np.ndarray,
        x0: int,
        y0: int,
        z0: int,
        range_xy: int,
        range_z: int
        ) -> np.ndarray:
    # neigborhood3d: Return 3D sub-array centered around '(x0,y0,z0)' with 
    # range 'range_xy' and 'range_z'
    # Parameters:
    # T = 3D image datacube
    # x0 = x-coordinate of the center 
    # y0 = y-coordinate of the center 
    # z0 = z-coordinate of the center
    # range_xy = radius of sub-arrayin x-y  
    # range_z = radius of sub-arrayin z  

    size_z = T.shape[0]
    size_xy = T.shape[1]
    
    x_min = int(max([round(x0-range_xy),0]))
    x_max = int(min([round(x0+range_xy)+1,size_xy]))    
    
    y_min = int(max([round(y0-range_xy),0]))
    y_max = int(min([round(y0+range_xy)+1,size_xy]))

    z_min = int(max([round(z0-range_z),0]))
    z_max = int(min([round(z0+range_z)+1,size_z]))
    
    return T[z_min:z_max,y_min:y_max,x_min:x_max]

#%%
def is_localmax3d(
        T: np.ndarray,
        x0: int,
        y0: int,
        z0: int,
        range_xy: int,
        range_z: int
        ) -> float:    
    # is_localmax3d: Returns true if tested '(x0,y0,z0)' is a local maximum 
    # in the neighborhood of radius 'range_xy' and 'range_z'
    # Parameters:
    # T = 3D image datacube
    # x0 = x-coordinate of the center 
    # y0 = y-coordinate of the center 
    # z0 = z-coordinate of the center
    # range_xy = radius of sub-arrayin x-y  
    # range_z = radius of sub-arrayin z 

    return max(neigborhood3d(T,x0,y0,z0,range_xy,range_z) ) <= T[z0,y0,x0]   

#%%
def get_localmax_points3d(
        T: np.ndarray,
        points: np.ndarray,
        localmax_range_xy: int,
        localmax_range_z: int
        )-> np.ndarray:
    # get_localmax_points3d: Returns points of local maximum coordinates 
    # in 3D image 'T' selected from input points 'points'
    # Parameters:
    # T = 3D image datacube
    # points: list of x-y-z coordinates
    # local_max_range_xy = radius of local maximum in x-y
    # local_max_range_z = radius of local maximum in z
    
    local_max_points = np.array(
        [p for p in points if 
             is_localmax3d(T,*(np.flip(p)),localmax_range_xy,localmax_range_z)
        ])
    return local_max_points

#%%
def points2mask3d(
        points: np.ndarray,
        shape: tuple
        )-> np.ndarray:
    # points2mask3d: Converts list of pixels to a binary segmentation mask 
    # Parameters:
    # points = list of pixels 
    # shape = output shape

    mask = np.zeros(shape)    
    for p in points:
        mask[*p]=1        
    return mask

#%%
def remove_nonlocal_maxima3d(
        segmentation: np.ndarray, 
        T: np.ndarray,
        localmax_range_xy: int,
        localmax_range_z: int
        )-> np.ndarray:     
    # remove_nonlocal_maxima3d: Returns segmentation masks containing only 
    # pixels that form local maxima of the radius given by 'local_max_range_xy'
    # and 'local_max_range_z'
    # Parameters:
    # segmentation = binary segmentation mask containing non-local maximum pixels
    # T = 3D image datacube
    # local_max_range_xy = radius of local maximum in x-y
    # local_max_range_z = radius of local maximum in z
    
    points = mask2points(segmentation)    
    localmax_points = get_localmax_points3d(
        T*segmentation + norm.rvs(loc=0, scale=10**-9, size=T.shape),        
        points, localmax_range_xy, localmax_range_z)    
    mask = points2mask3d(localmax_points, T.shape)
    return mask

#%%
def cfar3d(
        T: np.ndarray,
        pfa: float,
        w: np.ndarray, 
        localmax_range_xy: int,
        localmax_range_z: int
        )-> np.ndarray:     
    # cfar3d: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold and which 
    # form local maximum
    # Parameters:
    # T = input image with test statistics 
    # pfa = probability of false alarm
    # w = filter kernel    
    # local_max_range_xy = radius of local maximum in x-y
    # local_max_range_z = radius of local maximum in z
    
    segmentation = cfar_segmentation(T, pfa, w)    
    mask = remove_nonlocal_maxima3d(segmentation, T, localmax_range_xy,localmax_range_z)    
    return mask

#%%
def is_tp(
        p0: tuple, 
        p: tuple, 
        tp_threshold_xy: float, 
        tp_threshold_z: float,
        ) -> bool:
    # is_tp: Returns true if two 3D points are equal within tolerance
    # Parameters:
    # p0 = point
    # p = point
    # tp_threshold_xy = tolerance in x-y
    # tp_threshold_z = tolerance in z
    
    d = p0 - p
    return ( abs(d[0]) <= tp_threshold_z ) & (
        np.all( abs(d[1:]) <= tp_threshold_xy ) )

#%%
def evaluate_single_emitter_detections3d(
        points: np.ndarray, 
        p0: np.ndarray, 
        tp_threshold_xy: float, 
        tp_threshold_z: float,
        shape: tuple,
        ) -> tuple: 
    # evaluate_single_emitter_detections3d: Counts number of true positive,
    # false positive, true negative, false negative detections for a single 
    # emitter data in 3d
    # Parameters:
    # points = array of x-y-z coordinates of marked detections
    # p0 = x-y-z coordinate of true molecule position
    # tp_threshold_xy = tolerance in x-y
    # tp_threshold_z = tolerance in z
    # shape = image shape 
    
    n_pixels = int(np.prod(shape))
    n_points = points.shape[0]
    
    tp = 0; fp = 0; tn=0; fn = 0
    
    if n_points > 0:       

        is_tp_present = np.any(
            [ is_tp( p0, p, tp_threshold_xy, tp_threshold_z ) for p in points]
        )
        
        if is_tp_present:            
            tp = 1
            fp = n_points-1       
            tn = n_pixels-n_points
        else:
            fp = n_points
            fn = 1
            tn = n_pixels-1-n_points
    else:        
        fn = 1
        tn = n_pixels-1
    return tp, fp, tn, fn

#%%
def pd_monte_carlo_filter3d(
        n_samples: int,
        pfa: float,
        w: np.ndarray,
        a: float, 
        b: float, 
        shape: tuple,
        sigma_xy: float,
        sigma_z: float,
        margin_xy: float, 
        margin_z: float,
        psf_fun, 
        detector_type,
        localmax_range_xy: int,
        localmax_range_z: int,
        tp_threshold_xy: float, 
        tp_threshold_z: float,
        ) -> tuple:    
    # pd_monte_carlo_filter3d: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images filtered by kernel 'w' 
    # on which the detection algorithm is tested
    
    # Parameters:
    # n_samples = number of Monte Carlo trials
    # pfa = probability of false alarm
    # w = filter kernel    
    # a = molecule photon count
    # b = mean background value
    # shape = image shape 
    # sigma_xy = standard deviation of the molecule in x-y plane
    # sigma_z = standard deviation of the molecule in z plane
    # margin_xy = margin where coordinates are not allowed in x-y plane
    # margin_z = margin where coordinates are not allowed in z plane
    # psf_fun = point spread function model
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # local_max_range_xy = radius of local maximum in x-y
    # local_max_range_z = radius of local maximum in z
    # tp_threshold_xy = tolerance in x-y
    # tp_threshold_z = tolerance in z

    size_z = shape[0]
    size_xy = shape[1]            
        
    tp_vec = np.zeros(n_samples, dtype='int')
    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')            
    fn_vec = np.zeros(n_samples, dtype='int')
    
    for i in range(n_samples):   

        # coordinates
        (z0r,y0r,x0r) = random_coordinates3d(
            margin_xy, margin_z, size_xy, size_z)

        # single spot
        randomspot = get_singlespot3d(
            x0r,y0r,z0r,psf_fun,sigma_xy,sigma_z,a,size_xy,size_z)

        # received signal
        r = np.random.poisson(randomspot + np.full(shape, b)) 

        # test statistic
        T = test_statistic3d(r, w)

        # detection
        mask = detector_type(T,pfa,w,localmax_range_xy,localmax_range_z)        
                        
        # evaluation
        localmax_points = mask2points(mask)        

        # rounded coordinates
        (z0ri,y0ri,x0ri) = round_tuple( (z0r,y0r,x0r) )
        
        p0ri = (z0ri,y0ri,x0ri)

        (tp,fp,tn,fn) = evaluate_single_emitter_detections3d(
            localmax_points, p0ri, tp_threshold_xy, tp_threshold_z, shape)        
               
        tp_vec[i] = tp
        fp_vec[i] = fp
        tn_vec[i] = tn
        fn_vec[i] = fn
    
    return tp_vec, fp_vec, tn_vec, fn_vec

#%%
def pd_vec_monte_carlo_filter3d(
        pfa_vec: list, 
        n_samples: int, 
        w: np.ndarray,
        a: float, 
        b: float, 
        shape: tuple,
        sigma_xy: float,
        sigma_z: float,
        margin_xy: float, 
        margin_z: float,
        psf_fun, 
        detector_type,
        localmax_range_xy: int,
        localmax_range_z: int,
        tp_threshold_xy: float, 
        tp_threshold_z: float,
        ) -> tuple:        

    # pd_vec_monte_carlo_filter3d: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images filtered by kernel 'w' 
    # on which the detection algorithm is tested for a list of pfa values
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # n_samples = number of Monte Carlo trials
    # w = filter kernel        
    # a = molecule photon count
    # b = mean background value
    # shape = image shape 
    # sigma_xy = standard deviation of the molecule in x-y plane
    # sigma_z = standard deviation of the molecule in z plane
    # margin_xy = margin where coordinates are not allowed in x-y plane
    # margin_z = margin where coordinates are not allowed in z plane
    # psf_fun = point spread function model
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # local_max_range_xy = radius of local maximum in x-y
    # local_max_range_z = radius of local maximum in z
    # tp_threshold_xy = tolerance in x-y
    # tp_threshold_z = tolerance in z

    n_pfa = len(pfa_vec)  
    
    pd_mean_spot_vec = np.empty(n_pfa)
    pd_sem_spot_vec = np.empty(n_pfa)
    pfa_mean_spot_vec = np.empty(n_pfa)
    pfa_sem_spot_vec = np.empty(n_pfa)
    jaccard_mean_spot_vec = np.empty(n_pfa)
    jaccard_sem_spot_vec = np.empty(n_pfa)     

    for i, pfa in enumerate(tqdm(pfa_vec)):        
        
        (tp_sample_spot_vec, fp_sample_spot_vec, 
         tn_sample_spot_vec,fn_sample_spot_vec) = pd_monte_carlo_filter3d(
            n_samples, pfa, w, a, b, shape, sigma_xy, sigma_z, margin_xy, margin_z,
            psf_fun, detector_type, localmax_range_xy, localmax_range_z, tp_threshold_xy, tp_threshold_z)    

        tpr_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fn_sample_spot_vec)            
            
        pd_mean_spot_vec[i] = mean(tpr_sample_spot_vec)
        pd_sem_spot_vec[i] = sem(tpr_sample_spot_vec)            
            
        fpr_sample_spot_vec = fp_sample_spot_vec/(
            fp_sample_spot_vec+tn_sample_spot_vec)
        
        pfa_mean_spot_vec[i] = mean(fpr_sample_spot_vec)
        pfa_sem_spot_vec[i] = sem(fpr_sample_spot_vec)
    
        jaccard_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fp_sample_spot_vec+fn_sample_spot_vec)

        jaccard_mean_spot_vec[i] = mean(jaccard_sample_spot_vec)
        jaccard_sem_spot_vec[i] = sem(jaccard_sample_spot_vec)  
        
    return (pd_mean_spot_vec, pd_sem_spot_vec, 
            pfa_mean_spot_vec, pfa_sem_spot_vec,
            jaccard_mean_spot_vec, jaccard_sem_spot_vec)

#%%
def pfa_monte_carlo_filter3d(
        n_samples: int, 
        pfa: float,
        w: np.ndarray,
        b: float, 
        shape: tuple,
        detector_type, 
        localmax_range_xy: int,
        localmax_range_z: int,
        ) -> tuple:    
    
    # pfa_monte_carlo_filter3d: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images filtered by kernel 'w' on which 
    # the detection algorithm is tested
    
    # Parameters:
    # n_samples = number of Monte Carlo trials
    # pfa = probability of false alarm
    # w = filter kernel    
    # b = mean background value
    # shape = image shape 
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # local_max_range_xy = radius of local maximum in x-y
    # local_max_range_z = radius of local maximum in z

    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')            
    
    for i in range(n_samples):   

        # received signal
        r = np.random.poisson( np.full(shape, b) ) 

        # test statistic
        T = test_statistic3d(r, w)

        # detection
        mask = detector_type(T,pfa,w,localmax_range_xy,localmax_range_z)        
                        
        # evaluation
        localmax_points = mask2points(mask)               
               
        fp_vec[i] = localmax_points.shape[0]
        tn_vec[i] = np.prod(shape) - localmax_points.shape[0]
    
    return fp_vec, tn_vec

#%%
def pfa_vec_monte_carlo_filter3d(
        pfa_vec: list, 
        n_samples: int, 
        w: np.ndarray,
        b: float, 
        shape: tuple,
        detector_type, 
        localmax_range_xy: int,
        localmax_range_z: int,
        ) -> tuple:

    # pfa_vec_monte_carlo_filter3d: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images on which the detection 
    # algorithm is tested for a list of pfa values
    
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # n_samples = number of Monte Carlo trials
    # w = filter kernel  
    # b = mean background value
    # shape = image shape 
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # local_max_range_xy = radius of local maximum in x-y
    # local_max_range_z = radius of local maximum in z

    n_pfa = len(pfa_vec)  
    
    pfa_mean_bkg_vec = np.empty(n_pfa)
    pfa_sem_bkg_vec = np.empty(n_pfa)

    for i, pfa in enumerate(tqdm(pfa_vec)):        
        
        (fp_sample_bkg_vec, tn_sample_bkg_vec) = pfa_monte_carlo_filter3d(
            n_samples, pfa, w, b, shape, detector_type, localmax_range_xy, localmax_range_z)    

        fpr_sample_bkg_vec = fp_sample_bkg_vec/(
            fp_sample_bkg_vec+tn_sample_bkg_vec)
        
        pfa_mean_bkg_vec[i] = mean(fpr_sample_bkg_vec)
        pfa_sem_bkg_vec[i] = sem(fpr_sample_bkg_vec)
        
    return (pfa_mean_bkg_vec, pfa_sem_bkg_vec)

#%%
def get_pmf3d(
        a: float, 
        b: float, 
        psf_fun,
        psf_size_xy: int, 
        psf_size_z: int, 
        sigma_xy: float,
        sigma_z: float,
        )-> np.ndarray:

    # get_pmf3d: Returns Poisson matched filter with PSF function given by 
    # parameter 'psf_fun' in 3D
    
    # Parameters:
    # a = molecule photon count
    # b = mean background value
    # psf_fun = point spread function model
    # psf_size_xy = size of the filter kernel in x-y
    # psf_size_z = size of the filter kernel in z    
    # sigma_xy = standard deviation of the molecule in x-y plane
    # sigma_z = standard deviation of the molecule in z plane
    
    psf = get_psf3d(psf_fun, psf_size_xy, psf_size_z, sigma_xy, sigma_z)

    pmf = log(1+a/b*psf)
    pmf = pmf/sum(pmf)

    return pmf





