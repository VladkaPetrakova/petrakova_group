# MH, v1.2, 2024_10_08

#%% lib
import numpy as np
from scipy import signal
from numpy import sum, median, sqrt
from scipy.stats import norm
from scipy.ndimage import convolve
from skimage import filters

from detection_tools import remove_nonlocal_maxima

#%%
def test_statistic(image: np.ndarray, w: np.ndarray) -> np.ndarray:
    # test_statistic: Returns test statistic which is a filtered image
    # Parameters:
    # image = image to be filtered
    # w = filter kernel
    
    T = convolve(image.astype('float'), w, mode='mirror') 
    return T

#%%
def test_statistic_fft(image: np.ndarray, w: np.ndarray) -> np.ndarray:
    # test_statistic: Returns test statistic which is a filtered 
    # image implemented efficiently by FFT
    # Parameters:
    # image = image to be filtered
    # w = filter kernel
    
    image_pad = np.pad(image, (w.shape[0]-1)//2, mode='reflect')
    T = signal.fftconvolve( image_pad.astype('float'), w, mode='valid')     
    return T

#%%
def cfar_background_mean_estimate(T: np.ndarray, w: np.ndarray) -> float: 
    # cfar_background_mean_estimate: Returns global mean background level 
    # estimate given by median operation normalized by kernel sum
    # Parameters:
    # T = filtered image containing test statistic from which background mean is estimated
    
    return median(T)/sum(w)

#%%
def isf_threshold_norm(pfa: float, b: float, w: np.ndarray) -> float:
    # isf_threshold_norm: Returns inverse survival function (ISF) threshold for a
    # Gaussian distribution of filtered data
    # Parameters:
    # pfa = probability of false alarm
    # b = mean background level
    # w = filter kernel

    t0_mean = sum(w)*b
    t0_var = sum(w**2)*b
    
    tau = norm.isf(pfa, loc=t0_mean, scale=sqrt(t0_var))

    return tau

#%%
def cfar_segmentation(T: np.ndarray, pfa: float, w: np.ndarray) -> np.ndarray:
    # cfar_segmentation: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold
    # Parameters:
    # T = filtered image 
    # pfa = probability of false alarm    
    # w = filter kernel

    b_estimate = cfar_background_mean_estimate(T, w)  
    tau = isf_threshold_norm(pfa, b_estimate, w)
    mask = T > tau
    return mask

#%%
def cfar(
        T: np.ndarray, pfa: float,
        w: np.ndarray, local_max_range: int) -> np.ndarray:    
    # cfar: Returns binary mask segmentating filtered pixels which are
    # above global constant false alarm rate (cfar) isf threshold and which 
    # form local maximum
    # Parameters:
    # T = filtered image 
    # pfa = probability of false alarm
    # w = filter kernel    
    # local_max_range = range over which local maximum is searched

    segmentation = cfar_segmentation(T, pfa, w)    
    mask = remove_nonlocal_maxima(segmentation, T, local_max_range)    
    return mask

#%%
def cacfar_background_mean_estimate(
        T: np.ndarray, w: np.ndarray, kernel: np.ndarray) -> np.ndarray:   
    
    # cacfar_background_mean_estimate: Returns local mean background level 
    # estimate from filtered image given by arithmetic mean in 
    # neighborhood given by the kernel
    # Parameters:
    # T = filtered image containing test statistic from which background mean is estimated
    # w = filter kernel    
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed    
    
    b_estimate = convolve(T.astype('float'), kernel/sum(kernel))/sum(w)
    return b_estimate

#%%
def cacfar_segmentation(
        T: np.ndarray, pfa: float, w: np.ndarray, kernel: np.ndarray) -> np.ndarray:      
    # cacfar_segmentation: Returns binary mask segmentating filtered pixels 
    # which are above cell-averaging constant false alarm rate (ca-cfar) 
    # isf threshold, where the background estimate is given by local
    # arithmetic mean
    # Parameters:
    # T = filtered image 
    # pfa = probability of false alarm
    # w = filter kernel    
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    b_estimate = cacfar_background_mean_estimate(T, w, kernel)                   
    tau =  isf_threshold_norm(pfa, b_estimate, w)
    mask = T > tau
    return mask

#%%
def cacfar(
        T: np.ndarray, pfa: float, w: np.ndarray, 
        local_max_range: int, kernel: np.ndarray) -> np.ndarray:
    # cacfar: Returns binary mask segmentating filtered pixels which are
    # above cell-averaging constant false alarm rate (ca-cfar) isf 
    # threshold and which form local maximum, where the background estimate
    # is given by local arithmetic mean
    # Parameters:
    # T = filtered image 
    # pfa = probability of false alarm
    # w = filter kernel        
    # local_max_range = range over which local maximum is searched
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed    

    segmentation = cacfar_segmentation(T, pfa, w, kernel)        
    mask = remove_nonlocal_maxima(segmentation, T, local_max_range)        
    return mask

#%%
def oscfar_background_mean_estimate(
        T: np.ndarray, w: np.ndarray, kernel: np.ndarray) -> np.ndarray:      
    # oscfar_background_mean_estimate: Returns local mean background level 
    # estimate from filtered image given by median in neighborhood 
    # given by the kernel
    # Parameters:
    # T = filtered image containing test statistic from which background mean is estimated
    # w = filter kernel        
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed

    b_estimate = filters.median(T,
        footprint=kernel, mode='reflect', out='f')/sum(w)     
        
    return b_estimate

#%%
def oscfar_segmentation(
        T: np.ndarray, pfa: float, 
        w: np.ndarray, kernel: np.ndarray) -> np.ndarray:         
    # oscfar_segmentation: Returns binary mask segmentating filtered pixels 
    # which are above ordered-statistics constant false alarm rate (os-cfar) 
    # isf threshold, where the background estimate is given by local median
    # Parameters:
    # T = filtered image 
    # pfa = probability of false alarm
    # w = filter kernel        
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed    
    
    b_estimate = oscfar_background_mean_estimate(T, w, kernel)    
    tau =  isf_threshold_norm(pfa, b_estimate, w)
    mask = T > tau
    return mask

#%%
def oscfar(
        T: np.ndarray, pfa: float, 
        w: np.ndarray, local_max_range: int, kernel: np.ndarray) -> np.ndarray:    

    # oscfar: Returns binary mask segmentating filtered pixels which are
    # above  ordered-statistics constant false alarm rate (os-cfar) isf 
    # threshold and which form local maximum, where the background estimate
    # is given by local median
    # Parameters:
    # T = filtered image 
    # pfa = probability of false alarm
    # w = filter kernel            
    # local_max_range = range over which local maximum is searched
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed
      
    mask_nonlocal_max = oscfar_segmentation(T, pfa, w, kernel)        
    mask = remove_nonlocal_maxima(
        mask_nonlocal_max, T, local_max_range)        
    return mask

#%%
def jaccard_pfa_theory(
        a: float, b: float, s: np.ndarray, 
        w: np.ndarray, p0: float, pfa: float) -> float:
    
    # jaccard_pfa_theory: Returns theoretical Jaccard Index value 
    # Parameters:
    # a = photon count
    # b = mean background value
    # s = point spread function
    # w = filter kernel
    # p0 = a priori probability of signal absent event
    # pfa = probability of false alarm
    
    p1 = 1-p0        

    t0_mean = sum(w)*b
    t0_var = sum(w**2)*b

    t1_mean = a*sum(s*w)+b*sum(w)
    t1_var = a*sum(s*w**2)+b*sum(w**2)

    tau = norm.isf(pfa, loc=t0_mean, scale=sqrt(t0_var))
    
    tpr = norm.sf(tau, loc=t1_mean, scale=sqrt(t1_var) )
    fpr = norm.sf(tau, loc=t0_mean, scale=sqrt(t0_var))
    
    return tpr*p1 / (p1+fpr*p0) 














