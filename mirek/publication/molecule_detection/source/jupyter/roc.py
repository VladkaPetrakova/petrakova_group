# MH, v1.1, 2024_10_09

import numpy as np

#%%
def load_variable(
        var_name: str, 
        dictionary: dict
        ) -> None:
    
    # load_variable: Load variables into 'dictionary'
    
    dictionary[var_name] = np.loadtxt(
        dictionary['directory']/(var_name+'.txt')).astype('float')  