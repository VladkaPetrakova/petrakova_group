# MH, v1.1, 2024_10_09

#%% lib
from scipy.signal import convolve2d
from numpy import sqrt, mean
from tqdm import tqdm
import numpy as np

#%%
from signal_generation import gauss2d
from signal_generation import get_uniform_background
from signal_generation import get_nonuniform_background
from signal_generation import random_int_coordinates
from signal_generation import random_coordinates
from signal_generation import get_single_spot
from signal_generation import poisson_noise
from signal_generation import awgn_noise

from detection_tools import mask2points
from performance import evaluate_single_emitter_detections
from performance import sem

#%%
def test_statistic(
        r: np.ndarray, 
        G: np.ndarray,
        H: np.ndarray,
        I: np.ndarray
        )-> np.ndarray: 
    
    # test_statistic: Returns test statistics for net gradient detection method
    # Parameters:
    # r = received 2D signal
    # G = horizontal edge filter kernel
    # H = vertical edge filter kernel
    # I = local mean filter kernel

    x = convolve2d(r, G, boundary='symm', mode='same')    
    y = convolve2d(r, H, boundary='symm', mode='same')
    
    z = sqrt(x**2 + y**2)    
    
    w = convolve2d(z, I, boundary='symm', mode='same')
    
    return w

#%%
def pd_monte_carlo_netgrad(
    pfa: float,
    G: np.ndarray,
    H: np.ndarray,
    I: np.ndarray,
    n_samples: int,
    a: float, 
    all_dict: dict,
    detector_type,
    kernel: np.ndarray=None, 
    background_type: str='uniform',
    psf_fun=gauss2d,
    tp_threshold: float=sqrt(2), 
    add_awgn_noise: bool=False,
    int_coordinates: bool=False, 
    local_max_range: int=None, 
    seed: int=None, 
    **kwargs
    ) -> tuple:    
    # pd_monte_carlo_netgrad: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images on which the detection 
    # algorithm is tested
    # Parameters:
    # pfa = probability of false alarm
    # G = horizontal edge filter kernel
    # H = vertical edge filter kernel
    # I = local mean filter kernel    
    # n_samples = number of Monte Carlo trials
    # a = molecule photon count
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator
    # **kwargs = key word agruments for background generation function

    if seed is not None:
        np.random.seed(seed)     
    
    if local_max_range is None:
        local_max_range = all_dict['detector_dict']['local_max_range']
        
    background_dict = all_dict['background_dict']    
    size = background_dict['size']
    multispot_margin = all_dict['signal_dict']['multispot_margin']
    sigma = all_dict['signal_dict']['sigma']    
    sigma_n = background_dict['sigma_n']        
    shape = background_dict['shape']
       
    if background_type == 'uniform':
        background = get_uniform_background(background_dict, **kwargs)
    elif background_type == 'non-uniform':
        background = get_nonuniform_background(
            background_dict, **kwargs)
    
    if int_coordinates:
        coordinates_generator = random_int_coordinates
    else:
        coordinates_generator = random_coordinates
        
    tp_vec = np.zeros(n_samples, dtype='int')
    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')        
    fn_vec = np.zeros(n_samples, dtype='int')                
        
    for i in range(n_samples):   
    
        x0, y0 = coordinates_generator(multispot_margin, size)
    
        single_spot = get_single_spot(x0, y0, psf_fun, sigma, a, size)

        r = poisson_noise(single_spot + background)    
        
        if add_awgn_noise:
            r = r + awgn_noise(sigma_n, shape)  
            
        T = test_statistic(r, G, H, I)         
        
        if kernel is None:
            mask = detector_type(T, pfa, local_max_range)
        else:           
            mask = detector_type(T, pfa, local_max_range, kernel)        
  
        points = mask2points(mask)        
        
        p0 = np.array([x0, y0])        
        
        tp, fp, tn, fn = evaluate_single_emitter_detections(
            points, p0, tp_threshold, shape)
               
        tp_vec[i] = tp
        fp_vec[i] = fp
        tn_vec[i] = tn
        fn_vec[i] = fn
    
    return tp_vec, fp_vec, tn_vec, fn_vec

#%%
def pfa_monte_carlo_netgrad(
    pfa: float,
    G: np.ndarray,
    H: np.ndarray,
    I: np.ndarray,
    n_samples: int, 
    all_dict: dict, 
    detector_type,
    kernel: np.ndarray=None, 
    background_type: str='uniform', 
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    seed: int=None,
    **kwargs
    ) -> tuple:          
    # pfa_monte_carlo_netgrad: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images on which the detection 
    # algorithm is tested
    # Parameters:
    # pfa = probability of false alarm
    # G = horizontal edge filter kernel
    # H = vertical edge filter kernel
    # I = local mean filter kernel 
    # n_samples = number of Monte Carlo trials
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator    
    # **kwargs = key word agruments for background generation function

    if seed is not None:
        np.random.seed(seed)     
    
    if local_max_range is None:
        local_max_range = all_dict['detector_dict']['local_max_range']
        
    background_dict = all_dict['background_dict']  
    sigma_n = background_dict['sigma_n']            
    shape = background_dict['shape']

    if background_type == 'uniform':
        background = get_uniform_background(background_dict, **kwargs)        
    elif background_type == 'non-uniform':
        background = get_nonuniform_background(
            background_dict, **kwargs)
        
    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')
    
    for i in range(n_samples):   
           
        r = poisson_noise(background)    
                
        if add_awgn_noise:
            r = r + awgn_noise(sigma_n, shape) 
            
        T = test_statistic(r, G, H, I)  
                
        if kernel is None:
            mask = detector_type(T, pfa, local_max_range)
        else:           
            mask = detector_type(T, pfa, local_max_range, kernel)        
                                    
        points = mask2points(mask)        
                            
        fp_vec[i] = points.shape[0]
        tn_vec[i] = np.prod(shape) - points.shape[0]
            
    return fp_vec, tn_vec

#%%
def pd_vec_monte_carlo_netgrad(
    pfa_vec: list, 
    G: np.ndarray,
    H: np.ndarray,
    I: np.ndarray,
    n_samples: int, 
    a: float, 
    all_dict: dict,
    detector_type, 
    psf_fun=gauss2d,
    kernel: np.ndarray=None,
    background_type: str='uniform', 
    tp_threshold: float=sqrt(2), 
    int_coordinates: bool=False,
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    seed: int=None,
    **kwargs
    ) -> tuple: 

    # pd_vec_monte_carlo_netgrad: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images on which the detection 
    # algorithm is tested for a list of pfa values
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # G = horizontal edge filter kernel
    # H = vertical edge filter kernel
    # I = local mean filter kernel 
    # n_samples = number of Monte Carlo trials
    # a = molecule photon count
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator
    # **kwargs = key word agruments for 'pd_monte_carlo' function  

    n_pfa = len(pfa_vec)  
    
    pd_mean_spot_vec = np.empty(n_pfa)
    pd_sem_spot_vec = np.empty(n_pfa)
    pfa_mean_spot_vec = np.empty(n_pfa)
    pfa_sem_spot_vec = np.empty(n_pfa)
    jaccard_mean_spot_vec = np.empty(n_pfa)
    jaccard_sem_spot_vec = np.empty(n_pfa)   
    
    for i, pfa in enumerate(tqdm(pfa_vec)):        
        
        (tp_sample_spot_vec, fp_sample_spot_vec, 
         tn_sample_spot_vec,fn_sample_spot_vec)=pd_monte_carlo_netgrad(
            pfa, G, H, I, 
            n_samples, a, all_dict, detector_type, 
            kernel=kernel, 
            background_type=background_type,
            psf_fun=psf_fun, 
            tp_threshold=tp_threshold, 
            add_awgn_noise=add_awgn_noise, 
            int_coordinates=int_coordinates,
            local_max_range=local_max_range, 
            seed=seed, **kwargs)

        tpr_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fn_sample_spot_vec)            
            
        pd_mean_spot_vec[i] = mean(tpr_sample_spot_vec)
        pd_sem_spot_vec[i] = sem(tpr_sample_spot_vec)            
            
        fpr_sample_spot_vec = fp_sample_spot_vec/(
            fp_sample_spot_vec+tn_sample_spot_vec)
        
        pfa_mean_spot_vec[i] = mean(fpr_sample_spot_vec)
        pfa_sem_spot_vec[i] = sem(fpr_sample_spot_vec)
    
        jaccard_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fp_sample_spot_vec+fn_sample_spot_vec)

        jaccard_mean_spot_vec[i] = mean(jaccard_sample_spot_vec)
        jaccard_sem_spot_vec[i] = sem(jaccard_sample_spot_vec)     
    
    return (pd_mean_spot_vec, pd_sem_spot_vec, 
            pfa_mean_spot_vec, pfa_sem_spot_vec,
            jaccard_mean_spot_vec, jaccard_sem_spot_vec)

#%%
def pfa_vec_monte_carlo_netgrad(
    pfa_vec: list, 
    G: np.ndarray,
    H: np.ndarray,
    I: np.ndarray,
    n_samples: int, 
    all_dict: dict, 
    detector_type, 
    kernel: np.ndarray=None, 
    background_type: str='uniform', 
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    seed: int=None, 
    **kwargs
    ) -> tuple:           
    # pfa_vec_monte_carlo_netgrad: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images on which the detection 
    # algorithm is tested for a list of pfa values
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # G = horizontal edge filter kernel
    # H = vertical edge filter kernel
    # I = local mean filter kernel
    # n_samples = number of Monte Carlo trials
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator    
    # **kwargs = key word agruments for 'pfa_monte_carlo' function
    
    n_pfa = len(pfa_vec)  

    pfa_mean_bkg_vec = np.empty(n_pfa)
    pfa_sem_bkg_vec = np.empty(n_pfa)
    
    for i, pfa in enumerate(tqdm(pfa_vec)):        

        fp_sample_bkg_vec, tn_sample_bkg_vec = pfa_monte_carlo_netgrad(
            pfa, G, H, I,
            n_samples, all_dict, detector_type,
            kernel=kernel, background_type=background_type, 
            local_max_range=local_max_range, 
            add_awgn_noise=add_awgn_noise,  
            seed=seed, **kwargs)
        
        fpr_sample_bkg_vec = fp_sample_bkg_vec/(
            fp_sample_bkg_vec+tn_sample_bkg_vec)
        
        pfa_mean_bkg_vec[i] = mean(fpr_sample_bkg_vec)
        pfa_sem_bkg_vec[i] = sem(fpr_sample_bkg_vec)
        
    return pfa_mean_bkg_vec, pfa_sem_bkg_vec







