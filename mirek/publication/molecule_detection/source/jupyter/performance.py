# MH, v1.1, 2024_10_08

#%% lib
import numpy as np
from tqdm import tqdm
from numpy import min, sum, mean, std, sqrt

#%%
from signal_generation import gauss2d
from signal_generation import get_uniform_background
from signal_generation import get_nonuniform_background
from signal_generation import random_int_coordinates
from signal_generation import random_coordinates
from signal_generation import get_single_spot
from signal_generation import poisson_noise
from signal_generation import awgn_noise

from detection_tools import mask2points

from poisson_pmf import test_statistic
        
#%%
def sem(array: np.ndarray) -> float: 
    # sem: Returns standard error of the mean of the 'array'
    
    return std(array)/sqrt(len(array))

#%%
def evaluate_single_emitter_detections(
        points: np.ndarray, 
        p0: np.ndarray, 
        tp_threshold: float, 
        shape: tuple
        ) -> tuple: 
    # evaluate_single_emitter_detections: Counts number of true positive,
    # false positive, true negative, false negative detections for a single 
    # emitter data
    # Parameters:
    # points = array of x-y coordinates of marked detections
    # p0 = x-y coordinate of true molecule position
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # shape = image shape 
        
    distances = [np.linalg.norm(p-p0) for p in points]
    n_pixels = int(np.prod(shape))
    n_points = len(distances)
    
    tp = 0; fp = 0; tn=0; fn = 0
    
    if n_points > 0:        
        if( min(distances) <= tp_threshold ):            
            tp = 1
            fp = n_points-1       
            tn = n_pixels-n_points
        else:
            fp = n_points
            fn = 1
            tn = n_pixels-1-n_points
    else:        
        fn = 1
        tn = n_pixels-1
    return tp, fp, tn, fn

#%%
def pd_monte_carlo(
    pfa: float,
    n_samples: int,
    a: float, 
    all_dict: dict,
    detector_type, 
    kernel: np.ndarray=None, 
    background_type: str='uniform',
    psf_fun=gauss2d, 
    tp_threshold: float=sqrt(2), 
    int_coordinates: bool=False, 
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    seed: int=None, 
    **kwargs
    ) -> tuple:    
    # pd_monte_carlo: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images on which the detection 
    # algorithm is tested
    # Parameters:
    # pfa = probability of false alarm
    # n_samples = number of Monte Carlo trials
    # a = molecule photon count
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator
    # **kwargs = key word agruments for background generation function

    if seed is not None:
        np.random.seed(seed)     
    
    if local_max_range is None:
        local_max_range = all_dict['detector_dict']['local_max_range']
        
    background_dict = all_dict['background_dict']    
    size = background_dict['size']
    multispot_margin = all_dict['signal_dict']['multispot_margin']
    sigma = all_dict['signal_dict']['sigma']    
    sigma_n = background_dict['sigma_n']
    shape = background_dict['shape']
        
    if background_type == 'uniform':
        background = get_uniform_background(background_dict, **kwargs)
    elif background_type == 'non-uniform':
        background = get_nonuniform_background(
            background_dict, **kwargs)        
    
    if int_coordinates:
        coordinates_generator = random_int_coordinates
    else:
        coordinates_generator = random_coordinates
        
    tp_vec = np.zeros(n_samples, dtype='int')
    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')        
    fn_vec = np.zeros(n_samples, dtype='int')        
        
    for i in range(n_samples):   
    
        # create signal
        x0, y0 = coordinates_generator(multispot_margin, size)
    
        single_spot = get_single_spot(x0, y0, psf_fun, sigma, a, size)
    
        r = poisson_noise(single_spot + background)
        
        if add_awgn_noise:
            r = r + awgn_noise(sigma_n, shape)

        # detection
        if kernel is None:
            mask = detector_type(r, pfa, local_max_range)
        else:           
            mask = detector_type(r, pfa, local_max_range, kernel)        
  
        # evaluation
        points = mask2points(mask)        
        
        p0 = np.array([x0, y0])        
        
        tp, fp, tn, fn = evaluate_single_emitter_detections(
            points, p0, tp_threshold, shape)
               
        tp_vec[i] = tp
        fp_vec[i] = fp
        tn_vec[i] = tn
        fn_vec[i] = fn
    
    return tp_vec, fp_vec, tn_vec, fn_vec

#%%
def pfa_monte_carlo(
    pfa: float,
    n_samples: int, 
    all_dict: dict, 
    detector_type,
    kernel: np.ndarray=None, 
    background_type: str='uniform', 
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    seed: int=None,
    **kwargs
    ) -> tuple:          
    # pfa_monte_carlo: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images on which the detection 
    # algorithm is tested
    # Parameters:
    # pfa = probability of false alarm
    # n_samples = number of Monte Carlo trials
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator    
    # **kwargs = key word agruments for background generation function

    if seed is not None:
        np.random.seed(seed)     
    
    if local_max_range is None:
        local_max_range = all_dict['detector_dict']['local_max_range']
        
    background_dict = all_dict['background_dict']    
    sigma_n = background_dict['sigma_n']
    shape = background_dict['shape']
    
    if background_type == 'uniform':
        background = get_uniform_background(background_dict, **kwargs)        
    elif background_type == 'non-uniform':
        background = get_nonuniform_background(
            background_dict, **kwargs)
        
    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')
           
    for i in range(n_samples):   
           
        r = poisson_noise(background)
        
        if add_awgn_noise:
            r = r + awgn_noise(sigma_n, shape)
            
        if kernel is None:
            mask = detector_type(r, pfa, local_max_range)
        else:           
            mask = detector_type(r, pfa, local_max_range, kernel)        
                                    
        points = mask2points(mask)        
                            
        fp_vec[i] = points.shape[0]
        tn_vec[i] = np.prod(shape) - points.shape[0]
            
    return fp_vec, tn_vec

#%%
def pd_vec_monte_carlo(
    pfa_vec: list, 
    n_samples: int, 
    a: float, 
    all_dict: dict,
    detector_type, 
    kernel: np.ndarray=None,
    background_type: str='uniform', 
    psf_fun=gauss2d, 
    tp_threshold: float=sqrt(2), 
    int_coordinates: bool=False,
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    seed: int=None,
    **kwargs
    ) -> tuple: 
           
    # pd_vec_monte_carlo: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images on which the detection 
    # algorithm is tested for a list of pfa values
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # n_samples = number of Monte Carlo trials
    # a = molecule photon count
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator
    # **kwargs = key word agruments for 'pd_monte_carlo' function    
    
    n_pfa = len(pfa_vec)  
    
    pd_mean_spot_vec = np.empty(n_pfa)
    pd_sem_spot_vec = np.empty(n_pfa)
    pfa_mean_spot_vec = np.empty(n_pfa)
    pfa_sem_spot_vec = np.empty(n_pfa)
    jaccard_mean_spot_vec = np.empty(n_pfa)
    jaccard_sem_spot_vec = np.empty(n_pfa)    
    
    for i, pfa in enumerate(tqdm(pfa_vec)):        
        
        (tp_sample_spot_vec, fp_sample_spot_vec, 
             tn_sample_spot_vec, fn_sample_spot_vec) = pd_monte_carlo(
                pfa, n_samples, a, all_dict, detector_type, 
                kernel=kernel, background_type=background_type,
                psf_fun=psf_fun, tp_threshold=tp_threshold, 
                int_coordinates=int_coordinates,
                local_max_range=local_max_range,
                add_awgn_noise=add_awgn_noise,
                seed=seed, **kwargs)
        
        tpr_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fn_sample_spot_vec)
        
        pd_mean_spot_vec[i] = mean(tpr_sample_spot_vec)
        pd_sem_spot_vec[i] = sem(tpr_sample_spot_vec)

        fpr_sample_spot_vec = fp_sample_spot_vec/(
            fp_sample_spot_vec+tn_sample_spot_vec)

        pfa_mean_spot_vec[i] = mean(fpr_sample_spot_vec)
        pfa_sem_spot_vec[i] = sem(fpr_sample_spot_vec)
    
        jaccard_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fp_sample_spot_vec+fn_sample_spot_vec)

        jaccard_mean_spot_vec[i] = mean(jaccard_sample_spot_vec)
        jaccard_sem_spot_vec[i] = sem(jaccard_sample_spot_vec)        
        
    return (pd_mean_spot_vec, pd_sem_spot_vec, 
            pfa_mean_spot_vec, pfa_sem_spot_vec,
            jaccard_mean_spot_vec, jaccard_sem_spot_vec)

#%%
def pfa_vec_monte_carlo(
    pfa_vec: list, 
    n_samples: int, 
    all_dict: dict, 
    detector_type, 
    kernel: np.ndarray=None, 
    background_type: str='uniform', 
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    seed: int=None, 
    **kwargs
    ) -> tuple:           
    # pfa_vec_monte_carlo: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images on which the detection 
    # algorithm is tested for a list of pfa values
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # n_samples = number of Monte Carlo trials
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator    
    # **kwargs = key word agruments for 'pfa_monte_carlo' function    
    
    n_pfa = len(pfa_vec)  

    pfa_mean_bkg_vec = np.empty(n_pfa)
    pfa_sem_bkg_vec = np.empty(n_pfa)
    
    for i, pfa in enumerate(tqdm(pfa_vec)):        

        fp_sample_bkg_vec, tn_sample_bkg_vec = pfa_monte_carlo(
            pfa, n_samples, all_dict, detector_type,
            kernel=kernel, background_type=background_type, 
            local_max_range=local_max_range,
            add_awgn_noise=add_awgn_noise,
            seed=seed, **kwargs)
                
        fpr_sample_bkg_vec = fp_sample_bkg_vec/(
            fp_sample_bkg_vec+tn_sample_bkg_vec)
        
        pfa_mean_bkg_vec[i] = mean(fpr_sample_bkg_vec)
        pfa_sem_bkg_vec[i] = sem(fpr_sample_bkg_vec)
        
    return pfa_mean_bkg_vec, pfa_sem_bkg_vec

#%%
def evaluate_points(
    gt_points: np.ndarray, pr_points: np.ndarray, 
    gt_points_assigned: np.ndarray, pr_points_assigned: np.ndarray, 
    tp_threshold: 'float', shape: tuple) -> tuple:
    # evaluate_points: 
        
    # evaluate_points: Counts number of true positive,
    # false positive, true negative, false negative detections based on 
    # ground true molecule positions, predicted molecule positions, and 
    # molecule positions that were paired by linear_sum_assignment algorithm
    # Parameters:
    # gt_points = ground true molecule positions
    # pr_points = predicted molecule positions
    # gt_points_assigned = ground true molecule positions paired by linear_sum_assignment algorithm
    # pr_points_assigned = predicted molecule positions   paired by linear_sum_assignment algorithm 
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # shape = image shape         
        
    n_pairs = gt_points_assigned.shape[0]
    n_pixels = np.prod(shape)
    
    distances = np.zeros(n_pairs)
    for i in range(n_pairs):    
        distances[i] = np.linalg.norm(
            pr_points_assigned[i] - gt_points_assigned[i] )
    
    # paired points
    tp = 0; fp = 0; tn=0; fn = 0    
    for d in distances:
        if d <= tp_threshold:
            tp += 1
        else:
            fp += 1
            fn += 1    
    
    # un-paired points
    fn += len(gt_points) - len(gt_points_assigned)
    fp += len(pr_points) - len(pr_points_assigned)

    # the rest is tn
    tn += n_pixels - sum( (tp,fp,tn,fn) )
    
    return tp,fp,tn,fn

#%%
def get_tpr(tp_vec: np.ndarray, fn_vec: np.ndarray) -> np.ndarray:
    # get_tpr: Computes vector of true positive rates from vector of true 
    # positives and false negatives
    # Prameters:
    # tp_vec = vector of true positives
    # fn_vec = vector of false negatives
    
    n_frames = len(tp_vec)
    
    tpr_vec = np.zeros(n_frames)
    
    for i in range(n_frames):
        if( tp_vec[i]+fn_vec[i] == 0 ):        
            tpr_vec[i] = None
        else:
            tpr_vec[i] = tp_vec[i] / (tp_vec[i]+fn_vec[i])
            
    tpr_vec = tpr_vec[ ~np.isnan(tpr_vec)]
            
    return tpr_vec

#%%
def get_fpr(fp_vec: np.ndarray, tn_vec: np.ndarray) -> np.ndarray:
    # get_fpr: Computes vector of false positive rates from vector of false 
    # positives and true negatives
    # Prameters:
    # fp_vec = vector of false positives
    # tn_vec = vector of true negatives

    n_frames = len(fp_vec)
    
    fpr_vec = np.zeros(n_frames)
    
    for i in range(n_frames):
        if( fp_vec[i]+tn_vec[i] == 0 ):        
            fpr_vec[i] = None
        else:
            fpr_vec[i] = fp_vec[i] / (fp_vec[i]+tn_vec[i])
            
    fpr_vec = fpr_vec[ ~np.isnan(fpr_vec)]
            
    return fpr_vec

#%%
def pd_monte_carlo_filter(
        pfa: float,
        w: np.ndarray,
        n_samples: int,
        a: float, 
        all_dict: dict,
        detector_type, 
        psf_fun=gauss2d, 
        kernel: np.ndarray=None, 
        background_type: str='uniform',
        tp_threshold: float=sqrt(2), 
        add_awgn_noise: bool=False,
        int_coordinates: bool=False, 
        local_max_range: int=None, 
        is_detector_filter: bool=True,
        seed: int=None, 
        **kwargs
        ) -> tuple:        
    # pd_monte_carlo_filter: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images filtered by kernel 'w' 
    # on which the detection algorithm is tested
    # Parameters:
    # pfa = probability of false alarm
    # w = filter kernel    
    # n_samples = number of Monte Carlo trials
    # a = molecule photon count
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator
    # **kwargs = key word agruments for background generation function    
    
    if seed is not None:
        np.random.seed(seed)     
    
    if local_max_range is None:
        local_max_range = all_dict['detector_dict']['local_max_range']
        
    background_dict = all_dict['background_dict']    
    size = background_dict['size']
    multispot_margin = all_dict['signal_dict']['multispot_margin']
    sigma = all_dict['signal_dict']['sigma']   
    sigma_n = background_dict['sigma_n']        
    shape = background_dict['shape']
        
    if background_type == 'uniform':
        background = get_uniform_background(background_dict, **kwargs)
    elif background_type == 'non-uniform':
        background = get_nonuniform_background(
            background_dict, **kwargs)
    
    if int_coordinates:
        coordinates_generator = random_int_coordinates
    else:
        coordinates_generator = random_coordinates        
        
    tp_vec = np.zeros(n_samples, dtype='int')
    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')            
    fn_vec = np.zeros(n_samples, dtype='int')
        
    for i in range(n_samples):   

        # create signal
        x0, y0 = coordinates_generator(multispot_margin, size)
    
        single_spot = get_single_spot(x0, y0, psf_fun, sigma, a, size)

        r = poisson_noise(single_spot + background) 
                  
        if add_awgn_noise:
            r = r + awgn_noise(sigma_n, shape)         
        
        T = test_statistic(r, w)         

        # detection
        if is_detector_filter:
            if kernel is None:
                mask = detector_type(T,pfa,w,local_max_range)
            else:           
                mask = detector_type(T,pfa,w,local_max_range,kernel)        
        else:
            if kernel is None:
                mask = detector_type(T,pfa,local_max_range)
            else:           
                mask = detector_type(T,pfa,local_max_range,kernel)        
                
        # evaluation
        points = mask2points(mask)        
        
        p0 = np.array([x0, y0])        
        
        tp, fp, tn, fn = evaluate_single_emitter_detections(
            points, p0, tp_threshold, shape)
               
        tp_vec[i] = tp
        fp_vec[i] = fp
        tn_vec[i] = tn
        fn_vec[i] = fn
    
    return tp_vec, fp_vec, tn_vec, fn_vec

#%%
def pfa_monte_carlo_filter(
        pfa: float,
        w: np.ndarray,
        n_samples: int, 
        all_dict: dict, 
        detector_type,
        kernel: np.ndarray=None, 
        background_type: str='uniform', 
        local_max_range: int=None, 
        add_awgn_noise: bool=False,
        is_detector_filter: bool=True,    
        seed: int=None,
        **kwargs
        ) -> tuple:          
    
    # pfa_monte_carlo_filter: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images filtered by kernel 'w' on which 
    # the detection algorithm is tested
    # Parameters:
    # pfa = probability of false alarm
    # w = filter kernel    
    # n_samples = number of Monte Carlo trials
    # a = molecule photon count
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator    
    # **kwargs = key word agruments for background generation function    
    
    if seed is not None:
        np.random.seed(seed)     
    
    if local_max_range is None:
        local_max_range = all_dict['detector_dict']['local_max_range']
        
    background_dict = all_dict['background_dict']  
    sigma_n = background_dict['sigma_n']            
    shape = background_dict['shape']
        
    if background_type == 'uniform':
        background = get_uniform_background(background_dict, **kwargs)        
    elif background_type == 'non-uniform':
        background = get_nonuniform_background(
            background_dict, **kwargs)
        
    fp_vec = np.zeros(n_samples, dtype='int')
    tn_vec = np.zeros(n_samples, dtype='int')

    for i in range(n_samples):   
           
        r = poisson_noise(background)  
        
        if add_awgn_noise:
            r = r + awgn_noise(sigma_n, shape)         
        
        T = test_statistic(r, w)  

                
        if is_detector_filter:
            if kernel is None:
                mask = detector_type(T,pfa,w,local_max_range)
            else:           
                mask = detector_type(T,pfa,w,local_max_range,kernel)        
        else:
            if kernel is None:
                mask = detector_type(T,pfa,local_max_range)
            else:           
                mask = detector_type(T,pfa,local_max_range,kernel)                        
                                    
        points = mask2points(mask)        
                            
        fp_vec[i] = points.shape[0]
        tn_vec[i] = np.prod(shape) - points.shape[0]
            
    return fp_vec, tn_vec

#%%
def pd_vec_monte_carlo_filter(
        pfa_vec: list, 
        w: np.ndarray,
        n_samples: int, 
        a: float, 
        all_dict: dict,
        detector_type, 
        psf_fun=gauss2d, 
        kernel: np.ndarray=None,
        background_type: str='uniform', 
        tp_threshold: float=sqrt(2), 
        int_coordinates: bool=False,
        local_max_range: int=None, 
        add_awgn_noise: bool=False,
        is_detector_filter: bool=True,  
        seed: int=None,
        **kwargs
        ) -> tuple: 
                      
    # pd_vec_monte_carlo_filter: Monte Carlo simulation which generates number of
    # 'n_samples' of random single molecule images filtered by kernel 'w' 
    # on which the detection algorithm is tested for a list of pfa values
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # w = filter kernel        
    # n_samples = number of Monte Carlo trials
    # a = molecule photon count
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # int_coordinates = True if molecule coordinates are generated as 
    # random integers
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator
    # **kwargs = key word agruments for 'pd_monte_carlo_filter' function  
    
    n_pfa = len(pfa_vec)  
    
    pd_mean_spot_vec = np.empty(n_pfa)
    pd_sem_spot_vec = np.empty(n_pfa)
    pfa_mean_spot_vec = np.empty(n_pfa)
    pfa_sem_spot_vec = np.empty(n_pfa)
    jaccard_mean_spot_vec = np.empty(n_pfa)
    jaccard_sem_spot_vec = np.empty(n_pfa)     

    for i, pfa in enumerate(tqdm(pfa_vec)):        
        
        (tp_sample_spot_vec, fp_sample_spot_vec, 
         tn_sample_spot_vec,fn_sample_spot_vec)=pd_monte_carlo_filter(
            pfa, w, n_samples, a, all_dict, detector_type, psf_fun, 
            kernel=kernel, background_type=background_type,
            tp_threshold=tp_threshold, 
            int_coordinates=int_coordinates,
            local_max_range=local_max_range, 
            add_awgn_noise=add_awgn_noise, 
            is_detector_filter=is_detector_filter,
            seed=seed, **kwargs)

        tpr_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fn_sample_spot_vec)            
            
        pd_mean_spot_vec[i] = mean(tpr_sample_spot_vec)
        pd_sem_spot_vec[i] = sem(tpr_sample_spot_vec)            
            
        fpr_sample_spot_vec = fp_sample_spot_vec/(
            fp_sample_spot_vec+tn_sample_spot_vec)
        
        pfa_mean_spot_vec[i] = mean(fpr_sample_spot_vec)
        pfa_sem_spot_vec[i] = sem(fpr_sample_spot_vec)
    
        jaccard_sample_spot_vec = tp_sample_spot_vec/(
            tp_sample_spot_vec+fp_sample_spot_vec+fn_sample_spot_vec)

        jaccard_mean_spot_vec[i] = mean(jaccard_sample_spot_vec)
        jaccard_sem_spot_vec[i] = sem(jaccard_sample_spot_vec)  
        
    return (pd_mean_spot_vec, pd_sem_spot_vec, 
            pfa_mean_spot_vec, pfa_sem_spot_vec,
            jaccard_mean_spot_vec, jaccard_sem_spot_vec)

#%%
def pfa_vec_monte_carlo_filter(
    pfa_vec: list, 
    w: np.ndarray,
    n_samples: int, 
    all_dict: dict, 
    detector_type, 
    kernel: np.ndarray=None, 
    background_type: str='uniform', 
    local_max_range: int=None, 
    add_awgn_noise: bool=False,
    is_detector_filter: bool=True,  
    seed: int=None, 
    **kwargs) -> tuple:           

    # pfa_vec_monte_carlo_filter: Monte Carlo simulation which generates number of
    # 'n_samples' of random background images on which the detection 
    # algorithm is tested for a list of pfa values
    # Parameters:
    # pfa_vec = list of false alarm probabilities
    # w = filter kernel            
    # n_samples = number of Monte Carlo trials
    # all_dict = dictionary with all simulation parameters
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    # background_type = type of bacground to be used, 'uniform' or 'non-uniform'
    # local_max_range = radius of local maximum
    # add_awgn_noise = if true, zero-mean read-out noise is added
    # seed = seed of the random number generator    
    # **kwargs = key word agruments for 'pfa_monte_carlo' function      
    
    n_pfa = len(pfa_vec)  

    pfa_mean_bkg_vec = np.empty(n_pfa)
    pfa_sem_bkg_vec = np.empty(n_pfa)
    
    for i, pfa in enumerate(tqdm(pfa_vec)):        

        fp_sample_bkg_vec, tn_sample_bkg_vec = pfa_monte_carlo_filter(
            pfa, w, n_samples, all_dict, detector_type,
            kernel=kernel, background_type=background_type, 
            local_max_range=local_max_range, 
            add_awgn_noise=add_awgn_noise,  
            is_detector_filter=is_detector_filter,            
            seed=seed, **kwargs)
        
        fpr_sample_bkg_vec = fp_sample_bkg_vec/(
            fp_sample_bkg_vec+tn_sample_bkg_vec)
        
        pfa_mean_bkg_vec[i] = mean(fpr_sample_bkg_vec)
        pfa_sem_bkg_vec[i] = sem(fpr_sample_bkg_vec)
        
    return pfa_mean_bkg_vec, pfa_sem_bkg_vec



