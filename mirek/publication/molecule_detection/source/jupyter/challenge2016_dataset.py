# MH, v1.1, 2024_10_09

#%%
import numpy as np
from tqdm import tqdm
import pandas as pd

from btls_dataset import nm2pxs
from detection_tools import get_detection_points
from linking import lsa
from performance import evaluate_points
from show import show_image

#%%
def get_positions_nm_challenge2016(df: pd, i_frame: int) -> np.ndarray:
    # get_positions_nm_challenge2016: Load ground true molecule position 
    # coordinates in nanometers
    # Parameters:
    # df = pandas data frame with localization data
    # i_frame = index of the frame to be shown

    positions_nm = df[df['frame']==(i_frame+1)][['ynano','xnano']].to_numpy()
        
    return positions_nm

#%%
def get_positions_challenge2016(df, i_frame, pixel_size_nm) -> np.ndarray:  
    # get_positions_challenge2016: Load ground true molecule position coordinates 
    # in pixels
    # Parameters:
    # df = pandas data frame with localization data
    # i_frame = index of the frame to be shown
    # pixel_size_nm = size of one pixel in nanometers

    positions_nm = get_positions_nm_challenge2016(df, i_frame)
    positions = nm2pxs(positions_nm, pixel_size_nm)
    
    return positions

#%%
def evaluate_frame_challenge2016(
    T: np.ndarray, 
    df_activations: pd, 
    i_frame: int, 
    detector_type, 
    pfa: float, 
    local_max_range: int, 
    tp_threshold: float, 
    dataset_dict: dict, 
    kernel: np.ndarray=None,
    ) -> tuple:
    # evaluate_frame_challenge2016: Evaluate predicted molecule detections in 
    # the ith frame
    # Parameters:
    # T = input image with test statistics
    # df_activations = pandas data frame with activation data
    # i_frame = index of the frame to be shown
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    positions_nm = get_positions_nm_challenge2016( df_activations, i_frame )
    gt_points = nm2pxs(positions_nm, dataset_dict['pixel_size_nm'])
    pr_points = get_detection_points(T, detector_type, pfa, local_max_range)

    # assignment
    gt_points_assigned, pr_points_assigned = lsa(gt_points, pr_points)

    tp,fp,tn,fn = evaluate_points(gt_points, pr_points, 
        gt_points_assigned, pr_points_assigned, 
        tp_threshold, dataset_dict['shape'])

    return tp,fp,tn,fn  

#%%
def show_frame_performance_challenge2016(
    i_frame: int, 
    T_all: np.ndarray, 
    df_activations: pd,
    detector_type, 
    pfa: float, 
    local_max_range: int, 
    dataset_dict: dict, 
    kernel: np.ndarray=None,
    figsize: tuple=(5,5)
    ) -> None:
    # show_frame_performance_challenge2016: Show predicted and ground true molecule 
    # positions over input image
    # Parameters:
    # i_frame = index of the frame to be shown
    # T_all = input datacube with test statistics for all frames
    # df_activations = pandas data frame with activation data
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    T = T_all[i_frame]
    
    positions_nm = get_positions_nm_challenge2016( df_activations, i_frame )
    gt_points = nm2pxs(positions_nm, dataset_dict['pixel_size_nm'])
    pr_points = get_detection_points(T, detector_type, pfa, local_max_range)
        
    show_image(T, points=gt_points, title='ground true', figsize=figsize)
    show_image(T, points=pr_points, title='predictions', figsize=figsize)
    
#%%
def evaluate_all_frames_challenge2016(
    T_all: np.ndarray, 
    df_activations: pd,
    detector_type, 
    pfa: float, 
    local_max_range: int, 
    tp_threshold: float, 
    n_frames: int, 
    dataset_dict: dict,
    kernel: np.ndarray=None,
    ) -> tuple:
    
    # evaluate_all_frames_challenge2016: Evaluate predicted molecule detections in all frames
    # Parameters:
    # T_all = input datacube with test statistics for all frames
    # df_activations = pandas data frame with activation data
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # n_frames = number of frames to be evaluated
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    
    tp_vec = np.zeros(n_frames)
    fp_vec = np.zeros(n_frames)
    tn_vec = np.zeros(n_frames)
    fn_vec = np.zeros(n_frames)    
    
    for i in tqdm(range(n_frames)):
    
        tp_vec[i], fp_vec[i], tn_vec[i], fn_vec[i] = evaluate_frame_challenge2016(
            T_all[i], df_activations, i,  
            detector_type, pfa, local_max_range, tp_threshold, 
            dataset_dict, kernel=kernel)   
    
    return  tp_vec, fp_vec, tn_vec, fn_vec