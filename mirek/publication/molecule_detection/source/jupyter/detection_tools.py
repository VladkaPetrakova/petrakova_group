# MH, v1.1, 2024_10_08

import numpy as np
from scipy.stats import norm
from numpy import pi, min, max
from skimage.morphology import square, disk

#%%
def photon2lambda(a: int) -> float: 
    # photon2lambda: Converts photon count to the peak value of 2D gaussian 
    # model of a single molecule fluorescence
    # Prameters:
    # a = photon count
    
    return np.round(np.array(a)/(2*pi)).astype(int)  

#%%
def mask2points(mask: np.ndarray) -> np.ndarray:
    # mask2points: Converts binary segmentation mask to a list of pixel 
    # x-y coordinates
    # Parameters:
    # mask = binary segmentation mask
    
    return np.array(np.where(mask), dtype='int32').T

#%%
def neigborhood(T: np.ndarray, point: np.ndarray, r: int) -> np.ndarray:   
    # neigborhood: Return 2D sub-array centered around 'point' with range 'r'
    # Parameters:
    # T = 2D image usually containing test statistic
    # point: x-y coordinate of the center
    # r = radius of sub-array    
    
    (x_max, y_max) = T.shape
    
    x0=max([point[0]-r,0])
    x1=min([point[0]+r+1,x_max])
    
    y0=max([point[1]-r,0])
    y1=min([point[1]+r+1,y_max])
    
    return T[x0:x1, y0:y1]

#%%
def is_local_max(T: np.ndarray, point: np.ndarray, r: int) -> bool:
    # is_local_max: Returns true if tested 'point' is a local maximum 
    # in the neighborhood of radius 'r'
    # Parameters:
    # T = 2D image usually containing test statistic
    # point: x-y coordinate of the center
    # r = radius of sub-array    

    return max(neigborhood(T,point,r)) <= T[point[0],point[1]]   

#%%
def get_local_max_points(
        T: np.ndarray, points: np.ndarray, local_max_range: int) -> np.ndarray:
    # get_local_max_points: Returns points of local maximum coordinates 
    # in 2D image 'T' selected from input points 'points'
    # Parameters:
    # T = 2D image usually containing test statistic
    # points: list of x-y coordinates
    # local_max_range = radius of local maximum

    local_max_points = np.array(
        [p for p in points if is_local_max(T, p, local_max_range)])
    
    return local_max_points

#%%
def points2mask(points: np.ndarray, size: int) -> np.ndarray:   
    # points2mask: Converts list of pixels to a binary segmentation mask 
    # x-y coordinates
    # Parameters:
    # points = list of pixels 
    # size = output shape
    
    mask = np.zeros((size,size))    
    for p in points:
        mask[p[0],p[1]]=1        
    return mask

#%%
def remove_nonlocal_maxima(
        segmentation: np.ndarray, 
        T: np.ndarray, local_max_range: int) -> np.ndarray:  
    # remove_nonlocal_maxima: Returns segmentation masks containing only 
    # pixels that form local maxima of the radius given by 'local_max_range'
    # Parameters:
    # segmentation = binary segmentation mask containing non-local maximum pixels
    # T = 2D image usually containing test statistic
    # local_max_range = radius of local maximum
    
    points = mask2points(segmentation)    
    points_local_max = get_local_max_points(
        # T + norm.rvs(loc=0, scale=10**-9, size=T.shape),
        segmentation*T + norm.rvs(loc=0, scale=10**-9, size=T.shape),        
        points, local_max_range)    
    size = T.shape[0]    
    mask = points2mask(points_local_max, size)
    return mask

#%%
def get_square_annulus(
        guard_interval: int, reference_interval: int) -> np.ndarray:
    # get_square_annulus: Returns square annulus kernel shape
    # Parameters:
    # guard_interval = range of internal hole
    # reference_interval = width of non-zero band
    
    kernel = square(2*(guard_interval+reference_interval)+1) - np.pad(
        square(2*guard_interval+1), pad_width=reference_interval )
    return kernel     

#%%
def get_disk_annulus(
    guard_interval: int, reference_interval: int) -> np.ndarray:
    # get_disk_annulus: Returns disk annulus kernel shape
    # Parameters:
    # guard_interval = range of internal hole
    # reference_interval = width of non-zero band
    
    kernel = disk(guard_interval+reference_interval) - np.pad(
        disk(guard_interval), pad_width=reference_interval )
    return kernel   

#%%
def get_detection_points(
    T: np.ndarray, detector_type, pfa: float, 
    local_max_range: int, kernel: np.ndarray=None) -> np.ndarray:
    # get_detection_points: Return set x-y coordinates of detected molecules
    # Parameters: 
    # T = 2D image usually containing test statistic
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    if(kernel is None):
        mask = detector_type(T, pfa, local_max_range)
    else:           
        mask = detector_type(T, pfa, local_max_range, kernel)
        
    points = mask2points(mask)
    
    return points

#%%
def get_detection_points_filter(
    T: np.ndarray, detector_type, pfa: float,
    w: np.ndarray,
    local_max_range: int, 
    kernel: np.ndarray=None) -> np.ndarray:
    # get_detection_points_filter: Return set x-y coordinates of 
    # detected molecules from filtered image 'T'
    # Parameters: 
    # T = 2D image usually containing test statistic
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # w = filter kernel
    # local_max_range = radius of local maximum
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed            
    
    if(kernel is None):
        mask = detector_type(T, pfa, w, local_max_range)
    else:           
        mask = detector_type(T, pfa, w, local_max_range, kernel)
        
    points = mask2points(mask)
    
    return points

#%%
def emccd_conversion_adu2ph(
        adu: int, baseline: float, sensitivity: float,
        qe: float, em_gain: float) -> float:
    # emccd_conversion_adu2ph: emccd camera conversion from digital units to 
    # the number of photons
    # Parameters:
    # adu = arbitrary digital unit
    # baseline = camera offset level
    # sensitivity = camera conversion factor in [no. electrons / adu]
    # qe = quantum efficiency
    # em_gain = multiplication gain of emccd
    
    return 1/qe * sensitivity / em_gain * (adu-baseline)

#%%
def ccd_conversion_adu2ph(adu, baseline, sensitivity, qe):     
    # ccd_conversion_adu2ph: ccd camera conversion from digital units to 
    # the number of photons
    # Parameters:
    # adu = arbitrary digital unit
    # baseline = camera offset level
    # sensitivity = camera conversion factor in [no. electrons / adu]
    # qe = quantum efficiency
    return 1/qe * sensitivity * ( adu - baseline )
