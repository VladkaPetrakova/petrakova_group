# MH, v1.2, 2024_10_09

#%% lib
import numpy as np

from numpy import sqrt
from scipy.stats import norm
from scipy.ndimage import generic_filter

from scipy.stats import iqr
# from scipy.stats import median_abs_deviation
# from statsmodels.robust.scale import qn_scale
# from skimage.restoration import estimate_sigma

from poisson_nofilter import cfar_background_mean_estimate
from detection_tools import remove_nonlocal_maxima
from poisson_nofilter import cacfar_background_mean_estimate
from poisson_nofilter import cacfar_background_mean_estimate_fft
from poisson_nofilter import oscfar_background_mean_estimate

#%%
def cfar_background_std_estimate(T: np.ndarray) -> float:  
    # cfar_background_std_estimate: Returns global standard deviation 
    # estimate 
    # Parameters:
    # T = input image 
    
    std_estimate = iqr(T.ravel(), scale='normal')
    # std_estimate = qn_scale(T.ravel())
    # std_estimate = estimate_sigma(T)
    # std_estimate = median_abs_deviation(T.ravel(), scale='normal')
    
    return std_estimate

#%%
def isf_threshold(pfa: float, mu: float, sigma: float) -> float:    
    # isf_threshold: Returns inverse survival function (ISF) threshold for a
    # Gaussian distribution of filtered data
    # Parameters:
    # pfa = probability of false alarm
    # mu = mean
    # sigma = standard deviation

    return norm.isf(pfa, loc=mu, scale=sigma)

#%%
def cfar_segmentation(T: np.ndarray, pfa: float) -> np.ndarray:
    # cfar_segmentation: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold
    # Parameters:
    # T = input image 
    # pfa = probability of false alarm

    b_estimate = cfar_background_mean_estimate(T)        
    b_std_estimate = cfar_background_std_estimate(T)
    tau = isf_threshold(pfa, b_estimate, b_std_estimate)
    mask = T > tau
    return mask

#%%
def cfar(T: np.ndarray, pfa: float, local_max_range: int) -> np.ndarray: 
    # cfar: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold and which 
    # form local maximum
    # Parameters:
    # T = input image 
    # pfa = probability of false alarm
    # local_max_range = range over which local maximum is searched
    
    segmentation = cfar_segmentation(T, pfa)    
    mask = remove_nonlocal_maxima(segmentation, T, local_max_range)
    return mask 

#%%
def cacfar_background_std_estimate(
        T: np.ndarray, b: np.ndarray, kernel: np.ndarray) -> np.ndarray:                   
    
    # cacfar_background_std_estimate: Returns local standard deviation
    # estimate given by arithmetic mean in neighborhood given by the kernel
    # Parameters:
    # T = input image 
    # b = image with mean values
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    
    background_std_estimate = sqrt(
        cacfar_background_mean_estimate((T-b)**2, kernel))
    return background_std_estimate

#%%
def cacfar_background_std_estimate_fft(
        T: np.ndarray, b: np.ndarray, kernel: np.ndarray)-> np.ndarray: 

    # cacfar_background_std_estimate_fft: Returns local standard deviation
    # estimate given by arithmetic mean in neighborhood given by the kernel
    # efficiently implemented by FFT
    # Parameters:
    # T = input image 
    # b = image with mean values
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
              
    background_std_estimate = sqrt(
        cacfar_background_mean_estimate_fft((T-b)**2, kernel))
    return background_std_estimate

#%%
def cacfar_segmentation(
        T: np.ndarray, pfa: float, kernel: np.ndarray) -> np.ndarray:    
    # cacfar_segmentation: Returns binary mask segmentating pixels which are
    # above cell-averaging constant false alarm rate (ca-cfar) 
    # isf threshold, where the mean and std estimates iare given by local 
    # arithmetic means
    # Parameters:
    # T = input image 
    # pfa = probability of false alarm
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean and std is computed

    b_estimate = cacfar_background_mean_estimate(T, kernel)        
    b_std_estimate = cacfar_background_std_estimate(
        T, b_estimate, kernel)
    
    tau = isf_threshold(pfa, b_estimate, b_std_estimate)    
    mask = T > tau    
    return mask

#%%
def cacfar(
        T: np.ndarray, pfa: float, 
        local_max_range: int, kernel: np.ndarray) -> np.ndarray:
    # cacfar: Returns binary mask segmentating pixels which are
    # above  cell-averaging constant false alarm rate (ca-cfar) isf 
    # threshold and which form local maximum, where the mean and std estimates
    # are given by local arithmetic means
    # Parameters:
    # T = input image 
    # pfa = probability of false alarm
    # local_max_range = range over which local maximum is searched
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed 

    segmentation = cacfar_segmentation(T, pfa, kernel)    
    mask = remove_nonlocal_maxima(segmentation, T, local_max_range)    
    return mask

#%%
def robust_std_estimate(T: np.ndarray) -> float:
    # robust_std_estimate: Returns robust estimate of standard deviation
    # Parameters:
    # T = input image 

    return iqr(T.ravel(), scale='normal')

#%%
def oscfar_background_std_estimate(T: np.ndarray, kernel: np.ndarray) -> np.ndarray:           

    # oscfar_background_std_estimate: Returns local standard deviation
    # estimate given by interquantile range in neighborhood given by the kernel
    # Parameters:
    # T = input image 
    # kernel = binary filter kernel describing local neighborhood within the 
    # local std is computed

    b_std_estimate = generic_filter(T, robust_std_estimate, 
        footprint=kernel, mode='reflect', output='f')    
        
    return b_std_estimate

#%%
def oscfar_segmentation(
        T: np.ndarray, pfa: float, kernel: np.ndarray) -> np.ndarray:    
    # oscfar_segmentation: Returns binary mask segmentating pixels which are
    # above ordered-statistics constant false alarm rate (os-cfar) 
    # isf threshold, where the mean and std are given by robust estimators
    # Parameters:
    # T = input image 
    # pfa = probability of false alarm
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed

    b_estimate = oscfar_background_mean_estimate(T, kernel)        
    b_std_estimate = oscfar_background_std_estimate(T, kernel)
    
    tau = isf_threshold(pfa, b_estimate, b_std_estimate)    
    mask = T > tau    
    return mask

#%%
def oscfar(
        T: np.ndarray, pfa: float, 
        local_max_range: int, kernel: np.ndarray) -> np.ndarray:     
    # oscfar: Returns binary mask segmentating pixels which are
    # above  ordered-statistics constant false alarm rate (os-cfar) isf 
    # threshold and which form local maximum, where mean and std 
    # are given by robust estimators
    # Parameters:
    # T = input image 
    # pfa = probability of false alarm
    # local_max_range = range over which local maximum is searched
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed 

    segmentation = oscfar_segmentation(T, pfa, kernel)    
    mask = remove_nonlocal_maxima(segmentation, T, local_max_range)    
    return mask














