# MH, v1.1, 2024_10_09

#%% lib
import numpy as np
from tqdm import tqdm
import pathlib

from detection_tools import get_detection_points
from linking import lsa
from performance import evaluate_points
from show import show_image

#%%
def get_meas_positions(path: pathlib) -> np.ndarray:   
    # get_meas_positions: Load ground true molecule position coordinates 
    # in pixels
    # Parameters:
    # path = path to the *.csv data file
    
    if path.stat().st_size == 0:        
        return np.array([], dtype='int')
    
    points = np.genfromtxt(path, delimiter=',', dtype='int')
    
    if points.ndim == 1:
        points = points[np.newaxis, :]
        
    return points

#%%
def evaluate_frame_meas(
    images: np.ndarray, 
    gt_position_paths: list,
    i_frame: int, 
    detector_type, 
    pfa: float, 
    local_max_range: int, 
    tp_threshold: float, 
    dataset_dict: dict, 
    kernel: np.ndarray=None,
    ) -> tuple:
    # evaluate_frame_meas: Evaluate predicted molecule detections in the ith frame
    # Parameters:
    # images = datacube of all frames
    # gt_position_paths = list of paths to ground true positions
    # i_frame = index of the frame to be shown
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    gt_points = get_meas_positions(gt_position_paths[i_frame])    
    pr_points = get_detection_points(
        images[i_frame], detector_type, pfa,
        local_max_range, kernel=kernel)        
    
    # assignment
    gt_points_assigned, pr_points_assigned = lsa(gt_points, pr_points)
    
    # evaluation
    tp,fp,tn,fn = evaluate_points(
        gt_points, pr_points, gt_points_assigned, pr_points_assigned, 
        tp_threshold, dataset_dict['shape'])
    
    return tp,fp,tn,fn    

#%%
def show_frame_performance_meas(
    i_frame: int, 
    images: np.ndarray, 
    gt_position_paths: list,
    detector_type,
    pfa: float, 
    local_max_range: int, 
    dataset_dict: dict, 
    kernel: np.ndarray=None,
    ) -> None:
    # show_frame_performance_meas: Show predicted and ground true molecule 
    # positions over input image
    # Parameters:
    # i_frame = index of the frame to be shown
    # images = datacube of all frames
    # gt_position_paths = list of paths to ground true positions
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    image = images[i_frame]
    
    gt_points = get_meas_positions(gt_position_paths[i_frame])    
    pr_points = get_detection_points(
        image, detector_type, pfa, local_max_range, kernel=kernel)
    
    show_image(image, points=gt_points, 
               title='ground true', figsize=(5,5), hide_axis=False)
    show_image(image, points=pr_points, 
               title='predictions', figsize=(5,5), hide_axis=False)
    
#%%
def evaluate_all_frames_meas(
    images: np.ndarray, 
    gt_position_paths: list,
    detector_type,
    pfa: float, 
    local_max_range: int, 
    tp_threshold: float, 
    n_frames: int, 
    dataset_dict: dict,
    kernel: np.ndarray=None,
    ) -> tuple:
    # evaluate_all_frames_meas: Evaluate predicted molecule detections in all frames
    # Parameters:
    # images = datacube of all frames
    # gt_position_paths = list of paths to ground true positions
    # detector_type = function used in detection, e.g. cfar, cacfar, oscfar
    # pfa = probability of false alarm
    # local_max_range = radius of local maximum
    # tp_threshold = tolerance where predicted detection is still marked as 
    # true positive
    # n_frames = number of frames to be evaluated
    # dataset_dict = dictionary with all dataset parameters
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed

    tp_vec = np.zeros(n_frames)
    fp_vec = np.zeros(n_frames)
    tn_vec = np.zeros(n_frames)
    fn_vec = np.zeros(n_frames)    
    
    for i in tqdm(range(n_frames)):
    
        tp_vec[i],fp_vec[i],tn_vec[i],fn_vec[i] = evaluate_frame_meas(
            images, gt_position_paths, i, 
            detector_type, pfa, local_max_range, 
            tp_threshold, dataset_dict, kernel=kernel)   
    
    return  tp_vec, fp_vec, tn_vec, fn_vec















    