# MH, v1.1, 2024_10_07

import numpy as np
import pathlib
import json

#%%
def export_dictionary(dictionary: dict, path: pathlib) -> None :    
    # export_dictionary: Export dictionary as a json file
    # Parameters:
    # dictionary = dictionary to be exported
    # path = path on which to export
    
    dict_clean = dictionary.copy()

    # numpy arrays and pathlib path objects are not exported into json
    for k in dictionary:            
        if isinstance(dict_clean[k], np.ndarray) or isinstance(
            dict_clean[k], pathlib.WindowsPath):
                del dict_clean[k]
    
    json.dump(dict_clean, open(path, 'w' ) )    