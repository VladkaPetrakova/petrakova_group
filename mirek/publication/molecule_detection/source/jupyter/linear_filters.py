# MH, v1.0, 2024_02_07

#%% lib
import numpy as np
from numpy import pi, exp, mean
from scipy.signal import convolve2d

from signal_generation import get_single_spot

#%%
def laplacian(x: float, y: float, sigma: float) -> float:
    # laplacian: Returns value of 2D Laplacian of Gaussian
    # Parameters:
    # x = x-coordinate of the Laplacian of Gaussian       
    # y = y-coordinate of the Laplacian of Gaussian
    # sigma = standard deviation of Gaussian    

    return (-x**2-y**2+2*sigma**2) / (2*pi*sigma**6) * exp(
        -(x**2+y**2)/(2*sigma**2))

#%%
def get_log(LoG_sigma: float, filter_range: int) -> np.ndarray:
    # get_log: Returns Laplacian of Gaussian filter
    # Parameters:
    # LoG_sigma = standard deviation of Gaussian
    # filter_range = one-sided half size of the filter kernel
        
    filter_size = 2*filter_range+1
    
    xn = np.arange(-filter_range,filter_range+1)
    yn = np.arange(-filter_range,filter_range+1)
    
    LoG = np.zeros((filter_size, filter_size))
    for i in range(filter_size):
        for j in range(filter_size):
            LoG[i,j] = laplacian(xn[i],yn[j],LoG_sigma)
            
    return LoG

#%%
def difference_of_gaussian(
        x: float, y: float, sigma1: float, sigma2: float)-> float:
    # difference_of_gaussian: Returns value of 2D difference of Gaussians
    # Parameters:
    # x = x-coordinate of the difference of Gaussians        
    # y = y-coordinate of the difference of Gaussians
    # sigma1 = standard deviation of 1st Gaussian        
    # sigma2 = standard deviation of 2nd Gaussian            
    
    return 1/(2*pi*sigma1**2)*exp(-(x**2+y**2)/(2*sigma1**2)) \
        -1/(2*pi*sigma2**2)*exp(-(x**2+y**2)/(2*sigma2**2))      
        
#%%
def get_dog(
        DoG_sigma1: float, DoG_sigma2: 
        float, filter_range: int) -> np.ndarray:
    # get_dog: Returns difference of Gaussians filter
    # Parameters:
    # DoG_sigma1 = standard deviation of 1st Gaussian 
    # DoG_sigma2 = standard deviation of 2nd Gaussian 
    # filter_range = one-sided half size of the filter kernel

    filter_size = 2*filter_range+1
    
    xn = np.arange(-filter_range,filter_range+1)
    yn = np.arange(-filter_range,filter_range+1)
    
    DoG = np.zeros((filter_size, filter_size))
    for i in range(filter_size):
        for j in range(filter_size):
            DoG[i,j] = difference_of_gaussian(
                xn[i], yn[j], DoG_sigma1, DoG_sigma2)
            
    return DoG

#%%
def unit_box(x: float, y: float, a: float) -> float:
    # unit_box:  Returns value of 2D box function
    # x = x-coordinate of the box function       
    # y = y-coordinate of the box function
    # a = size of the box function
    
    return 1/a**2 * float( (x<=a/2) & (x>=-a/2) & (y<=a/2) & (y>=-a/2) )

#%%
def difference_of_arithmetic_means(
        x: float, y: float, a1: float, a2: float) -> float:
    # difference_of_arithmetic_means:  Returns value of the 
    # difference of arithmetic means
    # x = x-coordinate of the box function       
    # y = y-coordinate of the box function
    # a1 = size of the 1st box function
    # a2 = size of the 2nd box function    

    return unit_box(x,y,a1) - unit_box(x,y,a2)

#%%
def get_doa(
        DoA_a1: float, DoA_a2: float, filter_range: int) -> np.ndarray:
    # get_doa: Returns difference of arithmetic means filter
    # Parameters:
    # DoA_a1 = size of the 1st box function
    # DoA_a2 = size of the 2nd box function 
    # filter_range = one-sided half size of the filter kernel

    filter_size = 2*filter_range+1
    
    xn = np.arange(-filter_range,filter_range+1)
    yn = np.arange(-filter_range,filter_range+1)
    
    DoA = np.zeros((filter_size, filter_size))
    for i in range(filter_size):
        for j in range(filter_size):
            DoA[i,j] = difference_of_arithmetic_means(
                xn[i],yn[j],DoA_a1,DoA_a2)
            
    return DoA

#%%
def get_lg(sigma: float, filter_range: int, psf_fun) -> np.ndarray:
    # get_lg: Returns lowered Gaussian filter
    # Parameters:
    # sigma = standard deviation of the psf function
    # filter_range = one-sided half size of the filter kernel
    # psf_fun = point spread function model, e.g. 'gauss2d' or 'integrated_gauss2d'

    filter_size = 2*filter_range+1
    
    LG = get_single_spot(x0=filter_range, y0=filter_range,
        psf_fun=psf_fun, sigma=sigma, a=1, size=filter_size)

    LG = LG - mean(LG)

    return LG

#%%
def get_M(k: np.ndarray) -> np.ndarray: 
    # get_M: Returns an outer product of vector 'k' with itself 
    
    return np.outer(k,k)

#%%
def insert_zeros(k1: np.ndarray) -> np.ndarray:
    # insert_zeros: Insert zeros between every two values of vector 'k1'
    
    k2 = np.zeros(2*len(k1))
    k2[::2] = k1
    k2 = np.delete(k2,-1)
    return k2

#%%
def get_M_tilde(M: np.ndarray) -> np.ndarray:
    # get_M_tilde: Returns auxiliary matrix M_tilde in construction of 
    # B-spline wavelet filter
    # Parameters:
    # M = square matrix
    
    middle_index = int((M.shape[0]-1)/2)
    
    M_tilde = -M
    M_tilde[middle_index, middle_index] += 1
    
    return M_tilde

#%%
def get_M12(M1: np.ndarray, M2_tilde: np.ndarray) -> np.ndarray:
    # get_M12: Returns auxiliary matrix M12 in construction of 
    # B-spline wavelet filter
    # Parameters:
    # M1 = square matrix
    # M2_tilde = square matrix

    return convolve2d(M1, M2_tilde)

#%%
def get_bspline_wavelet(k1: np.ndarray) -> np.ndarray:
    # get_bspline_wavelet: Returns B-spline atrous wavelet filter kernel
    # Parameters:
    # k1 = vector of B-spline values    
    
    M1 = get_M(k1)
    
    k2 = insert_zeros(k1)
    
    M2 = get_M(k2)
    M2_tilde = get_M_tilde(M2)
    
    M12 = get_M12(M1, M2_tilde)
    
    return M12

















