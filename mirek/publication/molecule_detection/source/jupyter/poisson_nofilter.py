# MH, v1.2, 2024_10_07

#%% lib
import numpy as np
from numpy import sum, median
from scipy.stats import poisson
from scipy.ndimage import convolve
from scipy import signal
from skimage import filters
# from scipy.ndimage import generic_filter      
from detection_tools import remove_nonlocal_maxima

#%%
def cfar_background_mean_estimate(r: np.ndarray) -> float: 
    # cfar_background_mean_estimate: Returns global mean background level 
    # estimate given by median 
    # Parameters:
    # r = received 2D signal from which background mean is estimated
    
    return median(r)

#%%
def isf_threshold(pfa: float, b: float) -> int: 
    # isf_threshold: Returns inverse survival function (ISF) threshold for a
    # Poisson distribution
    # Parameters:
    # pfa = probability of false alarm
    # b = mean background level
    
    return poisson.isf(pfa, b).astype('int')

#%%
def cfar_segmentation(r: np.ndarray, pfa: float) -> np.ndarray:  
    # cfar_segmentation: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold
    # Parameters:
    # r = received 2D signal
    # pfa = probability of false alarm

    b_estimate = cfar_background_mean_estimate(r)    
    tau = isf_threshold(pfa, b_estimate)
    mask = r > tau
    return mask

#%%
def cfar(r: np.ndarray, pfa: float, local_max_range: int) -> np.ndarray:    
    # cfar: Returns binary mask segmentating pixels which are
    # above global constant false alarm rate (cfar) isf threshold and which 
    # form local maximum
    # Parameters:
    # r = received 2D signal
    # pfa = probability of false alarm
    # local_max_range = range over which local maximum is searched
    
    segmentation = cfar_segmentation(r, pfa)    
    mask = remove_nonlocal_maxima(segmentation, r, local_max_range)
    return mask 

#%%
def cacfar_background_mean_estimate(
        r: np.ndarray, kernel: np.ndarray) -> np.ndarray:    
    # cacfar_background_mean_estimate: Returns local mean background level 
    # estimate given by arithmetic mean in neighborhood given by the kernel
    # Parameters:
    # r = received 2D signal from which background mean is estimated
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    
    w = kernel/sum(kernel)
    b_estimate = convolve(r.astype('float'), w, mode='mirror')
    return b_estimate

#%%
def cacfar_background_mean_estimate_fft(
        r: np.ndarray, kernel: np.ndarray) -> np.ndarray: 
    # cacfar_background_mean_estimate_fft: Returns local mean background level 
    # estimate given by arithmetic mean in neighborhood given by the kernel 
    # efficiently implemented by FFT
    # Parameters:
    # r = received 2D signal from which background mean is estimated
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
    
    w = kernel/sum(kernel)    
    r_pad = np.pad(r, (w.shape[0]-1)//2, mode='reflect')
    b_estimate = signal.fftconvolve( 
        r_pad.astype('float'), w, mode='valid') 
    return b_estimate

#%%
def cacfar_segmentation(
        r: np.ndarray, pfa: float, kernel: np.ndarray) -> np.ndarray:  
    # cacfar_segmentation: Returns binary mask segmentating pixels which are
    # above cell-averaging constant false alarm rate (ca-cfar) 
    # isf threshold, where the background estimate is given by local arithmetic mean
    # Parameters:
    # r = received 2D signal
    # pfa = probability of false alarm
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed
     
    b_estimate = cacfar_background_mean_estimate(r, kernel)    
    tau = isf_threshold(pfa, b_estimate)
    mask = r > tau
    return mask

#%%
def cacfar(
        r: np.ndarray, pfa: float, local_max_range: int,
        kernel: np.ndarray) -> np.ndarray:     
    # cacfar: Returns binary mask segmentating pixels which are
    # above  cell-averaging constant false alarm rate (ca-cfar) isf 
    # threshold and which form local maximum, where the background estimate
    # is given by local arithmetic mean
    # Parameters:
    # r = received 2D signal
    # pfa = probability of false alarm
    # local_max_range = range over which local maximum is searched
    # kernel = binary filter kernel describing local neighborhood within the 
    # local mean is computed    
    
    mask_nonlocal_max = cacfar_segmentation(r, pfa, kernel)    
    mask = remove_nonlocal_maxima(
        mask_nonlocal_max, r, local_max_range)    
    return mask

#%%
def oscfar_background_mean_estimate(
        r: np.ndarray, kernel: np.ndarray) -> np.ndarray:    
    # oscfar_background_mean_estimate: Returns local mean background level 
    # estimate given by median in neighborhood given by the kernel
    # Parameters:
    # r = received 2D signal from which background mean is estimated
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed
    
    b_estimate = filters.median(r,
        footprint=kernel, mode='reflect', out='f')   
     
#     b_estimate = generic_filter(r, median, 
#         footprint=kernel, mode='reflect', output='float')
    
    return b_estimate

#%%
def oscfar_segmentation(
        r: np.ndarray, pfa: float, kernel: np.ndarray) -> np.ndarray: 
    # oscfar_segmentation: Returns binary mask segmentating pixels which are
    # above ordered-statistics constant false alarm rate (os-cfar) 
    # isf threshold, where the background estimate is given by local median
    # Parameters:
    # r = received 2D signal
    # pfa = probability of false alarm
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed
      
    b_estimate = oscfar_background_mean_estimate(r, kernel)    
    tau = isf_threshold(pfa, b_estimate)
    mask = r > tau
    return mask

#%%
def oscfar(
        r: np.ndarray, pfa: float, 
        local_max_range: int, kernel: np.ndarray) -> np.ndarray:    
    # oscfar: Returns binary mask segmentating pixels which are
    # above  ordered-statistics constant false alarm rate (os-cfar) isf 
    # threshold and which form local maximum, where the background estimate
    # is given by local median
    # Parameters:
    # r = received 2D signal
    # pfa = probability of false alarm
    # local_max_range = range over which local maximum is searched
    # kernel = binary filter kernel describing local neighborhood within the 
    # local median is computed   
    
    mask_nonlocal_max = oscfar_segmentation(r, pfa, kernel)        
    mask = remove_nonlocal_maxima(
        mask_nonlocal_max, r, local_max_range)        
    return mask









































