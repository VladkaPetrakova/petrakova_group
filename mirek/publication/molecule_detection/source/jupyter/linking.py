# MH, v1.1, 2024_10_08

#%% lib
import numpy as np
from scipy.optimize import linear_sum_assignment

#%%
def get_cost(gt_points: np.ndarray, pr_points: np.ndarray) -> np.ndarray:
    # get_cost: Returns a cost matrix used in linking by 
    # linear_sum_assignment function
    # Parameters:
    # gt_points = array of ground true molecule x-y coordinates
    # pr_points = array of predicted molecule x-y coordinates
    
    n_gt_points = gt_points.shape[0]
    n_pr_points = pr_points.shape[0]
    
    cost = np.zeros((n_pr_points,n_gt_points))
    
    for i in range(n_pr_points):
        for j in range(n_gt_points):
            cost[i,j] = np.linalg.norm(pr_points[i]-gt_points[j])
    
    return cost

#%%
def lsa(gt_points: np.ndarray, pr_points: np.ndarray) -> tuple:
    # lsa: Returns assigned ground true molecule positions with
    # assigned positions from predicted positions. The linking is done by 
    # linear_sum_assignment function
    # Parameters:
    # gt_points = array of ground true molecule x-y coordinates
    # pr_points = array of predicted molecule x-y coordinates

    cost = get_cost(gt_points, pr_points)
    
    row_pr_ind, col_gt_ind = linear_sum_assignment(cost)
    
    gt_points_assigned = gt_points[col_gt_ind]
    pr_points_assigned = pr_points[row_pr_ind]
    
    return gt_points_assigned, pr_points_assigned