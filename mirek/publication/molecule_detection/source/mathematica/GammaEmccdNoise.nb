(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     32638,        798]
NotebookOptionsPosition[     27282,        709]
NotebookOutlinePosition[     27722,        726]
CellTagsIndexPosition[     27679,        723]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Gamma Distributed EMCCD Noise", "Title",
 CellChangeTimes->{{3.933324729668354*^9, 3.933324747725814*^9}, {
  3.9333887570907516`*^9, 
  3.933388764000176*^9}},ExpressionUUID->"52db2253-f4df-4719-99fc-\
24d96372ebb6"],

Cell["MH, v1.1, 2024_08_23", "Text",
 CellChangeTimes->{{3.933324750285928*^9, 3.9333247672667265`*^9}, {
   3.9333878695205803`*^9, 3.9333878724627495`*^9}, 
   3.9333887717802916`*^9},ExpressionUUID->"b5a07bfa-b923-4cfd-ba0f-\
39a1ec2edac8"],

Cell[BoxData[
 RowBox[{"ClearAll", "[", "\"\<`*\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.7579218763506384`*^9, 3.757921904972211*^9}, 
   3.839919912358202*^9, {3.839921047583912*^9, 3.8399210478808064`*^9}},
 CellLabel->"In[52]:=",ExpressionUUID->"a6487de7-fc3e-4f00-bb14-3f98052e2715"],

Cell[CellGroupData[{

Cell["Init", "Section",
 CellChangeTimes->{{3.859521428841344*^9, 
  3.859521429273638*^9}},ExpressionUUID->"bf845f1f-1e99-4fe4-9ab7-\
640e3ec67311"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"Image", ",", 
    RowBox[{"ImageSize", "\[Rule]", "300"}]}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}]}], "Input",
 CellChangeTimes->{{3.8595214314464006`*^9, 3.859521446375717*^9}, 
   3.936328476862539*^9},
 CellLabel->"In[53]:=",ExpressionUUID->"0671af08-9289-450a-9407-2afcac9a1e99"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Select Frame", "Section",
 CellChangeTimes->{{3.9333248374607105`*^9, 3.933324841113035*^9}, {
  3.9333250679733915`*^9, 
  3.9333250700926666`*^9}},ExpressionUUID->"74588e31-3aec-43a6-b2b7-\
733d84438ad2"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"iFrame", "=", "0"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"iFrame", "=", "9"}], ";"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.933324842700973*^9, 3.9333248489393744`*^9}, {
  3.9333249873080854`*^9, 3.933324991676549*^9}, {3.933325092067996*^9, 
  3.933325093846777*^9}},
 CellLabel->"In[55]:=",ExpressionUUID->"b4303827-0539-4076-8e05-c3398bd7a4b6"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Paths", "Section",
 CellChangeTimes->{{3.9333250728278737`*^9, 
  3.9333250736604233`*^9}},ExpressionUUID->"998e8808-6053-46a1-a692-\
d945a8912e88"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"imageName", "=", 
    RowBox[{"StringPadLeft", "[", 
     RowBox[{
      RowBox[{"ToString", "[", 
       RowBox[{"iFrame", "+", "1"}], "]"}], ",", "5", ",", "\"\<0\>\""}], 
     "]"}]}], ";"}], "\n"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"path", "=", 
   RowBox[{
   "\"\<c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/tasks/\
molecule_detection/jupyter/dataset/mt0n2ld/sequence/\>\"", "<>", "imageName", 
    "<>", "\"\<.tif\>\""}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.933324852743871*^9, 3.9333248690542297`*^9}, {
  3.933324914973816*^9, 3.9333249521072145`*^9}, {3.9333878777557936`*^9, 
  3.933387885547797*^9}},
 CellLabel->"In[56]:=",ExpressionUUID->"2b9e0bd5-900d-4c75-b884-8077d5bc331b"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Functions", "Section",
 CellChangeTimes->{{3.933325013706489*^9, 
  3.933325014777712*^9}},ExpressionUUID->"3543b557-73c1-4ef9-ae0c-\
c7122c311353"],

Cell[BoxData[
 RowBox[{
  RowBox[{"normalize", "[", "data_", "]"}], ":=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"data", "-", 
     RowBox[{"Min", "[", "data", "]"}]}], ")"}], "/", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"Max", "[", "data", "]"}], "-", 
     RowBox[{"Min", "[", "data", "]"}]}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.859359375591*^9, 3.8593593892053337`*^9}},
 CellLabel->"In[58]:=",ExpressionUUID->"9bd17c56-809a-43a2-8579-9e87176a0fb7"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Import Raw Data", "Section",
 CellChangeTimes->{{3.8595159049917994`*^9, 3.859515906533163*^9}, {
  3.933325059358844*^9, 3.9333250602758193`*^9}, {3.9333271506469917`*^9, 
  3.933327152818574*^9}},ExpressionUUID->"db5a1312-1599-49c9-bf2e-\
41de5948aefc"],

Cell[BoxData[
 RowBox[{
  RowBox[{"imageRaw", "=", 
   RowBox[{"Import", "[", 
    RowBox[{"path", ",", "\"\<Data\>\""}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.9333272155164423`*^9, 3.9333272158641663`*^9}},
 CellLabel->"In[59]:=",ExpressionUUID->"ea2e0971-bb3d-48f8-a0fa-ea4585ff4db9"],

Cell[BoxData[
 RowBox[{"Image", "[", 
  RowBox[{"normalize", "[", "imageRaw", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.859357949124263*^9, 3.8593579519777164`*^9}, {
   3.8593579821236424`*^9, 3.8593579823616204`*^9}, {3.8593580849110775`*^9, 
   3.8593580885233917`*^9}, {3.8593581343331013`*^9, 
   3.8593581380540476`*^9}, {3.8593583681419983`*^9, 3.859358400899688*^9}, {
   3.859358493976386*^9, 3.8593585050876503`*^9}, {3.859358634867181*^9, 
   3.859358671216606*^9}, {3.8593587066809673`*^9, 3.8593587543966417`*^9}, {
   3.8593592639988256`*^9, 3.859359298096946*^9}, {3.859359396251658*^9, 
   3.8593593982712183`*^9}, {3.859373541918602*^9, 3.8593735539460907`*^9}, {
   3.859520629692236*^9, 3.859520630161393*^9}, 3.859521451445957*^9, 
   3.933327219855999*^9},
 CellLabel->"In[60]:=",ExpressionUUID->"ea34ba85-6a88-4faa-8059-f84a46904f83"],

Cell[BoxData[
 RowBox[{"Dimensions", "[", "imageRaw", "]"}]], "Input",
 CellChangeTimes->{3.9333272210119047`*^9},
 CellLabel->"In[61]:=",ExpressionUUID->"5a824ec1-303c-4292-96af-e06c9554edc2"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Min", "[", "imageRaw", "]"}], ",", 
   RowBox[{"Max", "[", "imageRaw", "]"}]}], "}"}]], "Input",
 CellChangeTimes->{{3.9333274845778704`*^9, 3.9333274989475026`*^9}},
 CellLabel->"In[62]:=",ExpressionUUID->"cafe2cdf-42c2-4877-9f53-0fc91561db10"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Photon Conversion", "Section",
 CellChangeTimes->{{3.933327159894884*^9, 3.933327166020666*^9}, {
  3.933388787576047*^9, 
  3.933388788124245*^9}},ExpressionUUID->"18a85559-a49a-4631-86b2-\
ed289a2ab748"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"qe", "=", "0.9"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sensitivity", "=", "45"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"baseline", "=", "100"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"emgain", "=", "300"}], ";"}]}], "Input",
 CellChangeTimes->{{3.9333271711560802`*^9, 3.933327199644926*^9}},
 CellLabel->"In[63]:=",ExpressionUUID->"e481ce51-af26-43fd-adf1-a0d4a9aad30f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"image", "=", 
   RowBox[{
    RowBox[{"1", "/", "qe"}], "*", 
    RowBox[{"sensitivity", "/", "emgain"}], "*", 
    RowBox[{"(", 
     RowBox[{"imageRaw", "-", "baseline"}], ")"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.9333276065411363`*^9, 3.9333276388151217`*^9}},
 CellLabel->"In[67]:=",ExpressionUUID->"2b9186e5-7fb0-494f-8d6f-ff9516d23d46"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Min", "[", "image", "]"}], ",", 
   RowBox[{"Max", "[", "image", "]"}]}], "}"}]], "Input",
 CellChangeTimes->{{3.9333274845778704`*^9, 3.9333274989475026`*^9}, {
  3.933329480605554*^9, 3.9333294814914412`*^9}},
 CellLabel->"In[68]:=",ExpressionUUID->"b2649cfa-e0a6-433f-b24f-22b3f72f9b95"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Noise", "Section",
 CellChangeTimes->{{3.933325104760451*^9, 
  3.933325108053956*^9}},ExpressionUUID->"c08b1ab1-a66a-4eaf-b0a0-\
dd682a45cde4"],

Cell[BoxData[
 RowBox[{
  RowBox[{"noise", "=", 
   RowBox[{"image", "\[LeftDoubleBracket]", 
    RowBox[{
     RowBox[{"1", ";;", "32"}], ",", 
     RowBox[{"1", ";;", "32"}]}], "\[RightDoubleBracket]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.933325109249766*^9, 3.933325110088837*^9}, {
  3.9333256066847243`*^9, 3.9333256077136946`*^9}},
 CellLabel->"In[69]:=",ExpressionUUID->"e5da504c-77fc-4316-a2ad-1bd743c5e6cc"],

Cell[BoxData[
 RowBox[{"Image", "[", 
  RowBox[{"normalize", "[", "noise", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.859357949124263*^9, 3.8593579519777164`*^9}, {
   3.8593579821236424`*^9, 3.8593579823616204`*^9}, {3.8593580849110775`*^9, 
   3.8593580885233917`*^9}, {3.8593581343331013`*^9, 
   3.8593581380540476`*^9}, {3.8593583681419983`*^9, 3.859358400899688*^9}, {
   3.859358493976386*^9, 3.8593585050876503`*^9}, {3.859358634867181*^9, 
   3.859358671216606*^9}, {3.8593587066809673`*^9, 3.8593587543966417`*^9}, {
   3.8593592639988256`*^9, 3.859359298096946*^9}, {3.859359396251658*^9, 
   3.8593593982712183`*^9}, {3.859373541918602*^9, 3.8593735539460907`*^9}, {
   3.859520629692236*^9, 3.859520630161393*^9}, 3.859521451445957*^9, {
   3.9333256222247343`*^9, 3.9333256227409306`*^9}, {3.9333896034268355`*^9, 
   3.933389618830373*^9}},
 CellLabel->"In[70]:=",ExpressionUUID->"74c9b1e6-e241-4307-94fc-728765b2c388"],

Cell[BoxData[
 RowBox[{
  RowBox[{"x", "=", 
   RowBox[{"noise", "//", "Flatten"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.933326781327605*^9, 3.9333267861028986`*^9}, {
  3.933326914761736*^9, 3.933326916973104*^9}},
 CellLabel->"In[71]:=",ExpressionUUID->"7debda80-a1fb-4038-a016-f20f3cc2be82"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"remove", " ", "non"}], "-", 
    RowBox[{"positive", " ", "numbers"}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"x", "=", 
    RowBox[{"Select", "[", 
     RowBox[{"x", ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"#", ">", "0"}], ")"}], "&"}]}], "]"}]}], ";"}]}]], "Input",
 CellChangeTimes->{{3.9333892397637415`*^9, 3.9333892658920484`*^9}, 
   3.9333893146333923`*^9, {3.9333894037729483`*^9, 3.9333894162639627`*^9}, {
   3.933389691740135*^9, 3.9333897109407578`*^9}},
 CellLabel->"In[72]:=",ExpressionUUID->"8f0600da-c236-4cb9-a554-fabc807d133a"],

Cell[BoxData[
 RowBox[{"Histogram", "[", 
  RowBox[{"x", ",", "Automatic", ",", "\"\<PDF\>\""}], "]"}]], "Input",
 CellChangeTimes->{{3.9333256414380484`*^9, 3.9333257402381535`*^9}, {
   3.933326013981948*^9, 3.9333260861231995`*^9}, 3.9333883772746344`*^9},
 CellLabel->"In[73]:=",ExpressionUUID->"bb21bee1-6283-4c21-a40e-58a2fda94403"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Gamma Distribution", "Section",
 CellChangeTimes->{{3.933326094107156*^9, 
  3.933326097627516*^9}},ExpressionUUID->"b33a93de-402e-41b2-81ef-\
ecf27f58ee63"],

Cell[BoxData[
 RowBox[{
  RowBox[{"pGamma", "[", 
   RowBox[{"x_", ",", "k_", ",", "\[Theta]_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"1", "/", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Gamma", "[", "k", "]"}], "*", 
      RowBox[{"\[Theta]", "^", "k"}]}], ")"}]}], "*", 
   RowBox[{"x", "^", 
    RowBox[{"(", 
     RowBox[{"k", "-", "1"}], ")"}]}], 
   RowBox[{"Exp", "[", 
    RowBox[{"-", 
     RowBox[{"(", 
      RowBox[{"x", "/", "\[Theta]"}], ")"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.9333263617766647`*^9, 3.9333264845460105`*^9}, {
  3.93338739618589*^9, 3.9333873995049915`*^9}, {3.933387959872659*^9, 
  3.9333879630252066`*^9}},
 CellLabel->"In[74]:=",ExpressionUUID->"891fa241-f2c5-414f-b997-9c3485222aad"],

Cell[BoxData[
 RowBox[{"Manipulate", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Show", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Histogram", "[", 
      RowBox[{"x", ",", "Automatic", ",", "\"\<PDF\>\""}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Plot", "[", 
      RowBox[{
       RowBox[{"pGamma", "[", 
        RowBox[{"t", ",", "k", ",", "\[Theta]"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"t", ",", "0", ",", "30"}], "}"}], ",", 
       RowBox[{"PlotRange", "->", "All"}]}], "]"}]}], "\[IndentingNewLine]", 
    "]"}], ",", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"k", ",", "1", ",", "10"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Theta]", ",", "1", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.9333263585253677`*^9, 3.9333263590266695`*^9}, {
   3.933326492368387*^9, 3.933326722383995*^9}, {3.933328644912425*^9, 
   3.933328644990556*^9}, {3.9333876248966827`*^9, 3.933387627316494*^9}, 
   3.9333879686844063`*^9, {3.933388170101801*^9, 3.933388170186483*^9}, {
   3.9333882217785764`*^9, 3.9333882226645365`*^9}, 3.9333883841194534`*^9, 
   3.9333898608509665`*^9},
 CellLabel->"In[75]:=",ExpressionUUID->"bc1aa6d1-9bac-4d24-afbb-26b84cb5520f"],

Cell[CellGroupData[{

Cell["Parameter Estimation", "Subsection",
 CellChangeTimes->{{3.933326764600464*^9, 3.9333267692075787`*^9}, {
   3.93338785530914*^9, 3.933387864830161*^9}, {3.9333879319767303`*^9, 
   3.933387932294136*^9}, 
   3.9333888636928988`*^9},ExpressionUUID->"8c7a6425-e86c-4965-b11b-\
214816ea93d2"],

Cell[BoxData[
 RowBox[{"\[Theta]hat", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"Mean", "[", 
     RowBox[{"x", " ", 
      RowBox[{"Log", "[", "x", "]"}]}], "]"}], "-", 
    RowBox[{
     RowBox[{"Mean", "[", "x", " ", "]"}], "*", 
     RowBox[{"Mean", "[", " ", 
      RowBox[{"Log", "[", "x", "]"}], "]"}]}]}], "//", "N"}]}]], "Input",
 CellChangeTimes->{{3.9333268078251033`*^9, 3.9333268895217705`*^9}, {
   3.9333285205811205`*^9, 3.933328563001614*^9}, {3.933330234640895*^9, 
   3.933330237160915*^9}, {3.9333304575242586`*^9, 3.933330457983017*^9}, 
   3.933389440558666*^9},
 CellLabel->"In[76]:=",ExpressionUUID->"e63c4667-c6a0-43e2-8bbf-d1537a789193"],

Cell[BoxData[
 RowBox[{"khat", "=", 
  RowBox[{
   RowBox[{"Mean", "[", "x", "]"}], "/", "\[Theta]hat"}]}]], "Input",
 CellChangeTimes->{{3.933326986928177*^9, 3.9333269966993456`*^9}},
 CellLabel->"In[77]:=",ExpressionUUID->"dd12309b-d343-417b-a294-e5969138b618"],

Cell[BoxData[
 RowBox[{"Nx", "=", 
  RowBox[{"Length", "[", "x", "]"}]}]], "Input",
 CellChangeTimes->{{3.9333269402836795`*^9, 3.9333269503142796`*^9}},
 CellLabel->"In[78]:=",ExpressionUUID->"53fdb063-b8f6-47fd-8f60-5530f12c39bd"],

Cell[BoxData[
 RowBox[{"\[Theta]tilde", "=", 
  RowBox[{
   RowBox[{"Nx", "/", 
    RowBox[{"(", 
     RowBox[{"Nx", "-", "1"}], ")"}]}], "\[Theta]hat"}]}]], "Input",
 CellChangeTimes->{{3.9333269610640926`*^9, 3.933326973180131*^9}},
 CellLabel->"In[79]:=",ExpressionUUID->"be8f61ae-197a-4b25-bc2a-f37878d96ae3"],

Cell[BoxData[
 RowBox[{"ktilde", "=", 
  RowBox[{"khat", "-", 
   RowBox[{
    RowBox[{"1", "/", "Nx"}], 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"3", "khat"}], "-", 
      RowBox[{
       RowBox[{"2", "/", "3"}], 
       RowBox[{"(", 
        RowBox[{"khat", "/", 
         RowBox[{"(", 
          RowBox[{"1", "+", "khat"}], ")"}]}], ")"}]}], "-", 
      RowBox[{
       RowBox[{"4", "/", "5"}], 
       RowBox[{"(", 
        RowBox[{"khat", "/", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"1", "+", "khat"}], ")"}], "^", "2"}]}], ")"}]}]}], 
     ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.9333285852831836`*^9, 3.9333286310659103`*^9}},
 CellLabel->"In[80]:=",ExpressionUUID->"19a3d77e-1511-4287-8a1f-dddd0b06adde"],

Cell[BoxData[
 RowBox[{"Show", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Histogram", "[", 
    RowBox[{"x", ",", "Automatic", ",", "\"\<PDF\>\""}], "]"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"pGamma", "[", 
      RowBox[{"t", ",", "khat", ",", "\[Theta]hat"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"t", ",", "0", ",", "30"}], "}"}]}], "]"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"pGamma", "[", 
      RowBox[{"t", ",", "ktilde", ",", "\[Theta]tilde"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"t", ",", "0", ",", "30"}], "}"}]}], "]"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input",
 CellChangeTimes->{{3.9333270415395293`*^9, 3.9333270474294167`*^9}, {
   3.9333876005532417`*^9, 3.933387617053028*^9}, 3.933387977595169*^9, {
   3.933388120164206*^9, 3.933388121420045*^9}, {3.9333881595982876`*^9, 
   3.933388159667296*^9}, 3.9333883991524305`*^9, {3.9333894635936265`*^9, 
   3.9333895053815193`*^9}, {3.9333898503483677`*^9, 3.933389854321689*^9}},
 CellLabel->"In[81]:=",ExpressionUUID->"fe8d8205-2ee3-4acb-89e1-f2358eb67fbd"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Gauss Distribution", "Section",
 CellChangeTimes->{{3.9333270903159533`*^9, 3.9333270932054615`*^9}, {
  3.933387561932927*^9, 3.933387564259147*^9}, {3.9333878143465967`*^9, 
  3.9333878159863577`*^9}, {3.933388033449794*^9, 
  3.933388033730052*^9}},ExpressionUUID->"4e002d57-c2c1-4e7f-a391-\
0564a2cef590"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Mean", "[", "x", "]"}], "//", "N"}]], "Input",
 CellChangeTimes->{{3.933327118559367*^9, 3.9333271237514944`*^9}},
 CellLabel->"In[82]:=",ExpressionUUID->"d7c41171-6b44-44b5-8b77-608ab9a7acf0"],

Cell[BoxData[
 RowBox[{"Variance", "[", "x", "]"}]], "Input",
 CellChangeTimes->{{3.9333286964434624`*^9, 3.9333287154002275`*^9}},
 CellLabel->"In[83]:=",ExpressionUUID->"3e062840-ba92-4188-8aed-9862b06fdcae"],

Cell[BoxData[
 RowBox[{
  RowBox[{"pGauss", "[", 
   RowBox[{"x_", ",", "\[Mu]_", ",", "\[Sigma]_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"1", "/", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"2", "\[Pi]", " ", 
      RowBox[{"\[Sigma]", "^", "2"}]}], "]"}]}], 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "1"}], "/", 
      RowBox[{"(", 
       RowBox[{"2", 
        RowBox[{"\[Sigma]", "^", "2"}]}], ")"}]}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"x", "-", "\[Mu]"}], ")"}], "^", "2"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.933387410375903*^9, 3.9333874683372693`*^9}},
 CellLabel->"In[84]:=",ExpressionUUID->"610f6528-36e0-4e35-a11e-0ea6fab0fcea"],

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"pGauss", "[", 
    RowBox[{"t", ",", 
     RowBox[{"Mean", "[", "x", "]"}], ",", 
     RowBox[{"StandardDeviation", "[", "x", "]"}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", 
     RowBox[{"-", "5"}], ",", "30"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.933328724063917*^9, 3.933328789016799*^9}, {
   3.9333288197208524`*^9, 3.9333288367714205`*^9}, {3.9333874800748053`*^9, 
   3.933387531582281*^9}, 3.933387594110528*^9, {3.933388155306959*^9, 
   3.933388155360341*^9}},
 CellLabel->"In[85]:=",ExpressionUUID->"3d1adcb0-98a5-43a0-a688-60e9bd04c84f"],

Cell[BoxData[
 RowBox[{"Show", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Histogram", "[", 
    RowBox[{"x", ",", "Automatic", ",", "\"\<PDF\>\""}], "]"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"pGauss", "[", 
      RowBox[{"t", ",", 
       RowBox[{"Mean", "[", "x", "]"}], ",", 
       RowBox[{"StandardDeviation", "[", "x", "]"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"t", ",", 
       RowBox[{"-", "5"}], ",", "30"}], "}"}]}], "]"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input",
 CellChangeTimes->{{3.9333270415395293`*^9, 3.9333270474294167`*^9}, {
   3.9333876005532417`*^9, 3.933387617053028*^9}, 3.9333880488137455`*^9, {
   3.9333881330864053`*^9, 3.9333881439410324`*^9}, 3.9333884064842176`*^9, {
   3.9333901574730115`*^9, 3.933390159991499*^9}},
 CellLabel->"In[86]:=",ExpressionUUID->"faa2ad47-138d-4065-a77c-bae422401c3f"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Poisson Distribution", "Section",
 CellChangeTimes->{{3.9333270903159533`*^9, 3.9333270932054615`*^9}, {
   3.933387561932927*^9, 3.933387564259147*^9}, 3.93338782419872*^9, {
   3.9333880353155584`*^9, 
   3.9333880355850897`*^9}},ExpressionUUID->"b4b1e876-3122-4599-a4e2-\
ffaee2dcbc2d"],

Cell[BoxData[
 RowBox[{
  RowBox[{"pPoisson", "[", 
   RowBox[{"x_", ",", "\[Lambda]_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{
    RowBox[{"\[Lambda]", "^", "x"}], "/", 
    RowBox[{"x", "!"}]}], 
   RowBox[{"Exp", "[", 
    RowBox[{"-", "\[Lambda]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.9333875669950175`*^9, 3.9333875851802287`*^9}},
 CellLabel->"In[87]:=",ExpressionUUID->"4be0d24f-ac95-4518-9e1a-240d99648a3a"],

Cell[BoxData[
 RowBox[{"Manipulate", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Show", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Histogram", "[", 
      RowBox[{"x", ",", "Automatic", ",", "\"\<PDF\>\""}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Plot", "[", 
      RowBox[{
       RowBox[{"pPoisson", "[", 
        RowBox[{"t", ",", "\[Lambda]"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"t", ",", "0", ",", "30"}], "}"}], ",", 
       RowBox[{"PlotRange", "->", "All"}]}], "]"}]}], "\[IndentingNewLine]", 
    "]"}], ",", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"\[Lambda]", ",", "1", ",", "20"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.9333263585253677`*^9, 3.9333263590266695`*^9}, {
   3.933326492368387*^9, 3.933326722383995*^9}, {3.933328644912425*^9, 
   3.933328644990556*^9}, {3.9333876248966827`*^9, 3.933387627316494*^9}, 
   3.9333879686844063`*^9, {3.933388170101801*^9, 3.933388170186483*^9}, {
   3.933388216267976*^9, 3.9333882642331657`*^9}, 3.933388332165801*^9, 
   3.9333884132828283`*^9},
 CellLabel->"In[88]:=",ExpressionUUID->"d4e10d66-0553-4265-86da-9065cbfc384e"],

Cell[BoxData[
 RowBox[{"Show", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Histogram", "[", 
    RowBox[{"x", ",", "Automatic", ",", "\"\<PDF\>\""}], "]"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"pPoisson", "[", 
        RowBox[{"t", ",", 
         RowBox[{"Mean", "[", "x", "]"}]}], "]"}], ",", "\[IndentingNewLine]", 
       RowBox[{"pPoisson", "[", 
        RowBox[{"t", ",", 
         RowBox[{"Variance", "[", "x", "]"}]}], "]"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"pPoisson", "[", 
        RowBox[{"t", ",", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Mean", "[", "x", "]"}], "+", 
            RowBox[{"Variance", "[", "x", "]"}]}], ")"}], "/", "2"}]}], 
        "]"}]}], "\[IndentingNewLine]", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"t", ",", "0", ",", "30"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"PlotLegends", "\[Rule]", 
      RowBox[{"Placed", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
         "\"\<Mean[x]\>\"", ",", "\"\<Var[x]\>\"", ",", 
          "\"\<(Mean[x]+Var[x])/2]\>\""}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"Right", ",", "Top"}], "}"}]}], "]"}]}]}], "]"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input",
 CellChangeTimes->{{3.933328724063917*^9, 3.933328789016799*^9}, {
   3.9333288197208524`*^9, 3.9333288367714205`*^9}, {3.9333874800748053`*^9, 
   3.933387531582281*^9}, 3.933387594110528*^9, {3.933387677318468*^9, 
   3.9333877347707834`*^9}, {3.9333883225822077`*^9, 3.933388327436116*^9}, {
   3.9333884212966337`*^9, 3.933388496292363*^9}, 3.9333886583501287`*^9},
 CellLabel->"In[89]:=",ExpressionUUID->"3673de57-d0bf-43ef-bcb0-1585013bb7cd"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Compare Gamma, Gauss, Poisson", "Section",
 CellChangeTimes->{{3.9333270903159533`*^9, 3.9333270932054615`*^9}, {
  3.933387561932927*^9, 3.933387564259147*^9}, {3.933387834915391*^9, 
  3.93338783521607*^9}, {3.9333879929614773`*^9, 
  3.9333879976674395`*^9}},ExpressionUUID->"dfd03e9e-2cac-42d0-a752-\
d582becb690b"],

Cell[BoxData[
 RowBox[{"fig", "=", 
  RowBox[{"Show", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Histogram", "[", 
     RowBox[{
      RowBox[{"noise", "//", "Flatten"}], ",", "Automatic", ",", 
      "\"\<PDF\>\"", ",", 
      RowBox[{"ChartStyle", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", "Gray", "}"}], ",", 
         RowBox[{"Opacity", "[", "0.2", "]"}]}], "}"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"AxesLabel", "->", 
       RowBox[{"{", 
        RowBox[{"\"\<Intensity\>\"", ",", "\"\<Probability\>\""}], " ", 
        "}"}]}]}], "]"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"pGamma", "[", 
         RowBox[{"t", ",", "khat", ",", "\[Theta]hat"}], "]"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{"pGauss", "[", 
         RowBox[{"t", ",", 
          RowBox[{"Mean", "[", "x", "]"}], ",", 
          RowBox[{"StandardDeviation", "[", "x", "]"}]}], "]"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{"pPoisson", "[", 
         RowBox[{"t", ",", 
          RowBox[{"Mean", "[", "x", "]"}]}], "]"}]}], "\[IndentingNewLine]", 
       "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"t", ",", 
        RowBox[{"-", "5"}], ",", "30"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{"PlotLegends", "\[Rule]", 
       RowBox[{"Placed", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
          "\"\<Gamma\>\"", ",", "\"\<Normal\>\"", ",", "\"\<Poisson\>\""}], 
          "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"Right", ",", "Top"}], "}"}]}], "]"}]}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.9333270415395293`*^9, 3.9333270474294167`*^9}, {
   3.9333288528374777`*^9, 3.9333289028100557`*^9}, {3.933328938951906*^9, 
   3.9333289437864227`*^9}, {3.933328990817052*^9, 3.9333290012027645`*^9}, {
   3.933329031997942*^9, 3.933329040124484*^9}, {3.933329090659999*^9, 
   3.933329197331318*^9}, {3.9333292352981544`*^9, 3.933329305305453*^9}, {
   3.933329357570759*^9, 3.9333294156328506`*^9}, {3.9333875528512707`*^9, 
   3.9333875545993814`*^9}, 3.93338798395947*^9, {3.9333883007096868`*^9, 
   3.9333883007565656`*^9}, {3.9333885120534053`*^9, 
   3.9333885245350275`*^9}, {3.9333899360514097`*^9, 3.9333899361298056`*^9}, 
   3.9333899682282653`*^9, {3.9333900359133615`*^9, 3.933390056360671*^9}, {
   3.935312306038414*^9, 3.9353123437958975`*^9}, {3.9358196149477296`*^9, 
   3.9358196162429323`*^9}, {3.9363493582584686`*^9, 3.936349401828788*^9}, {
   3.9363494567280855`*^9, 3.9363494622037*^9}, {3.936349518799396*^9, 
   3.936349519184656*^9}, {3.936349604226155*^9, 3.936349607101415*^9}},
 CellLabel->"In[90]:=",ExpressionUUID->"c35d6856-95d1-4e2f-b03a-0f407d6f4857"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Export", "[", 
   RowBox[{"\"\<figures/gammaFit.png\>\"", ",", "fig", ",", 
    RowBox[{"ImageResolution", "\[Rule]", "300"}]}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.824360795202119*^9, 3.824360797189836*^9}, {
   3.824368689133608*^9, 3.824368709519113*^9}, {3.8475276241782446`*^9, 
   3.8475276276281652`*^9}, {3.8502050757977505`*^9, 
   3.8502050766610193`*^9}, {3.8559956871937385`*^9, 3.855995690293275*^9}, {
   3.855995767320737*^9, 3.8559957690174584`*^9}, {3.8807779178363914`*^9, 
   3.880777993900387*^9}, 3.8817191185692377`*^9, {3.8817246619317017`*^9, 
   3.8817246639795055`*^9}, {3.9355622535063157`*^9, 
   3.9355622581455717`*^9}, {3.9358196364048147`*^9, 3.935819666004119*^9}, {
   3.9358197393648834`*^9, 3.9358197502779408`*^9}},
 CellLabel->"In[91]:=",ExpressionUUID->"7a76f3a1-b5bf-44af-857f-05d86a5f548f"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{794.1428571428571, 883.7142857142857},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
DockedCells->{},
FrontEndVersion->"13.3 for Microsoft Windows (64-bit) (June 3, 2023)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"6bda6bbd-2575-41c0-95b7-e3ae2d30b5c4"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 223, 4, 98, "Title",ExpressionUUID->"52db2253-f4df-4719-99fc-24d96372ebb6"],
Cell[806, 28, 243, 4, 35, "Text",ExpressionUUID->"b5a07bfa-b923-4cfd-ba0f-39a1ec2edac8"],
Cell[1052, 34, 291, 4, 28, "Input",ExpressionUUID->"a6487de7-fc3e-4f00-bb14-3f98052e2715"],
Cell[CellGroupData[{
Cell[1368, 42, 149, 3, 67, "Section",ExpressionUUID->"bf845f1f-1e99-4fe4-9ab7-640e3ec67311"],
Cell[1520, 47, 432, 10, 48, "Input",ExpressionUUID->"0671af08-9289-450a-9407-2afcac9a1e99"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1989, 62, 212, 4, 67, "Section",ExpressionUUID->"74588e31-3aec-43a6-b2b7-733d84438ad2"],
Cell[2204, 68, 435, 10, 48, "Input",ExpressionUUID->"b4303827-0539-4076-8e05-c3398bd7a4b6"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2676, 83, 154, 3, 67, "Section",ExpressionUUID->"998e8808-6053-46a1-a692-d945a8912e88"],
Cell[2833, 88, 771, 18, 105, "Input",ExpressionUUID->"2b9e0bd5-900d-4c75-b884-8077d5bc331b"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3641, 111, 154, 3, 67, "Section",ExpressionUUID->"3543b557-73c1-4ef9-ae0c-c7122c311353"],
Cell[3798, 116, 466, 12, 28, "Input",ExpressionUUID->"9bd17c56-809a-43a2-8579-9e87176a0fb7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4301, 133, 261, 4, 67, "Section",ExpressionUUID->"db5a1312-1599-49c9-bf2e-41de5948aefc"],
Cell[4565, 139, 296, 6, 28, "Input",ExpressionUUID->"ea2e0971-bb3d-48f8-a0fa-ea4585ff4db9"],
Cell[4864, 147, 860, 13, 28, "Input",ExpressionUUID->"ea34ba85-6a88-4faa-8059-f84a46904f83"],
Cell[5727, 162, 193, 3, 28, "Input",ExpressionUUID->"5a824ec1-303c-4292-96af-e06c9554edc2"],
Cell[5923, 167, 297, 6, 28, "Input",ExpressionUUID->"cafe2cdf-42c2-4877-9f53-0fc91561db10"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6257, 178, 211, 4, 67, "Section",ExpressionUUID->"18a85559-a49a-4631-86b2-ed289a2ab748"],
Cell[6471, 184, 450, 10, 86, "Input",ExpressionUUID->"e481ce51-af26-43fd-adf1-a0d4a9aad30f"],
Cell[6924, 196, 385, 9, 28, "Input",ExpressionUUID->"2b9186e5-7fb0-494f-8d6f-ff9516d23d46"],
Cell[7312, 207, 342, 7, 28, "Input",ExpressionUUID->"b2649cfa-e0a6-433f-b24f-22b3f72f9b95"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7691, 219, 150, 3, 67, "Section",ExpressionUUID->"c08b1ab1-a66a-4eaf-b0a0-dd682a45cde4"],
Cell[7844, 224, 422, 9, 28, "Input",ExpressionUUID->"e5da504c-77fc-4316-a2ad-1bd743c5e6cc"],
Cell[8269, 235, 937, 14, 28, "Input",ExpressionUUID->"74c9b1e6-e241-4307-94fc-728765b2c388"],
Cell[9209, 251, 298, 6, 28, "Input",ExpressionUUID->"7debda80-a1fb-4038-a016-f20f3cc2be82"],
Cell[9510, 259, 660, 17, 48, "Input",ExpressionUUID->"8f0600da-c236-4cb9-a554-fabc807d133a"],
Cell[10173, 278, 338, 5, 28, "Input",ExpressionUUID->"bb21bee1-6283-4c21-a40e-58a2fda94403"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10548, 288, 163, 3, 67, "Section",ExpressionUUID->"b33a93de-402e-41b2-81ef-ecf27f58ee63"],
Cell[10714, 293, 736, 20, 28, "Input",ExpressionUUID->"891fa241-f2c5-414f-b997-9c3485222aad"],
Cell[11453, 315, 1239, 26, 124, "Input",ExpressionUUID->"bc1aa6d1-9bac-4d24-afbb-26b84cb5520f"],
Cell[CellGroupData[{
Cell[12717, 345, 296, 5, 54, "Subsection",ExpressionUUID->"8c7a6425-e86c-4965-b11b-214816ea93d2"],
Cell[13016, 352, 662, 15, 28, "Input",ExpressionUUID->"e63c4667-c6a0-43e2-8bbf-d1537a789193"],
Cell[13681, 369, 264, 5, 28, "Input",ExpressionUUID->"dd12309b-d343-417b-a294-e5969138b618"],
Cell[13948, 376, 232, 4, 28, "Input",ExpressionUUID->"53fdb063-b8f6-47fd-8f60-5530f12c39bd"],
Cell[14183, 382, 313, 7, 28, "Input",ExpressionUUID->"be8f61ae-197a-4b25-bc2a-f37878d96ae3"],
Cell[14499, 391, 746, 23, 28, "Input",ExpressionUUID->"19a3d77e-1511-4287-8a1f-dddd0b06adde"],
Cell[15248, 416, 1152, 25, 105, "Input",ExpressionUUID->"fe8d8205-2ee3-4acb-89e1-f2358eb67fbd"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[16449, 447, 315, 5, 67, "Section",ExpressionUUID->"4e002d57-c2c1-4e7f-a391-0564a2cef590"],
Cell[16767, 454, 228, 4, 28, "Input",ExpressionUUID->"d7c41171-6b44-44b5-8b77-608ab9a7acf0"],
Cell[16998, 460, 210, 3, 28, "Input",ExpressionUUID->"3e062840-ba92-4188-8aed-9862b06fdcae"],
Cell[17211, 465, 692, 20, 28, "Input",ExpressionUUID->"610f6528-36e0-4e35-a11e-0ea6fab0fcea"],
Cell[17906, 487, 634, 14, 28, "Input",ExpressionUUID->"3d1adcb0-98a5-43a0-a688-60e9bd04c84f"],
Cell[18543, 503, 903, 20, 86, "Input",ExpressionUUID->"faa2ad47-138d-4065-a77c-bae422401c3f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19483, 528, 295, 5, 67, "Section",ExpressionUUID->"b4b1e876-3122-4599-a4e2-ffaee2dcbc2d"],
Cell[19781, 535, 422, 11, 28, "Input",ExpressionUUID->"4be0d24f-ac95-4518-9e1a-240d99648a3a"],
Cell[20206, 548, 1159, 24, 124, "Input",ExpressionUUID->"d4e10d66-0553-4265-86da-9065cbfc384e"],
Cell[21368, 574, 1802, 42, 181, "Input",ExpressionUUID->"3673de57-d0bf-43ef-bcb0-1585013bb7cd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23207, 621, 325, 5, 67, "Section",ExpressionUUID->"dfd03e9e-2cac-42d0-a752-d582becb690b"],
Cell[23535, 628, 2840, 61, 200, "Input",ExpressionUUID->"c35d6856-95d1-4e2f-b03a-0f407d6f4857"],
Cell[26378, 691, 876, 14, 28, "Input",ExpressionUUID->"7a76f3a1-b5bf-44af-857f-05d86a5f548f"]
}, Open  ]]
}, Open  ]]
}
]
*)

