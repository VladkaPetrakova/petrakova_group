// MH, v1.6, 2021_07_08

print("Run: Enhance first frame");

// global parameters
var inputDirectory, inputFilePath
var scaleBarWidth, scaleBarHeight, scaleBarFontSize, calibrationBarZoom, titleFontSize

// default values
//inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "jhi213/01_2021_03_26/first_frame/";     

//inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "jhi213/02_2021_05_25/first_frame/";          

//inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "jhi213/03_2021_07_30/first_frame/";      
    
scaleBarWidth = 1;
scaleBarHeight = 10;
scaleBarFontSize = 38;
calibrationBarZoom = 2;
titleFontSize = 38;

// input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: Enhance first frame" );

function processImage(inputFilePath){
    
    checkFileExistence(inputFilePath);

    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);                    
    
    // process only tiff files    
    if ( endsWith(fileName, ".tiff") || endsWith(fileName, ".tif") ){

        print("Processing file: " + fileName);    
        
        open(inputFilePath);                                                         

        // contrast
        run("Enhance Contrast", "saturated=0");        
        
        // scale to 1024x1024
        run("Size...", "width=1024 height=1024 depth=1 constrain average interpolation=None");    
        
        // scale bar
        run("Scale Bar...", "width=" + scaleBarWidth + " height=" + scaleBarHeight + 
            " font=" + scaleBarFontSize + " color=White background=None " + 
            "location=[Lower Left] bold overlay");
    
        // calibration bar
        run("Calibration Bar...", "location=[Lower Right] fill=White label=Black " + 
            "number=2 decimal=0 font=12 zoom=" + calibrationBarZoom + " overlay");
    
        // title         
        title = replace(fileNameWithoutExtension, "_", ", "); 
        title = replace(title, "pct", "%");               
        
        setFont("SansSerif", titleFontSize, "bold antialiased");
        setColor("white");
        setJustification("center");
        Overlay.drawString(title, getWidth/2, 1.5*titleFontSize, 0.0);        
        Overlay.show();              
     
        // output directory
        outputDirectory = File.getParent(inputFilePath) + File.separator;    

        // export  
        saveAs("PNG", outputDirectory + fileNameWithoutExtension + ".png");
        
        close();    
                  
    }        
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];        

        if (arg[0] == "scaleBarWidth")
            scaleBarWidth = parseInt(arg[1]);

        if (arg[0] == "scaleBarHeight")
            scaleBarHeight = parseInt(arg[1]);
            
        if (arg[0] == "scaleBarFontSize")
            scaleBarFontSize = parseInt(arg[1]);            
            
        if (arg[0] == "calibrationBarZoom")
            calibrationBarZoom = parseInt(arg[1]);            
            
        if (arg[0] == "titleFontSize")
            titleFontSize = parseInt(arg[1]);                
            
    }
                   
}
 
  
 
 
  



