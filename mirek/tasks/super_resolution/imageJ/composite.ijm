// MH, v1.2, 2021_06_30

print("Run: Composite");

var inputFilePaths, legendBackgroundX, legendBackgroundY, legendFontSize

// default values
inputFilePaths = newArray(
    "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
         "jhi213/01_2021_03_26/super_resolution_post_128x128/01_80RG_561nm_100ms.tiff",
    "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
         "jhi213/01_2021_03_26/super_resolution_post_128x128/02_80RG_561nm_200ms.tiff",
    "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
         "jhi213/01_2021_03_26/super_resolution_post_128x128/03_80RG_561nm_400ms.tiff"    
);  

legendBackgroundX = 380;
legendBackgroundY = 170;
legendFontSize = 36;

// input
setGlobalVariables( getArgument() );

// main
processComposition(inputFilePaths);

print( "Finished: Composite" );

function processComposition(inputFilePaths){

    nFiles = lengthOf(inputFilePaths);

    // check existence
    for (i = 0; i < nFiles; i++) {
        checkFileExistence( inputFilePaths[i] );    
    }

    // open 
    for (i = 0; i < nFiles; i++) {
        open( inputFilePaths[i] );    
    }

    // file names 
    fileNames = newArray( nFiles );
    fileNamesWithoutExtension = newArray( nFiles );

    for (i = 0; i < nFiles; i++) {
        fileNames[i] = File.getName( inputFilePaths[i] );    
        fileNamesWithoutExtension[i] = File.getNameWithoutExtension( inputFilePaths[i] );    
    }

    // print
    for (i = 0; i < nFiles; i++) {       
        print("Processing file: " + fileNames[i]);               
    }
    
    if (nFiles == 2) {
        // c1 - red, c2 - green, c3 - blue, c4 - gray, c5 - cyan, c6 - magenta, c7 - yellow
        run("Merge Channels...",
            "c2=" + fileNames[0] + " c6=" + fileNames[1] + " create ignore");
            
        colors = newArray("green", "magenta");

    }
    
    if (nFiles == 3) {
        run("Merge Channels...",
            "c2=" + fileNames[0] + " c7=" + fileNames[1] + " c6=" + fileNames[2] + " create ignore");

        colors = newArray("green", "yellow", "magenta");
        
    }
    
    run("RGB Color");
    
//    // draw black legend background
//    run("Colors...", "foreground=black background=white selection=yellow");
//    makeRectangle(0, 0, legendBackgroundX, legendBackgroundY);
//    run("Fill", "slice");
//    run("Select None");       
    
    // legend text
    setFont("SansSerif", legendFontSize, "bold antialiased");
    for (i = 0; i < nFiles; i++) {
        setColor(colors[i]);        
        title = replace(fileNamesWithoutExtension[i], "_", ", ");
        Overlay.drawString( title, legendFontSize/4, legendFontSize + i*1.25*legendFontSize, 0.0);
    }
    Overlay.show();

    // output directory
    outputDirectory = File.getParent(File.getParent(inputFilePaths[0])) + File.separator +
        "composite" + File.separator;

    createNonExistingDirectory(outputDirectory);
    
    outputFileName = fileNamesWithoutExtension[0];
    for (i = 1; i < nFiles; i++) {
        outputFileName = outputFileName + "-" + fileNamesWithoutExtension[i];
    }

    // export     
    saveAs("PNG", outputDirectory + outputFileName  + ".png");
    saveAs("TIFF", outputDirectory + outputFileName  + ".tiff");
    
    close("*");
    
}


function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputFilePaths")
            inputFilePaths = split( arg[1], "," );
            
        if (arg[0] == "legendBackgroundX")
            legendBackgroundX = parseInt(arg[1]);      
                     
        if (arg[0] == "legendBackgroundY")
            legendBackgroundY = parseInt(arg[1]);    
                       
        if (arg[0] == "legendFontSize")
            legendFontSize = parseInt(arg[1]);               
            
    }
                   
}


