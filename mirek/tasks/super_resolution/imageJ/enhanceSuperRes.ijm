// MH, v1.4, 2021_05_27 

print("Run: Enhance super resolution images");

// global parameters
var inputDirectory, calibrationBarZoom, scaleBarWidth, scaleBarHeight, scaleBarFontSize, titleFontSize

// default values
//inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "jhi213/01_2021_03_26/super_resolution_128x128/";
// inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "jhi213/01_2021_03_26/super_resolution_post_128x128/";

 inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "biocev/01_2021_05_26/super_resolution_128x128/";
// inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "biocev/01_2021_05_26/super_resolution_post_128x128/";

//scaleBarWidth = 1;
//calibrationBarZoom = 2;
//scaleBarHeight = 10;
//scaleBarFontSize = 36;
//titleFontSize = 36;

scaleBarWidth = 1;
calibrationBarZoom = 8;
scaleBarHeight = 40;
scaleBarFontSize = 144;
titleFontSize = 144;

// input
setGlobalVariables( getArgument() );

// main
//processImage(inputDirectory + "01_80RG_561nm_100ms.tiff");
//processImage(inputDirectory + "02_80RG_561nm_200ms.tiff");

//processImage(inputDirectory + "01_80RG_561nm_200ms_20pct_160nm.tiff");

processFolder(inputDirectory);

print( "Finished: Enhance super resolution images" );

function processImage(inputFilePath){
    
    checkFileExistence(inputFilePath);

    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);  
            
    // process only tiff files    
    if ( endsWith(fileName, ".tiff") || endsWith(fileName, ".tif") ){

        print("Processing file: " + fileName);    
        
        open(inputFilePath);                                           

        // scale to 1024x1024
        run("Size...", "width=1024 height=1024 depth=1 constrain interpolation=None");        
        
        // scale bar       
        run("Scale Bar...", "width=" + scaleBarWidth + " height=" + scaleBarHeight + " font=" + 
            scaleBarFontSize + " color=White background=None location=[Lower Left] bold overlay");

        // calibration bar
        run("Calibration Bar...", "location=[Lower Right] fill=White label=Black " +
             "number=2 decimal=1 font=12 zoom=" +  calibrationBarZoom + " overlay");

        // title
        title = replace(fileNameWithoutExtension, "_", ", "); 
        title = replace(title, "pct", "%");         
        
        setFont("SansSerif", titleFontSize, "bold antialiased");
        setColor("white");
        setJustification("center");
        Overlay.drawString(title, getWidth/2, 1.5*titleFontSize, 0.0);        
        Overlay.show();        

        // output directory
        outputDirectory = File.getParent(inputFilePath) + File.separator;
        outputFilePath = outputDirectory + fileNameWithoutExtension + ".png";

        // export   
        saveAs("PNG", outputFilePath);
        
        close();
        
    }

}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];        

        if (arg[0] == "scaleBarWidth")
            scaleBarWidth = parseInt(arg[1]);

        if (arg[0] == "scaleBarHeight")
            scaleBarHeight = parseInt(arg[1]);
            
        if (arg[0] == "scaleBarFontSize")
            scaleBarFontSize = parseInt(arg[1]);            
            
        if (arg[0] == "calibrationBarZoom")
            calibrationBarZoom = parseInt(arg[1]);            
            
        if (arg[0] == "titleFontSize")
            titleFontSize = parseInt(arg[1]);            
    
            
    }
                   
}
 
  
 
 
    



