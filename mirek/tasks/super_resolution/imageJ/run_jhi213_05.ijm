// MH, v1.0, 2021_09_09

print("\\Clear");

//setBatchMode(true);

// ***** Measurement parameters *****

measurement = "jhi213/05_2021_09_09"; 

thunderStormImageNames1 = newArray(
    "01_80RG_ROI01_561nm_50pct_100ms_500gain_100nm",
    "02_80RG_ROI01_561nm_50pct_200ms_500gain_100nm",
    "03_80RG_ROI02_561nm_50pct_100ms_500gain_100nm",
    "04_80RG_ROI02_561nm_50pct_200ms_500gain_100nm"
    );
    
thunderStormImageNames2 = newArray(    
    "05_80RG_ROI03_643nm_100pct_100ms_500gain_100nm",
    "06_80RG_ROI04_643nm_100pct_200ms_500gain_100nm",
    "07_80RG_ROI05_643nm_100pct_100ms_500gain_100nm",
    "08_80RG_ROI05_643nm_100pct_200ms_500gain_100nm"
    );
    
roiImageNames1 = newArray(
    "01_80RG_ROI01_561nm_50pct_100ms_500gain_100nm",
    "07_80RG_ROI05_643nm_100pct_100ms_500gain_100nm"
    );

videoImageNames = newArray(
    "01_80RG_ROI01_561nm_50pct_100ms_500gain_100nm",
    "07_80RG_ROI05_643nm_100pct_100ms_500gain_100nm"
    );
    
// paths
macroPath = "C:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/tasks/super_resolution/imageJ/";
measurementDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    measurement + File.separator;

// start time
startTime_ms = startTime();

// ***** Main *****

print("Processing Measurement: " + measurement.replace("/", ", ") );

//runMacro(macroPath+"preProcess.ijm", "inputDirectory="+measurementDirectory+"converted/");
//runMacro(macroPath+"setPixelSize.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/ " +
//    "pixelSize_nm=99"
//    );
//runMacro(macroPath+"crop.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/");
//runMacro(macroPath+"firstFrame.ijm", "inputDirectory="+measurementDirectory+"image_stack_128x128/" );
runMacro(macroPath+"enhanceFirstFrame.ijm", "inputDirectory="+measurementDirectory+"first_frame/ " + 
    "titleFontSize=38");

//for (i = 0; i < lengthOf(thunderStormImageNames1); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames1[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=500 " +
//        "cameraBaseLevelOffset=65 " + 
//        "cameraPhotoelectronsPerAdCount=12.05 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=3 " +          
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//
//        "multiEmitterFitting=false " +
////        "multiEmitterFitting=true " +
////        "multiEmitterFittingSameIntensity=false " +
////        "multiEmitterFittingMaxEmitters=5 " +
////        "multiEmitterFittingFixedIntensity=true " +
////        "multiEmitterFittingExpectedIntensityMin=500  " +
////        "multiEmitterFittingExpectedIntensityMax=2500 " +
////        "multiEmitterFittingPvalue=1.0E-6 " +    
//
//        "removeDuplicates=0 " +     
////        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=8 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=0 " +
//        "filterSigmaMax=170 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=2 " +
//        "driftMagnification=4 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//        
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames2); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames2[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=500 " +
//        "cameraBaseLevelOffset=65 " + 
//        "cameraPhotoelectronsPerAdCount=12.05 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=3 " +          
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//
////        "multiEmitterFitting=false " +
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
////        "removeDuplicates=0 " +     
//        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=8 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=0 " +
//        "filterSigmaMax=170 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=8 " +
//        "driftMagnification=4 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//        
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//runMacro( macroPath + "enhanceSuperRes.ijm",
//    "inputDirectory=" + measurementDirectory + "super_resolution_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"    
//    );
//
//runMacro( macroPath + "enhanceSuperRes.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"        
//    );
//
//runMacro( macroPath + "titleDrift.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/" );

//for (i = 0; i < lengthOf(videoImageNames); i++) {
//    runMacro(macroPath+"video.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+videoImageNames[i]+".tiff " +        
//        "processIndividualImage=1 " +
//        "exposureTimeIndex=5"
//        );
//}

// ***** Processing of ROIs *****

//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "roi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " + 
//        "roiWidth=32 roiHeight=32" );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "enhanceRoi.ijm",  "inputDirectory=" + measurementDirectory + 
//        "roi/" +  roiImageNames1[i] + "/ " + 
//        "scaleBarWidth=80"
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "superResRoi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " +
//        "roiWidth=32 " +
//        "roiHeight=32 " +
//        "roiStrokeWidth=4 " +
//        "roiTitleFontSize=36 " +
//        "scaleBarWidth=1 " +
//        "calibrationBarZoom=2 " +
//        "scaleBarHeight=10 " +
//        "scaleBarFontSize=36 " +
//        "titleFontSize=36"  
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "montageRoi.ijm",  "inputDirectory=" + measurementDirectory + 
//        "roi/" + roiImageNames1[i] + "/");    
//}

// ***** Finish *****
                        
print("Finished Measurement: " + measurement.replace("/", ", ") );

// finish time
finishTime(startTime_ms);

