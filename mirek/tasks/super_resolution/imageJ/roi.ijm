// MH, v1.7, 2021_06_30

print("Run: ROI");

// global parameters
var inputDirectory, inputFilePath, roiWidth, roiHeight

// default values
//inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" + 
//    "jhi213/01_2021_03_26/super_resolution_post_128x128/";   
 inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "biocev/01_2021_05_26/super_resolution_post_128x128/";

roiWidth = 32;
roiHeight = 32;
// roiWidth = 64;
// roiHeight = 64;

// input
setGlobalVariables( getArgument() );

//processImage(inputDirectory + "01_80RG_561nm_100ms.tiff");
//processImage(inputDirectory + "02_80RG_561nm_200ms.tiff");

//processImage(inputDirectory + "01_80RG_561nm_200ms_20pct_160nm.tiff");

processImage(inputFilePath);

print( "Finished: ROI" );

function processImage(inputFilePath){   

    checkFileExistence(inputFilePath);
            
    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);    

    print("Processing file: " + fileName);    
      
    // roi origins path
    roiOriginPath = File.getParent(File.getParent(inputFilePath)) + 
        File.separator + "roiOrigin" + File.separator + fileNameWithoutExtension + ".csv";
    checkFileExistence(roiOriginPath);

    // roi origins in result table
    open(roiOriginPath);
    IJ.renameResults("Results");

    outputParentDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator + 
        "roi" + File.separator;
    createNonExistingDirectory(outputParentDirectory);            
        
    outputDirectory =  outputParentDirectory + fileNameWithoutExtension + File.separator;
    createNonExistingDirectory(outputDirectory);    

    open(inputFilePath);    
    
    for (i = 0; i < nResults(); i++) {

        imageName = IJ.pad(i+1,2);    
        
        print("Processing: ROI" + imageName + ".tiff");

        run("Specify...", "width=" + roiWidth + " height=" + roiHeight + 
            " x=" + getResult("X", i) + " y=" + getResult("Y", i) + " centered");
               
        run("Duplicate...", "title=" + imageName + ".tiff");
           
        saveAs("Tiff",  outputDirectory + imageName + ".tiff");
    
        close();    
    }   
    
    close();    

    close("Results");
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];
            
        if (arg[0] == "inputFilePath")
            inputFilePath = arg[1];  
                      
        if (arg[0] == "roiWidth")
            roiWidth = parseInt(arg[1]); 
                             
        if (arg[0] == "roiHeight")
            roiHeight = parseInt(arg[1]);                              
                        
    }                   
    
}




