// MH, v1.3, 2021_08_25

print("Run: Set pixel size");

var inputDirectory, pixelSize_nm

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/image_stack_512x512/";
//pixelSize_nm = 100;

// input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: Set pixel size" );

function processImage(inputFilePath){       

    checkFileExistence(inputFilePath);
            
    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);


    if ( endsWith(fileName, ".tiff") || endsWith(fileName, ".tif") ){
                
        print("Processing file: " + fileName);
    
        open(inputFilePath);                                                             
        
        // set pixel size
        pixelSize_um = pixelSize_nm / 1000;
                
        Stack.setXUnit("um");
        Stack.setYUnit("um");
        
        Stack.getDimensions(width, height, channels, slices, frames);        
                
        run("Properties...", "channels=" + channels + " slices=" + slices + " frames=" + frames +  
            " pixel_width=" + pixelSize_um + " pixel_height=" + pixelSize_um + " voxel_depth=1");    
    
        // output directory       
        outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator +
            "image_stack_" + width + "x" + height + File.separator;   
        createNonExistingDirectory(outputDirectory);
    
        // export
        saveAs("Tiff", outputDirectory + fileNameWithoutExtension + ".tiff");
        
        close();

    }
    
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];

        if (arg[0] == "pixelSize_nm")
            pixelSize_nm = parseInt(arg[1]);            
            
    }
                   
}
