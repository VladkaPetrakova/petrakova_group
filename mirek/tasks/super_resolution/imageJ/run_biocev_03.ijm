// MH, v1.1, 2021_09_23

print("\\Clear");

//setBatchMode(true);

// ***** Measurement parameters *****

measurement = "biocev/03_2021_09_13"; 

//thunderStormImageNames1 = newArray(
//    "05_80RG_ROI05_647nm_100pct_300gain_160nm_100ms",
//    "18_80RG_ROI14_561nm_100pct_300gain_160nm_100ms"
//    );

thunderStormImageNames1 = newArray(
    "01_80RG_ROI01_647nm_20pct_300gain_160nm_100ms",
    "02_80RG_ROI02_647nm_40pct_300gain_160nm_100ms",
    "03_80RG_ROI03_647nm_60pct_300gain_160nm_100ms",
    "04_80RG_ROI04_647nm_80pct_300gain_160nm_100ms",
    "05_80RG_ROI05_647nm_100pct_300gain_160nm_100ms",
    "06_80RG_ROI06_647nm_60pct_300gain_160nm_50ms",
    "07_80RG_ROI07_647nm_60pct_300gain_160nm_150ms",
    "08_80RG_ROI08_647nm_60pct_300gain_160nm_200ms",
    "09_80RG_ROI09_647nm_60pct_300gain_160nm_100ms",
    "10_80RG_ROI09_647nm_60pct_300gain_160nm_100ms",
    "11_80RG_ROI09_647nm_60pct_300gain_160nm_100ms",
    "12_80RG_ROI10_561nm_20pct_300gain_160nm_100ms",
    "14_80RG_ROI12_561nm_60pct_300gain_160nm_100ms",
    "15_80RG_ROI12_561nm_60pct_300gain_160nm_100ms",
    "16_80RG_ROI12_561nm_60pct_300gain_160nm_100ms",
    "17_80RG_ROI13_561nm_80pct_300gain_160nm_100ms",
    "18_80RG_ROI14_561nm_100pct_300gain_160nm_100ms",
    "20_80RG_ROI16_561nm_50pct_300gain_160nm_150ms",
    "21_80RG_ROI17_561nm_50pct_300gain_160nm_200ms"  
    );

thunderStormImageNames2 = newArray(
    "13_80RG_ROI11_561nm_40pct_300gain_160nm_100ms",
    "19_80RG_ROI15_561nm_50pct_300gain_160nm_50ms" 
    );
    
thunderStormImageNames3 = newArray(
    "22_80RY_ROI01_561nm_20pct_300gain_160nm_100ms",
    "23_80RY_ROI02_561nm_40pct_300gain_160nm_100ms",
    "24_80RY_ROI03_561nm_60pct_300gain_160nm_100ms",
    "25_80RY_ROI03_561nm_60pct_300gain_160nm_100ms",
    "26_80RY_ROI03_561nm_60pct_300gain_160nm_100ms",
    "27_80RY_ROI04_561nm_80pct_300gain_160nm_100ms",
    "28_80RY_ROI05_561nm_100pct_300gain_160nm_100ms",
    "29_80RY_ROI06_561nm_50pct_300gain_160nm_50ms",
    "30_80RY_ROI07_561nm_50pct_300gain_160nm_150ms",
    "31_80RY_ROI08_561nm_50pct_300gain_160nm_200ms"    
    );
        
roiImageNames1 = newArray(
    "05_80RG_ROI05_647nm_100pct_300gain_160nm_100ms",
    "18_80RG_ROI14_561nm_100pct_300gain_160nm_100ms"
    );

videoImageNames = newArray(
    "05_80RG_ROI05_647nm_100pct_300gain_160nm_100ms",
    "18_80RG_ROI14_561nm_100pct_300gain_160nm_100ms"
    );
   
// paths
macroPath = "C:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/tasks/super_resolution/imageJ/";
measurementDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    measurement + File.separator;

// start time
startTime_ms = startTime();

// ***** Main *****

print("Processing Measurement: " + measurement );

//runMacro(macroPath+"convertBiocev.ijm", "inputDirectory="+measurementDirectory+"raw/" );
//runMacro(macroPath+"preProcess.ijm", "inputDirectory="+measurementDirectory+"converted/" );
//runMacro(macroPath+"crop.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/" );
//runMacro(macroPath+"firstFrame.ijm", "inputDirectory="+measurementDirectory+"image_stack_128x128/" );
runMacro(macroPath+"enhanceFirstFrame.ijm", "inputDirectory="+measurementDirectory+"first_frame/" +
    " titleFontSize=34");

//for (i = 0; i < lengthOf(thunderStormImageNames1); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames1[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=300 " +
//        "cameraBaseLevelOffset=100 " + 
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=3 " +          
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//
////        "multiEmitterFitting=false " +    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
////        "removeDuplicates=0 " +         
//        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=30 " +
//        "filterSigmaMax=200 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=8 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//        
//        "densityFilter=0 " +
//    
//        "merging=0 " + 
//
//        "renderGaussianSigma=10"
//        
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames2); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames2[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=300 " +
//        "cameraBaseLevelOffset=100 " + 
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=3 " +          
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//
////        "multiEmitterFitting=false " +    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//        "removeDuplicates=1 " +         
////        "removeDuplicates=0 " +     
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=30 " +
//        "filterSigmaMax=200 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=5 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//        
//        "densityFilter=0 " +
//    
//        "merging=0"
//        );
//}

//runMacro( macroPath + "enhanceSuperRes.ijm",
//    "inputDirectory=" + measurementDirectory + "super_resolution_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"    
//    );
//
//runMacro( macroPath + "enhanceSuperRes.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"        
//    );
//
//runMacro( macroPath + "titleDrift.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/" );

//for (i = 0; i < lengthOf(videoImageNames); i++) {
//    runMacro(macroPath+"video.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+videoImageNames[i]+".tiff " +        
//        "processIndividualImage=1 " +
//        "exposureTimeIndex=7"
//        );
//}

// ***** Processing of ROIs *****

//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "roi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " + 
//        "roiWidth=64 roiHeight=64" );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "enhanceRoi.ijm",  "inputDirectory=" + measurementDirectory + 
//        "roi/" +  roiImageNames1[i] + "/ " + 
//        "scaleBarWidth=80"
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "superResRoi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " +
//        "roiWidth=64 " +
//        "roiHeight=64 " +
//        "roiStrokeWidth=2 " +
//        "roiTitleFontSize=80 " +
//        "scaleBarWidth=1 " +
//        "calibrationBarZoom=2 " +
//        "scaleBarHeight=10 " +
//        "scaleBarFontSize=36 " +
//        "titleFontSize=36"  
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "montageRoi.ijm",  "inputDirectory=" + measurementDirectory + 
//        "roi/" + roiImageNames1[i] + "/");    
//}

// ***** Finish *****
                        
print("Finished Measurement: " + measurement );

// finish time
finishTime(startTime_ms);

