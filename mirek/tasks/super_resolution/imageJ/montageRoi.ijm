// MH, v1.1, 2021_06_30

print("Run: Montage of ROI");

// global parameters
var inputDirectory

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/roi/01_80RG_561nm_100ms/";

// input
setGlobalVariables( getArgument() );

// main
processDirectory(inputDirectory);

print("Finished: Montage of ROI");

function processDirectory(inputDirectory){    

    checkDirectoryExistence(inputDirectory);

    directoryName = File.getName(inputDirectory);   

    print("Processing directory: " + directoryName);    
    
    // open all *.png images
    fileList = getFileList(inputDirectory);
    
    for (i = 0; i < lengthOf(fileList); i++) {
        
        fileName = fileList[i];    
    
        // process only tiff files
        if ( endsWith(fileName, ".png") ){               
    
            inputFilePath = inputDirectory + fileName;
                    
            open(inputFilePath);
    
        }    
    }  

    // make montage from image stack
    run("Images to Stack", "name=Stack title=[] use");
    run("Make Montage...", "columns=4 rows=3 scale=1");

    // output irectory
    outputDirectory = File.getParent(File.getParent(inputDirectory)) + File.separator +
        "montageRoi" + File.separator;

    createNonExistingDirectory(outputDirectory);
    
    // export
    saveAs("PNG", outputDirectory + directoryName + ".png");       

    close("*");

}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];                                       
                        
    }                   
    
}






