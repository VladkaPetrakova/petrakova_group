// MH, v1.0, 2021_08_28

print("Run: Convert BIOCEV data");

var inputDirectory

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "biocev/02_2021_08_04/raw/";

 // input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: Convert BIOCEV data" );

function processImage(inputFilePath){       

    checkFileExistence(inputFilePath);
            
    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);

    if ( endsWith(fileName, ".nd2") ){
                
        print("Processing file: " + fileName);
    
        open(inputFilePath);                                   
                  
        // output directory       
        outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator +
            "converted" + File.separator;   
        createNonExistingDirectory(outputDirectory);
    
        // export
        saveAs("Tiff", outputDirectory + fileNameWithoutExtension + ".tiff");
        
        close();

    }
        
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];        
            
    }
                   
}

