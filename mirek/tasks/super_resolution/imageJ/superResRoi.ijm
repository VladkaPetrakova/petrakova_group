// MH, v1.3, 2021_08_25

print("Run: Super resolution images with ROI");

// global parameters
var inputDirectory, inputFilePath, calibrationBarZoom, scaleBarHeight, scaleBarFontSize, titleFontSize, 
    roiWidth, roiHeight, roiStrokeWidth, roiTitleFontSize, showCalibrationBar, showTitle, scaleBarWidth
    
// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" + 
    "jhi213/01_2021_03_26/super_resolution_post_128x128/";

scaleBarWidth = 1;

calibrationBarZoom = 2;
scaleBarHeight = 10;
scaleBarFontSize = 36;
titleFontSize = 36;

roiWidth = 32;
roiHeight = 32;
// roiWidth = 64;
// roiHeight = 64;
roiStrokeWidth = 4;
roiTitleFontSize = 32;

showCalibrationBar = true;
showTitle = true;

// input
setGlobalVariables( getArgument() );

// main
//processImage(inputDirectory + "01_80RG_561nm_100ms.tiff");

processImage(inputFilePath);

print( "Finished: Super resolution images with ROI" );

function processImage(inputFilePath){       

    checkFileExistence(inputFilePath);
            
    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);    

    print("Processing file: " + fileName);    
    
    // roi origins path
    roiOriginPath = File.getParent(File.getParent(inputFilePath)) + 
        File.separator + "roiOrigin" + File.separator + fileNameWithoutExtension + ".csv";
    checkFileExistence(roiOriginPath);

    // roi origins in result table
    open(roiOriginPath);
    IJ.renameResults("Results");

    // roi stroke width    
    run("Roi Defaults...", "color=yellow stroke=" + roiStrokeWidth + " group=0");

    open(inputFilePath);                                
    
    // loop over rois
    for (i = 0; i < nResults(); i++) {
    
        // roi title
        roiTitle = IJ.pad(i+1,2);
    
        // insert roi title
        setFont("SansSerif", roiTitleFontSize, "bold antialiased");
        setColor("yellow");
        Overlay.drawString( roiTitle , getResult("X", i) - roiTitleFontSize/2, getResult("Y", i) - roiHeight/2 - 0.25* roiTitleFontSize, 0.0 ); 
        Overlay.show();
    
        // draw roi rectangles        
        run("Specify...", "width=" + roiWidth + " height=" + roiHeight + 
            " x=" + getResult("X", i) + " y=" + getResult("Y", i) + " centered");            
        run("Add Selection...");           
           
    }   
    
    run("Select None");

    // scale to 1024x1024
    run("Size...", "width=1024 height=1024 depth=1 constrain interpolation=None");       

    // scale bar       
    run("Scale Bar...", "width=" + scaleBarWidth + " height=" + scaleBarHeight + " font=" + scaleBarFontSize +
        " color=White background=None location=[Lower Left] bold overlay");

    if (showCalibrationBar)               
        run("Calibration Bar...", "location=[Lower Right] fill=White label=Black " +
             "number=2 decimal=1 font=12 zoom=" +  calibrationBarZoom + " overlay");

    if (showTitle){
    
        title = replace(fileNameWithoutExtension, "_", ", "); 
        title = replace(title, "pct", "%");  
        
        setFont("SansSerif", titleFontSize, "bold antialiased");
        setColor("white");
        setJustification("center");
        Overlay.drawString(title, getWidth/2, 1.5*titleFontSize, 0.0);        
        Overlay.show();        
        
    }
    
    // output directory
    outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator + 
        "super_resolution_roi_128x128";
    createNonExistingDirectory(outputDirectory);

    // export
    saveAs("PNG",  outputDirectory + File.separator + fileNameWithoutExtension + ".png");
    
    close();

    close("Results");

}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];
            
        if (arg[0] == "inputFilePath")
            inputFilePath = arg[1];       
                             
        if (arg[0] == "calibrationBarZoom")
            calibrationBarZoom = parseInt(arg[1]);  
                                  
        if (arg[0] == "scaleBarHeight")
            scaleBarHeight = parseInt(arg[1]);    
            
        if (arg[0] == "scaleBarWidth")
            scaleBarWidth = parseInt(arg[1]);  
                  
        if (arg[0] == "scaleBarFontSize")
            scaleBarFontSize = parseInt(arg[1]); 
                                   
        if (arg[0] == "titleFontSize")
            titleFontSize = parseInt(arg[1]);  
                                  
        if (arg[0] == "roiWidth")
            roiWidth = parseInt(arg[1]);  
                            
        if (arg[0] == "roiHeight")
            roiHeight = parseInt(arg[1]);   
                        
        if (arg[0] == "roiStrokeWidth")
            roiStrokeWidth = parseInt(arg[1]); 
                                   
        if (arg[0] == "roiTitleFontSize")
            roiTitleFontSize = parseInt(arg[1]);   
            
        if (arg[0] == "showCalibrationBar")
            showCalibrationBar = parseInt(arg[1]);   
            
        if (arg[0] == "showTitle")
            showTitle = parseInt(arg[1]);                                                                                                           
                        
    }                   
    
}
