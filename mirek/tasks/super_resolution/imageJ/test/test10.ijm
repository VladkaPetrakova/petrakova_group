// MH, v1.0, 2021_08_20

args = split(getArgument(), " ");

// default values
inputDirectory = "";
x = 0;

for (i = 0; i < lengthOf(args); i++) {

    assignment = split(args[i], "=");
    
    if (assignment[0] == "inputDirectory")
        inputDirectory = assignment[1];

    if (assignment[0] == "x")
        x = assignment[1];

}

print(inputDirectory);
print(x);