
print("\\Clear");

getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
print("Start time: " + IJ.pad(hour,2) + ":" + IJ.pad(minute,2) + ":" + IJ.pad(second,2));

startTime_ms = getTime();
//print(startTime_ms);

wait(1000);

endTime_ms = getTime();
//print(endTime_ms);

totalTime_ms = endTime_ms - startTime_ms;

totalTime_s = totalTime_ms/1000;

//print(totalTime_s);

getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
print("Finish time: " + IJ.pad(hour,2) + ":" + IJ.pad(minute,2) + ":" + IJ.pad(second,2));

totalTime_min = Math.floor(totalTime_s/60);

print("Total time: " + totalTime_min + "min " + Math.round(totalTime_s-60*totalTime_min) + "s");