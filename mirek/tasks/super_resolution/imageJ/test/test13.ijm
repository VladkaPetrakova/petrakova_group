path = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/roiOrigin/01_80RG_561nm_100ms.csv";

open(path);

IJ.renameResults("Results");

nRois = nResults();

for (i=0; i< nRois; i++){
    print("x: " + getResult("X", i) + ", y: " + getResult("Y", i));
}
