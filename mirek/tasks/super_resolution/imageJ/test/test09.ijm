
inputFilePath = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    "jhi/2021_03_26/image_stack_128x128/80RG_561nm_100ms.tiff";

open(inputFilePath);

getPixelSize(unit, pixelWidth, pixelHeight);

print("unit: " + unit);
print("pixelWidth: " + pixelWidth);
print("pixelHeight: " + pixelHeight);
