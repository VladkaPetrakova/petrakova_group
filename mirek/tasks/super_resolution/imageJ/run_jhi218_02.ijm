// MH, v1.0, 2021_09_10

print("\\Clear");

//setBatchMode(true);

// ***** Measurement parameters *****

measurement = "jhi218/02_2021_09_10"; 

thunderStormImageNames1 = newArray(
    "01_80RG_ROI01_638nm_10pct_500gain_160nm_100ms",
    "02_80RG_ROI02_638nm_20pct_500gain_160nm_100ms",
    "03_80RG_ROI09_638nm_30pct_500gain_160nm_100ms",
    "04_80RG_ROI04_638nm_40pct_500gain_160nm_100ms",
    "05_80RG_ROI05_638nm_50pct_500gain_160nm_100ms",
    "06_80RG_ROI06_638nm_60pct_500gain_160nm_100ms",
    "07_80RG_ROI07_638nm_70pct_500gain_160nm_100ms",
    "08_80RG_ROI08_638nm_80pct_500gain_160nm_100ms",
    "09_80RG_ROI09_638nm_25pct_500gain_160nm_100ms",
    "10_80RG_ROI09_638nm_25pct_500gain_160nm_100ms",
    "11_80RG_ROI09_638nm_25pct_500gain_160nm_100ms"
    );    
   
// paths
macroPath = "C:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/tasks/super_resolution/imageJ/";
measurementDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    measurement + File.separator;

// start time
startTime_ms = startTime();

// ***** Main *****

print("Processing Measurement: " + measurement.replace("/", ", ") );

//runMacro(macroPath+"preProcess.ijm", "inputDirectory="+measurementDirectory+"converted/");
//runMacro(macroPath+"setPixelSize.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/ " +
//    "pixelSize_nm=160"
//    );
//runMacro(macroPath+"crop.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/");
//runMacro(macroPath+"firstFrame.ijm", "inputDirectory="+measurementDirectory+"image_stack_128x128/" );
//runMacro(macroPath+"enhanceFirstFrame.ijm", "inputDirectory="+measurementDirectory+"first_frame/ " + 
//    "titleFontSize=38");

for (i = 0; i < lengthOf(thunderStormImageNames1); i++) {
    runMacro(macroPath+"thunderStorm.ijm", 
        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames1[i]+".tiff " +
        
        "processIndividualImage=1 " +
    
        "cameraGain=500 " +
        "cameraBaseLevelOffset=65 " + 
        "cameraPhotoelectronsPerAdCount=12.05 " + 
        "cameraQuantumEfficiency=0.95 " +
        
        "fittingRadius=3 " +          
        "fittingMethod=Weighted-Least-squares " +
        "fittingInitialSigma=1.5 " +        

//        "multiEmitterFitting=false " +
        "multiEmitterFitting=true " +
        "multiEmitterFittingSameIntensity=false " +
        "multiEmitterFittingMaxEmitters=5 " +
        "multiEmitterFittingFixedIntensity=true " +
        "multiEmitterFittingExpectedIntensityMin=500  " +
        "multiEmitterFittingExpectedIntensityMax=2500 " +
        "multiEmitterFittingPvalue=1.0E-6 " +
    
//        "removeDuplicates=0 " +     
        "removeDuplicates=1 " +     
    
//        "superResolutionMagnification=8 " + 
//        "superResolutionMagnification=16 " + 
        "superResolutionMagnification=32 " + 
        "histogramAverages=8 " + 
        
        "filterSigmaMin=30 " +
        "filterSigmaMax=200 " +
    
        "filterIntensity=0 " +

        "driftCorrection=1 " +
        "numberOfBins=8 " +
//        "driftMagnification=4 " +
//        "driftMagnification=8 " +
        "driftMagnification=16 " +
        "crossCorrelationSmoothingFactor=0.5 " +        

        "densityFilter=0 " +
//        "densityFilter=1 " +
        "densityFilterRadius=50 " +           
        "densityFilterNeighbours=5 " +   
    
        "merging=0"
        );
}


runMacro( macroPath + "enhanceSuperRes.ijm",
    "inputDirectory=" + measurementDirectory + "super_resolution_128x128/ " +
    "scaleBarWidth=1 " +
    "calibrationBarZoom=2 " +
    "scaleBarHeight=10 " +
    "scaleBarFontSize=36 " +
    "titleFontSize=36"    
    );

runMacro( macroPath + "enhanceSuperRes.ijm", 
    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/ " +
    "scaleBarWidth=1 " +
    "calibrationBarZoom=2 " +
    "scaleBarHeight=10 " +
    "scaleBarFontSize=36 " +
    "titleFontSize=36"        
    );

runMacro( macroPath + "titleDrift.ijm", 
    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/" );

// ***** Finish *****
                        
print("Finished Measurement: " + measurement.replace("/", ", ") );

// finish time
finishTime(startTime_ms);

