// MH, v1.2, 2021_08_25

print("\\Clear");

//setBatchMode(true);

// ***** Measurement parameters *****

measurement = "jhi213/01_2021_03_26";

roiImageNames = newArray(
    "01_80RG_561nm_100ms",
    "02_80RG_561nm_200ms", 
    "03_80RG_561nm_400ms", 
    "05_80RG_640nm_200ms");
    
compositeImageNames = newArray(
    "01_80RG_561nm_100ms", 
    "02_80RG_561nm_200ms", 
    "03_80RG_561nm_400ms");

// start time
startTime_ms = startTime();

// ***** Main *****

// paths
macroPath = "C:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/tasks/super_resolution/imageJ/";
measurementDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    measurement + File.separator;

print("Processing Measurement: " + measurement.replace("/", ", ") );

//runMacro(macroPath+"preProcess.ijm", "inputDirectory="+measurementDirectory+"converted/");
//runMacro(macroPath+"setPixelSize.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/ " +
//    "pixelSize_nm=100"
//);
//runMacro(macroPath+"crop.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/");
//runMacro(macroPath+"firstFrame.ijm", "inputDirectory="+measurementDirectory+image_stack_128x128/");
runMacro(macroPath+"enhanceFirstFrame.ijm", "inputDirectory="+measurementDirectory+"first_frame/");
//runMacro(macroPath+"thunderStorm.ijm", "inputDirectory="+measurementDirectory+"image_stack_128x128/");

//runMacro(macroPath+"thunderStorm.ijm", 
//    "inputDirectory="+measurementDirectory+"image_stack_128x128/ "+
//    "processIndividualImage=0 " +
//
//    "cameraGain=300 " +
//    "cameraBaseLevelOffset=65 " + 
//    "cameraPhotoelectronsPerAdCount=12.05 " + 
//    "cameraQuantumEfficiency=0.95 " +
//    
//    "fittingRadius=3 " +  
//    "fittingInitialSigma=1.5 " +
//    "fittingMethod=Weighted-Least-squares " +
//
//    "multiEmitterFitting=false " +
//    
////    "multiEmitterFitting=true " +
////    "multiEmitterFittingSameIntensity=false " +
////    "multiEmitterFittingMaxEmitters=5 " +
////    "multiEmitterFittingFixedIntensity=true " +
////    "multiEmitterFittingExpectedIntensityMin=500  " +
////    "multiEmitterFittingExpectedIntensityMax=2500 " +
////    "multiEmitterFittingPvalue=1.0E-6 " +
//
////    "removeDuplicates=1 " +             
//    "removeDuplicates=0 " +     
//        
//    "superResolutionMagnification=8 " + 
//    "histogramAverages=8 " + 
//    
////    "filterSigmaMin=30 " +
//    "filterSigmaMin=0 " +
//    "filterSigmaMax=170 " +
//
//    "filterIntensity=0 " +
//    
//    "driftCorrection=1 " +
//    "numberOfBins=8 " +
//    "driftMagnification=8 " + 
//    "crossCorrelationSmoothingFactor=0.5 " +    
//        
//    "densityFilter=0 " +
//
//    "merging=1"
//    );

//runMacro( macroPath + "enhanceSuperRes.ijm",
//    "inputDirectory=" + measurementDirectory + "super_resolution_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"    
//    );
//
//runMacro( macroPath + "enhanceSuperRes.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"        
//    );
//
//runMacro( macroPath + "titleDrift.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/" );

//runMacro(macroPath+"video.ijm", "inputDirectory="+measurementDirectory+//setBatchMode(true);"image_stack_128x128/" );

// ***** Processing of ROIs *****

//for (i = 0; i < lengthOf(roiImageNames); i++) {
//    runMacro( macroPath + "roi.ijm",  
//        "inputFilePath="+measurementDirectory+"super_resolution_post_128x128/"+roiImageNames[i]+".tiff " +
//        "roiWidth=32 roiHeight=32" 
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames); i++) {
//    runMacro( macroPath + "enhanceRoi.ijm",  
//        "inputDirectory=" + measurementDirectory + "roi/" +  roiImageNames[i] + "/ " + 
//        "scaleBarWidth=80"
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames); i++) {
//    runMacro( macroPath + "superResRoi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames[i] + ".tiff " +
//        "roiWidth=32 " +
//        "roiHeight=32 " +
//        "roiStrokeWidth=4 " +
//        "roiTitleFontSize=36 " +
//        "scaleBarWidth=1 " +
//        "calibrationBarZoom=2 " +
//        "scaleBarHeight=10 " +
//        "scaleBarFontSize=36 " +
//        "titleFontSize=36"  
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames); i++) {
//    runMacro( macroPath + "montageRoi.ijm",  "inputDirectory=" + measurementDirectory + 
//        "roi/" + roiImageNames[i] + "/");    
//}

// ***** Processing of compositions *****

//function processCompositions(compositeImageNames){    
//    
//    argument = Array.copy(compositeImageNames);
//    for (i = 0; i < lengthOf(compositeImageNames); i++) {
//        argument[i] = measurementDirectory + "super_resolution_post_128x128/" + argument[i] + ".tiff";           
//    }
//    argument = String.join(argument, ",");
//    
//    runMacro( macroPath + "composite.ijm", "inputFilePaths=" + argument); 
//    
//    compositeName = String.join(compositeImageNames, "-");
//        
//    runMacro( macroPath + "roi.ijm", "inputFilePath=" + measurementDirectory + "composite/" + 
//        compositeName + ".tiff");
//            
//    runMacro( macroPath + "enhanceRoi.ijm",  "inputDirectory=" + measurementDirectory + "roi/" + 
//        compositeName + "/");
//    
//    runMacro( macroPath + "superResRoi.ijm",  
//        "inputFilePath=" + measurementDirectory + "composite/" + compositeName + ".tiff" + 
//        " showCalibrationBar=0 showTitle=0");
//    
//    runMacro( macroPath + "montageRoi.ijm",  
//        "inputDirectory=" + measurementDirectory + "roi/" + compositeName + "/"); 
//
//}
//
//processCompositions(compositeImageNames);

// ***** Finish *****
                        
print("Finished Measurement: " + measurement.replace("/", ", ") );

// finish time
finishTime(startTime_ms);

