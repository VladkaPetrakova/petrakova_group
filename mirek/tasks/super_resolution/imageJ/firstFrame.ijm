// MH, v1.9, 2021_06_28

print("Run: First frame");

var inputDirectory

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/image_stack_128x128/";

// input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: First frame" );

function processImage(inputFilePath){
    
    checkFileExistence(inputFilePath);
        
    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);     

    print("Processing file: " + fileName);    
                
    open(inputFilePath);                                
        
    // extract the first frame
    run("Make Substack...", "  slices=1");          

    // output directory               
    outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator +
        "first_frame" + File.separator;    
    createNonExistingDirectory(outputDirectory);

    // export          
    saveAs("Tiff", outputDirectory + fileNameWithoutExtension + ".tiff");
    
    close("*");  
              
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];        
            
    }
                   
}



