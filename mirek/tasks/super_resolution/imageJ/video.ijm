// MH, v1.6, 2021_09_15

print("Run: Video");

// global parameters
var inputDirectory, inputFilePath, processIndividualImage
var exposureTimeIndex

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    "jhi213/01_2021_03_26/image_stack_128x128/";    
exposureTimeIndex = 3; // jhi
// exposureTimeIndex = 4; // biocev

// input
setGlobalVariables( getArgument() );

// main

// main
if (processIndividualImage)
    processImage(inputFilePath);
else
    processFolder(inputDirectory);

print( "Finished: Video" );

function processImage(inputFilePath){
    
    checkFileExistence(inputFilePath);
        
    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);     

    print("Processing file: " + fileName);
    print("Path: " + inputFilePath);
    
    // process only tiff files    
    if ( endsWith(fileName, ".tiff") || endsWith(fileName, ".tif") ){
            
        open(inputFilePath);         
                  
        exposureTime_s = getExposureTimeFromFileName(fileNameWithoutExtension, exposureTimeIndex);

//        print(exposureTime_s);
        
        fps = parseFloat(1/exposureTime_s);
        
        getPixelSize(unit, pixelWidth_um, pixelHeight_um);  
        pixelSize_um = pixelWidth_um;
        
        Stack.getDimensions( width, height, channels, slices, frames );        
        
        run("Properties...",
            "channels=" + channels + " slices=" + slices + " frames=" + frames + 
            " pixel_width=" + pixelSize_um + " pixel_height=" + pixelSize_um + " voxel_depth=0" +
            " frame=[" + exposureTime_s + "sec]");
        
        run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");

        // first 100 frames
        firstFrame = 1;
        lastFrame = 100;        
        
        run("Make Substack...", "frames=" + firstFrame + "-" + lastFrame);
         
        outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator + 
            "video_" + firstFrame + "-" + lastFrame + File.separator;
        createNonExistingDirectory(outputDirectory);  
        
        run("AVI... ", "compression=None frame=" + fps + " save=" + outputDirectory + 
            fileNameWithoutExtension + ".avi");
        
        close();           

        // last 100 frames
        firstFrame = 1901;
        lastFrame = 2000;
           
        run("Make Substack...", "frames=" + firstFrame + "-" + lastFrame);
           
        outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator + 
            "video_" + firstFrame + "-" + lastFrame + File.separator;
        createNonExistingDirectory(outputDirectory);  
        
        run("AVI... ", "compression=None frame=" + fps + " save=" + outputDirectory + 
            fileNameWithoutExtension + ".avi");
        
        close("*");  

    }
    
}

function getExposureTimeFromFileName(fileNameWithoutExtension, exposureTimeIndex){

    fileNameArray = split(fileNameWithoutExtension, "_");
    
    exposureTime_ms = fileNameArray[exposureTimeIndex];
    exposureTime_ms = replace(exposureTime_ms, "ms", "");
    exposureTime_ms = parseInt(exposureTime_ms);
    
    exposureTime_s = exposureTime_ms/1000;    

    return exposureTime_s;
    
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");


        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];     
               
        if (arg[0] == "inputFilePath")
            inputFilePath = arg[1];        
            
        if (arg[0] == "processIndividualImage")
            processIndividualImage = parseInt(arg[1]); 
                                   
        if (arg[0] == "exposureTimeIndex")
            exposureTimeIndex = parseInt(arg[1]);                  
                        
    }                   
}



