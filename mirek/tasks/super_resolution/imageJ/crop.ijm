// MH, v1.5, 2021_08_25

print("Run: Crop");

// global parameters
var inputDirectory, size, isBiplane

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/image_stack_512x512/";
size = 128;
isBiplane = false;

// input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: Crop" );

function processImage(inputFilePath){       

    checkFileExistence(inputFilePath);

    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);     

    print("Processing file: " + fileName);
        
    open(inputFilePath);                                
    
    height = getHeight();
    width = getWidth();
          
    // crop
    if (isBiplane)
        makeRectangle(width/4-size/2, height/2-size/2, size, size); // crop left image
    else  
        makeRectangle(width/2-size/2, height/2-size/2, size, size);

    run("Crop");
    
    // output directory    
    outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator + 
        "image_stack_" + size + "x" + size + File.separator;    
    createNonExistingDirectory(outputDirectory);       

    // export  
    saveAs("Tiff", outputDirectory + fileNameWithoutExtension + ".tiff");
    
    close();
    
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];

        if (arg[0] == "size")
            size = parseInt(arg[1]);            

        if (arg[0] == "isBiplane")
            isBiplane = parseInt(arg[1]);         
                        
    }
                   
}

