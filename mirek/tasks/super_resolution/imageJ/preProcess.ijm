// MH, v1.4, 2021_08_23
// Change lut to Fire and export to 512x512 folder.

print("Run: Pre-processing");

var inputDirectory

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/converted/";
     
// input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: Pre-processing" );

function processImage(inputFilePath){       

    checkFileExistence(inputFilePath);
            
    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);

    if ( endsWith(fileName, ".tiff") || endsWith(fileName, ".tif") ){
                
        print("Processing file: " + fileName);
    
        open(inputFilePath);                                   
    
        // lut
        run("Fire");            
                
        Stack.getDimensions(width, height, channels, slices, frames);        
           
        // output directory       
        outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator +
            "image_stack_" + width + "x" + height + File.separator;   
        createNonExistingDirectory(outputDirectory);
    
        // export
        saveAs("Tiff", outputDirectory + fileNameWithoutExtension + ".tiff");
        
        close();

    }
        
}


function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];        
            
    }
                   
}



