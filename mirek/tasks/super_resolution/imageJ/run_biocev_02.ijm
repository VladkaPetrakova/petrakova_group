// MH, v1.2, 2021_09_01

print("\\Clear");

//setBatchMode(true);

// ***** Measurement parameters *****

measurement = "biocev/02_2021_08_04"; 

thunderStormImageNames1 = newArray(
    "05_40RG_561nm_50pct_200ms_300gain_160nm"    
    );

thunderStormImageNames2 = newArray(
    "06_40RG_561nm_50pct_200ms_300gain_160nm_TC"        
    );
    
thunderStormImageNames3 = newArray(
    "07_40RG_561nm_100pct_200ms_300gain_107nm_TC"    
    );

thunderStormImageNames4 = newArray(
    "08_40RG_561nm_100pct_200ms_300gain_43nm_TC",
    "10_40RG_647nm_100pct_200ms_300gain_43nm_QC",
    "11_40RG_647nm_100pct_200ms_300gain_43nm_QC_TZ2",
    "12_40RG_647nm_100pct_200ms_300gain_43nm_QC_TZ2_df",
    "13_40RG_647nm_100pct_200ms_300gain_43nm_QC_TZ2_fo"   
    );    
    
roiImageNames1 = newArray(
    "05_40RG_561nm_50pct_200ms_300gain_160nm",
    "06_40RG_561nm_50pct_200ms_300gain_160nm_TC"
    );


roiImageNames = Array.concat(roiImageNames1);
   
// paths
macroPath = "C:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/tasks/super_resolution/imageJ/";
measurementDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    measurement + File.separator;

// start time
startTime_ms = startTime();

// ***** Main *****

//print("Processing Measurement: " + measurement.replace("/", ", ") );
print("Processing Measurement: " + measurement );

//runMacro(macroPath+"convertBiocev.ijm", "inputDirectory="+measurementDirectory+"raw/" );
//runMacro(macroPath+"preProcess.ijm", "inputDirectory="+measurementDirectory+"converted/" );
//runMacro(macroPath+"crop.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/" );
//runMacro(macroPath+"firstFrame.ijm", "inputDirectory="+measurementDirectory+"image_stack_128x128/" );
//runMacro(macroPath+"enhanceFirstFrame.ijm", "inputDirectory="+measurementDirectory+"first_frame/" +
//    " titleFontSize=34");

//for (i = 0; i < lengthOf(thunderStormImageNames1); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames1[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=300 " +
//        "cameraBaseLevelOffset=100 " + 
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=3 " +          
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=30 " +
//        "filterSigmaMax=200 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=8 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//        
//        "densityFilter=1 " +
//        "densityFilterRadius=20 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames2); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames2[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=300 " +
//        "cameraBaseLevelOffset=100 " + 
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=3 " +          
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=25 " +
//        "filterSigmaMax=200 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=16 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//        
//        "densityFilter=1 " +
//        "densityFilterRadius=20 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames2); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames2[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=300 " +
//        "cameraBaseLevelOffset=100 " + 
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=3 " +          
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=30 " +
//        "filterSigmaMax=150 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=3 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//        
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames4); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames4[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//    
//        "cameraGain=300 " +
//        "cameraBaseLevelOffset=90 " + 
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        
//        "fittingRadius=7 " +  
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=3 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=30 " +
//        "filterSigmaMax=200 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=0 " +
//        
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}
//
//runMacro( macroPath + "enhanceSuperRes.ijm",
//    "inputDirectory=" + measurementDirectory + "super_resolution_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"    
//    );
//
//runMacro( macroPath + "enhanceSuperRes.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"        
//    );
//
//runMacro( macroPath + "titleDrift.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/" );

// ***** Processing of ROIs *****

for (i = 0; i < lengthOf(roiImageNames1); i++) {
    runMacro( macroPath + "roi.ijm",  "inputFilePath=" + measurementDirectory + 
        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " + 
        "roiWidth=64 roiHeight=64" );    
}

for (i = 0; i < lengthOf(roiImageNames); i++) {
    runMacro( macroPath + "enhanceRoi.ijm",  "inputDirectory=" + measurementDirectory + 
        "roi/" +  roiImageNames[i] + "/ " + 
        "scaleBarWidth=40"
        );    
}

for (i = 0; i < lengthOf(roiImageNames1); i++) {
    runMacro( macroPath + "superResRoi.ijm",  "inputFilePath=" + measurementDirectory + 
        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " +
        "roiWidth=64 " +
        "roiHeight=64 " +
        "roiStrokeWidth=2 " +
        "roiTitleFontSize=80 " +
        "scaleBarWidth=1 " +
        "calibrationBarZoom=2 " +
        "scaleBarHeight=10 " +
        "scaleBarFontSize=36 " +
        "titleFontSize=36"  
        );    
}

for (i = 0; i < lengthOf(roiImageNames); i++) {
    runMacro( macroPath + "montageRoi.ijm",  "inputDirectory=" + measurementDirectory + 
        "roi/" + roiImageNames[i] + "/");    
}

// ***** Finish *****
                        
print("Finished Measurement: " + measurement );

// finish time
finishTime(startTime_ms);

