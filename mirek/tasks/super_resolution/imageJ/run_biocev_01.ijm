// MH, v1.3, 2021_09_07

print("\\Clear");

//setBatchMode(true);

// ***** Measurement parameters *****

measurement = "biocev/01_2021_05_26"; 

thunderStormImageNames1 = newArray(
    "01_80RG_561nm_200ms_20pct_160nm",
    "02_80RG_647nm_200ms_20pct_160nm",
    "03_80RG_561nm_200ms_50pct_160nm",
    "04_80RG_647nm_200ms_20pct_160nm",    
    "10_80RG_561nm_200ms_100pct_160nm_TC",
    "11_40R_647nm_100ms_40pct_160nm"
    );

thunderStormImageNames2 = newArray(
    "05_80RG_561nm_200ms_50pct_64nm",
    "06_80RG_647nm_200ms_10pct_64nm"
    );

thunderStormImageNames3 = newArray(
    "07_80RG_561nm_200ms_50pct_64nm",
    "08_80RG_647nm_200ms_20pct_64nm"
    );
    
thunderStormImageNames4 = newArray(
    "09_80RG_561nm_200ms_100pct_64nm_TC"
    );
        
roiImageNames1 = newArray(
    "01_80RG_561nm_200ms_20pct_160nm", 
    "02_80RG_647nm_200ms_20pct_160nm",
    "03_80RG_561nm_200ms_50pct_160nm",
    "04_80RG_647nm_200ms_20pct_160nm"
    );

roiImageNames2 = newArray(
    "07_80RG_561nm_200ms_50pct_64nm",
    "08_80RG_647nm_200ms_20pct_64nm"
    );    

roiImageNames = Array.concat(roiImageNames1, roiImageNames2);

compositeImageNames1 = newArray( 
    "01_80RG_561nm_200ms_20pct_160nm", 
    "02_80RG_647nm_200ms_20pct_160nm" 
    );    
compositeImageNames2 = newArray(     
    "03_80RG_561nm_200ms_50pct_160nm", 
    "04_80RG_647nm_200ms_20pct_160nm"
    );
compositeImageNames3 = newArray(     
    "07_80RG_561nm_200ms_50pct_64nm", 
    "08_80RG_647nm_200ms_20pct_64nm"
    );

// start time
startTime_ms = startTime();
    
// ***** Main *****

// paths
macroPath = "C:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/tasks/super_resolution/imageJ/";
measurementDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
    measurement + File.separator;

print("Processing Measurement: " + measurement.replace("/", ", ") );

//runMacro(macroPath+"preProcess.ijm", "inputDirectory="+measurementDirectory+"converted/");
//runMacro(macroPath+"crop.ijm", "inputDirectory="+measurementDirectory+"image_stack_512x512/");
//runMacro(macroPath+"firstFrame.ijm", "inputDirectory="+measurementDirectory+"image_stack_128x128/");
runMacro(macroPath+"enhanceFirstFrame.ijm", "inputDirectory="+measurementDirectory+"first_frame/ " + 
    "titleFontSize=42");

//for (i = 0; i < lengthOf(thunderStormImageNames1); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames1[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//                    
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        "cameraBaseLevelOffset=100 " + 
//        "cameraGain=300 " +
//        
//        "fittingRadius=3 " +  
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//    
//        "removeDuplicates=1 " +     
//        
//        "filterSigmaMin=30 " +
//        "filterSigmaMax=150 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=8 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames2); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames2[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//                    
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        "cameraBaseLevelOffset=90 " + 
//        "cameraGain=300 " +
//        
//        "fittingRadius=3 " +  
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//            
//        "removeDuplicates=1 " +     
//        
//        "filterSigmaMin=30 " +
//        "filterSigmaMax=200 " +
//    
//        "filterIntensity=0 " +        
//
//        "driftCorrection=1 " +
//        "numberOfBins=2 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=1 " +        
//
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames3); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames3[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//                    
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        "cameraBaseLevelOffset=100 " + 
//        "cameraGain=300 " +
//        
//        "fittingRadius=3 " +  
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//            
//        "removeDuplicates=1 " +     
//        
//        "filterSigmaMin=15 " +
//        "filterSigmaMax=200 " +
//    
//        "filterIntensity=0 " +        
//
//        "driftCorrection=1 " +
//        "numberOfBins=8 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=0.5 " +        
//
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//for (i = 0; i < lengthOf(thunderStormImageNames4); i++) {
//    runMacro(macroPath+"thunderStorm.ijm", 
//        "inputFilePath="+measurementDirectory+"image_stack_128x128/"+thunderStormImageNames4[i]+".tiff " +
//        
//        "processIndividualImage=1 " +
//                    
//        "cameraPhotoelectronsPerAdCount=62.5 " + 
//        "cameraQuantumEfficiency=0.95 " +
//        "cameraBaseLevelOffset=90 " + 
//        "cameraGain=300 " +
//        
//        "fittingRadius=3 " +  
//        "fittingMethod=Weighted-Least-squares " +
//        "fittingInitialSigma=1.5 " +        
//    
//        "multiEmitterFitting=true " +
//        "multiEmitterFittingSameIntensity=false " +
//        "multiEmitterFittingMaxEmitters=5 " +
//        "multiEmitterFittingFixedIntensity=true " +
//        "multiEmitterFittingExpectedIntensityMin=500  " +
//        "multiEmitterFittingExpectedIntensityMax=2500 " +
//        "multiEmitterFittingPvalue=1.0E-6 " +
//    
//        "removeDuplicates=1 " +     
//    
//        "superResolutionMagnification=32 " + 
//        "histogramAverages=8 " + 
//        
//        "filterSigmaMin=25 " +
//        "filterSigmaMax=150 " +
//    
//        "filterIntensity=0 " +
//
//        "driftCorrection=1 " +
//        "numberOfBins=6 " +
//        "driftMagnification=16 " +
//        "crossCorrelationSmoothingFactor=1 " +
//
//        "densityFilter=1 " +
//        "densityFilterRadius=50 " +           
//        "densityFilterNeighbours=5 " +   
//    
//        "merging=0"
//        );
//}

//runMacro( macroPath + "enhanceSuperRes.ijm",
//    "inputDirectory=" + measurementDirectory + "super_resolution_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"    
//    );
//
//runMacro( macroPath + "enhanceSuperRes.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/ " +
//    "scaleBarWidth=1 " +
//    "calibrationBarZoom=2 " +
//    "scaleBarHeight=10 " +
//    "scaleBarFontSize=36 " +
//    "titleFontSize=36"        
//    );
//
//runMacro( macroPath + "titleDrift.ijm", 
//    "inputDirectory=" + measurementDirectory + "super_resolution_post_128x128/" );

// ***** Processing of ROIs *****

//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "roi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " + 
//        "roiWidth=128 roiHeight=128" );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames2); i++) {
//    runMacro( macroPath + "roi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames2[i] + ".tiff " + 
//        "roiWidth=256 roiHeight=256" );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames); i++) {
//    runMacro( macroPath + "enhanceRoi.ijm",  "inputDirectory=" + measurementDirectory + 
//        "roi/" +  roiImageNames[i] + "/ " + 
//        "scaleBarWidth=80"
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames1); i++) {
//    runMacro( macroPath + "superResRoi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames1[i] + ".tiff " +
//        "roiWidth=128 " +
//        "roiHeight=128 " +
//        "roiStrokeWidth=4 " +
//        "roiTitleFontSize=120 " +
//        "scaleBarWidth=1 " +
//        "calibrationBarZoom=2 " +
//        "scaleBarHeight=10 " +
//        "scaleBarFontSize=36 " +
//        "titleFontSize=36"  
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames2); i++) {
//    runMacro( macroPath + "superResRoi.ijm",  "inputFilePath=" + measurementDirectory + 
//        "super_resolution_post_128x128/" + roiImageNames2[i] + ".tiff " +
//        "roiWidth=256 " +
//        "roiHeight=256 " +
//        "roiStrokeWidth=4 " +
//        "roiTitleFontSize=120 " +
//        "scaleBarWidth=1 " +
//        "calibrationBarZoom=2 " +
//        "scaleBarHeight=10 " +
//        "scaleBarFontSize=36 " +
//        "titleFontSize=36"  
//        );    
//}
//
//for (i = 0; i < lengthOf(roiImageNames); i++) {
//    runMacro( macroPath + "montageRoi.ijm",  "inputDirectory=" + measurementDirectory + 
//        "roi/" + roiImageNames[i] + "/");    
//}

// ***** Processing of compositions *****

//function processCompositions(compositeImageNames, roiWidth){    
//
//    // macro argument
//    argument = Array.copy(compositeImageNames);
//    for (i = 0; i < lengthOf(compositeImageNames); i++) {
//        argument[i] = measurementDirectory + "super_resolution_post_128x128/" + argument[i] + ".tiff";           
//    }
//    argument = String.join(argument, ",");
//    
//    runMacro( macroPath + "composite.ijm",
//        "inputFilePaths=" + argument + " " +
//        "legendFontSize=136 " + 
//        "legendBackgroundX=2500 " +
//        "legendBackgroundY=400"
//        ); 
//    
//    compositeName = String.join(compositeImageNames, "-");
//        
//    runMacro( macroPath + "roi.ijm", "inputFilePath=" + measurementDirectory + "composite/" + 
//        compositeName + ".tiff " +
//        "roiWidth=" + roiWidth + " roiHeight=" + roiWidth);
//            
//    runMacro( macroPath + "enhanceRoi.ijm",  "inputDirectory=" + measurementDirectory + "roi/" + 
//        compositeName + "/ " + 
//        "scaleBarWidth=80"
//        );
//    
//    runMacro( macroPath + "superResRoi.ijm",  
//        "inputFilePath=" + measurementDirectory + "composite/" + compositeName + ".tiff " + 
//        "showCalibrationBar=0 " + 
//        "showTitle=0 " +
//        "roiWidth=" + roiWidth + " " +
//        "roiHeight=" + roiWidth + " " +
//        "roiStrokeWidth=4 " +
//        "roiTitleFontSize=120 " +
//        "scaleBarWidth=1 " +
//        "calibrationBarZoom=2 " +
//        "scaleBarHeight=10 " +
//        "scaleBarFontSize=36 " +
//        "titleFontSize=36"  
//        );    
//    
//    runMacro( macroPath + "montageRoi.ijm",  
//        "inputDirectory=" + measurementDirectory + "roi/" + compositeName + "/"); 
//
//}
//
//processCompositions(compositeImageNames1, 128);
//processCompositions(compositeImageNames2, 128);
//processCompositions(compositeImageNames3, 256);

// ***** Finish *****
                        
print("Finished Measurement: " + measurement.replace("/", ", ") );

// finish time
finishTime(startTime_ms);


