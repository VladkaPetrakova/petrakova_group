// MH, v1.3, 2021_08_24

print("Run: Enhance ROI");

// global parameters
var inputDirectory, scaleBarWidth, scaleBarHeight, scaleBarFontSize, titleFontSize

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/roi/01_80RG_561nm_100ms/";

//scaleBarWidth = 80;    
scaleBarHeight = 16;
scaleBarFontSize = 80;
titleFontSize = 100;

// input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: Enhance ROI" );

function processImage(inputFilePath){    

    checkFileExistence(inputFilePath);

    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);                
      
    // process only tiff files    
    if ( endsWith(fileName, ".tiff") || endsWith(fileName, ".tif") ){

        print("Processing file: " + fileName);    
        
        open(inputFilePath);                                            

        // scale to 1024x1024
        run("Size...", "width=1024 height=1024 depth=1 constrain interpolation=None");       
 
        // convert µm to nm
        getPixelSize( unit, pixelWidth_um, pixelHeight_um );        

        Stack.setXUnit("nm");
        
        run("Properties...", "channels=1 slices=1 frames=1 pixel_width=" + 
            pixelWidth_um * 1000 + " pixel_height=" + pixelHeight_um * 1000 + " voxel_depth=1");

        // scale bar
        run("Scale Bar...", "width=" + scaleBarWidth + " height=" + scaleBarHeight + " font=" + 
            scaleBarFontSize + " color=White background=None location=[Lower Left] bold overlay");
        
        // insert title text                    
        title = fileNameWithoutExtension;
        setFont("SansSerif", titleFontSize, "bold antialiased");
        setColor("white");
        setJustification("center");
        Overlay.drawString(title, getWidth/2, titleFontSize, 0.0);           
        Overlay.show();        
        
        // output directory
        outputDirectory = File.getParent(inputFilePath) + File.separator;
        outputFilePath = outputDirectory + fileNameWithoutExtension + ".png";

        // export           
        saveAs("PNG", outputFilePath);       

        close();

    }

}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];
            
        if (arg[0] == "scaleBarWidth")
            scaleBarWidth = arg[1];      
                  
        if (arg[0] == "scaleBarHeight")
            scaleBarHeight = parseInt(arg[1]); 
                             
        if (arg[0] == "scaleBarFontSize")
            scaleBarFontSize = parseInt(arg[1]);    
            
        if (arg[0] == "titleFontSize")
            titleFontSize = parseInt(arg[1]);                                          
                        
    }                   
    
}


  
