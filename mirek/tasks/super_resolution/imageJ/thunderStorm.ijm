// MH, v1.9, 2021_08_25

print("Run: ThunderSTORM");

// global parameters
var inputDirectory, inputFilePath, processIndividualImage
var cameraGain, cameraBaseLevelOffset, cameraPhotoelectronsPerAdCount,
    cameraQuantumEfficiency
var fittingRadius, fittingMethod, fittingInitialSigma
var multiEmitterFitting, multiEmitterFittingSameIntensity, multiEmitterFittingMaxEmitters, 
    multiEmitterFittingFixedIntensity, multiEmitterFittingExpectedIntensityMin, 
    multiEmitterFittingExpectedIntensityMax, multiEmitterFittingPvalue
var removeDuplicates
var superResolutionMagnification, histogramAverages
var filterSigmaMin, filterSigmaMax
var filterIntensity, filterIntensityMax
var densityFilter, densityFilterNeighbours, densityFilterRadius
var driftCorrection, crossCorrelationSmoothingFactor, numberOfBins, driftMagnification
var merging 
var renderGaussianSigma

// default values
//processIndividualImage = false;
//processIndividualImage = true;

// inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "jhi213/01_2021_03_26/image_stack_128x128/";
//inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "biocev/01_2021_05_26/image_stack_128x128/";
//inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
//     "biocev/02_2021_08_04/image_stack_128x128/";

//inputFilePath = inputDirectory + "01_80RG_561nm_100ms.tiff";
//inputFilePath = inputDirectory + "02_80RG_561nm_200ms.tiff";

//inputFilePath = inputDirectory + "01_80RG_561nm_200ms_20pct_160nm.tiff";
//inputFilePath = inputDirectory + "07_80RG_561nm_200ms_50pct_64nm.tiff";
//inputFilePath = inputDirectory + "08_80RG_647nm_200ms_20pct_64nm.tiff";
//inputFilePath = inputDirectory + "09_80RG_561nm_200ms_100pct_64nm_TC.tiff";
//inputFilePath = inputDirectory + "11_40R_647nm_100ms_40pct_160nm.tiff";

//inputFilePath = inputDirectory + "05_40RG_561nm_50pct_200ms_160nm.tiff";
//inputFilePath = inputDirectory + "06_40RG_561nm_50pct_200ms_160nm_TC.tiff";
//inputFilePath = inputDirectory + "07_40RG_561nm_100pct_200ms_107nm_TC.tiff";
//inputFilePath = inputDirectory + "08_40RG_561nm_100pct_200ms_43nm_TC.tiff";
//inputFilePath = inputDirectory + "10_40RG_647nm_100pct_200ms_43nm_QC.tiff";

//cameraGain = 300;
//cameraBaseLevelOffset = 65.0; // jhi
//cameraBaseLevelOffset = 80; // biocev
//cameraBaseLevelOffset = 100; // biocev
//cameraPhotoelectronsPerAdCount = 12.05; // jhi
//cameraPhotoelectronsPerAdCount = 62.5; // biocev
//cameraQuantumEfficiency = 0.95;

//fittingInitialSigma = 1.5;        
//fittingRadius = 3;        
//fittingMethod = "Weighted Least squares";

//multiEmitterFitting = "true";
//multiEmitterFittingSameIntensity = "false";
//multiEmitterFittingMaxEmitters = 5;
//multiEmitterFittingFixedIntensity = "true";
//multiEmitterFittingExpectedIntensityMin = 500; 
//multiEmitterFittingExpectedIntensityMax = 2500;
//multiEmitterFittingPvalue = 1.0E-6;

//removeDuplicates = true;
//superResolutionMagnification = 8; // jhi
//superResolutionMagnification = 32; // biocev      
//histogramAverages = 8;

//filterSigmaMin = 0;
//filterSigmaMin = 30;
//filterSigmaMax = 150;
//filterSigmaMax = 170;
//filterSigmaMax = 250;

//filterIntensity = false;
//filterIntensityMax = 2500;

//densityFilter = true;
//densityFilterNeighbours=5;
//densityFilterRadius=20;

//driftCorrection = false;
//numberOfBins = 8;
//driftMagnification = 16;
//crossCorrelationSmoothingFactor = 0.25;
//crossCorrelationSmoothingFactor = 0.5;
//crossCorrelationSmoothingFactor = 0.75;

//merging = false;

//renderGaussianSigma = 10;

// input
setGlobalVariables( getArgument() );
       
// main
if (processIndividualImage)
    processImage(inputFilePath);
else
    processFolder(inputDirectory);

print( "Finished: ThunderSTORM" );

function processImage(inputFilePath){
    
    checkFileExistence(inputFilePath);

    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);

    print("Processing file: " + fileName);    
    
    // process only tiff files    
    if ( endsWith(fileName, ".tiff") || endsWith(fileName, ".tif") ){
    
        open(inputFilePath);                                                        
                
        // pixel size
        getPixelSize(unit, pixelWidth_um, pixelHeight_um);
        pixelSize_nm = pixelWidth_um * 1000;
        
        // set camera parameters
        run("Camera setup", 
            "offset=" + cameraBaseLevelOffset + " quantumefficiency=" + cameraQuantumEfficiency + 
            " isemgain=true photons2adu=" + cameraPhotoelectronsPerAdCount + 
            " gainem=" + cameraGain + " pixelsize=" + pixelSize_nm);    

        thunderStormArgument = "filter=[Wavelet filter (B-Spline)] scale=2.0 order=3 detector=[Local " + 
            "maximum] connectivity=8-neighbourhood threshold=std(Wave.F1) estimator=[PSF: " + 
            "Integrated Gaussian] sigma=" + fittingInitialSigma + " fitradius=" + fittingRadius + 
            " method=[" + replace(fittingMethod,"-"," ") + "] full_image_fitting=false " +
            "mfaenabled=" + multiEmitterFitting + " keep_same_intensity=" + multiEmitterFittingSameIntensity + 
            " nmax=" + multiEmitterFittingMaxEmitters + " fixed_intensity=" + multiEmitterFittingFixedIntensity + 
            " expected_intensity=" + multiEmitterFittingExpectedIntensityMin + ":" + 
            multiEmitterFittingExpectedIntensityMax + " pvalue=" + multiEmitterFittingPvalue  +
            " renderer=[Histograms] magnification=" + superResolutionMagnification + 
            " colorize=false threed=false avg=" + histogramAverages + " repaint=50";

//        print(thunderStormArgument);
       
        // thunderStorm analysis       
        run("Run analysis", thunderStormArgument);

        // loc before post-processing   
        run("Show results table");

        // output directory
        outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator + 
            "super_resolution_128x128" + File.separator;
        createNonExistingDirectory(outputDirectory);

        // export loc before post-processing   
        run("Export results", "floatprecision=5 filepath=" + 
            outputDirectory + fileNameWithoutExtension + ".csv" + 
            " fileformat=[CSV (comma separated)] " + 
            "sigma=true intensity=true offset=true saveprotocol=true x=true y=true " + 
            "bkgstd=true id=true uncertainty_xy=true frame=true");
                
        // super-resolution image before post-processing   
        run("Visualization", "imleft=0.0 imtop=0.0 imwidth=128.0 imheight=128.0 " +
             "renderer=[Histograms] magnification=" + superResolutionMagnification +
             " avg=" + histogramAverages + " colorize=false threed=false");      

        // change LUT
        run("Fire");
            
        // export super-resolution image before post-processing                 
        saveAs("Tiff", outputDirectory + fileNameWithoutExtension +".tiff");       

        // output directory post
        outputDirectoryPost = File.getParent(File.getParent(inputFilePath)) + File.separator + 
            "super_resolution_post_128x128" + File.separator;                 
        createNonExistingDirectory(outputDirectoryPost);
        
        // post-processing     

        // remove duplicates
        if (removeDuplicates)
            run("Show results table", "action=duplicates distformula=uncertainty_xy");
                    
        // sigma and intensity filter
        if (filterIntensity)
            run("Show results table", "action=filter formula=[(sigma > " + filterSigmaMin +
                ") & (sigma < " + filterSigmaMax + ") & (intensity < " + filterIntensityMax + ")]");
        else
            run("Show results table", "action=filter formula=[(sigma > " + filterSigmaMin +
                ") & (sigma < " + filterSigmaMax + ")]");        
                
        // drift    
        if (driftCorrection){
            run("Show results table", "action=drift path="+outputDirectoryPost+ fileNameWithoutExtension + 
                ".json magnification=" + driftMagnification + 
                " method=[Cross correlation] ccsmoothingbandwidth=" + 
                crossCorrelationSmoothingFactor + " save=true steps=" + numberOfBins + 
                " showcorrelations=false");

            selectWindow("Drift");
            saveAs("PNG", outputDirectoryPost + "drift_" + fileNameWithoutExtension + ".png");
            close();                
        }
                
        // density filter  
        if (densityFilter)     
            run("Show results table", "action=density neighbors=" + densityFilterNeighbours + 
                " radius=" + densityFilterRadius + " dimensions=2D");
                
        // merging
        if (merging)
            run("Show results table",
                "action=merge zcoordweight=0.1 offframes=1 dist=20.0 framespermolecule=0");

        // export loc after post-processing   
        run("Export results", "floatprecision=5 filepath=" + 
            outputDirectoryPost + fileNameWithoutExtension + ".csv" + 
            " fileformat=[CSV (comma separated)] " + 
            "sigma=true intensity=true offset=true saveprotocol=true x=true y=true " + 
            "bkgstd=true id=true uncertainty_xy=true frame=true");
   
        // super-resolution image
        run("Visualization", "imleft=0.0 imtop=0.0 imwidth=128.0 imheight=128.0 " +
             "renderer=[Normalized Gaussian] dxforce=true magnification=" + superResolutionMagnification +
             " colorize=false dx=" + renderGaussianSigma + " threed=false dzforce=false");     

        // lut
        run("Fire");
        
        // export super-resolution image after post-processing              
        saveAs("Tiff", outputDirectoryPost + fileNameWithoutExtension +".tiff");        

        // close all image windows
        close("*");        

    }   
    
}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
            
    argumentArray = split(inputArguments, " ");
    
    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
        
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];     
               
        if (arg[0] == "inputFilePath")
            inputFilePath = arg[1];        
            
        if (arg[0] == "processIndividualImage")
            processIndividualImage = parseInt(arg[1]);        
                                   
        if (arg[0] == "cameraGain")
            cameraGain = parseFloat(arg[1]);
                        
        if (arg[0] == "cameraBaseLevelOffset")
            cameraBaseLevelOffset = parseFloat(arg[1]);
            
        if (arg[0] == "cameraPhotoelectronsPerAdCount")
            cameraPhotoelectronsPerAdCount = parseFloat(arg[1]);

        if (arg[0] == "cameraQuantumEfficiency")
            cameraQuantumEfficiency = parseFloat(arg[1]);

        if (arg[0] == "fittingInitialSigma")
            fittingInitialSigma = parseFloat(arg[1]);

        if (arg[0] == "fittingRadius")
          fittingRadius = parseFloat(arg[1]);

        if (arg[0] == "fittingMethod")
           fittingMethod = arg[1];

        if (arg[0] == "multiEmitterFitting")
           multiEmitterFitting = arg[1];

        if (arg[0] == "multiEmitterFittingSameIntensity")
           multiEmitterFittingSameIntensity = arg[1];

        if (arg[0] == "multiEmitterFittingMaxEmitters")
           multiEmitterFittingMaxEmitters = parseInt(arg[1]);

        if (arg[0] == "multiEmitterFittingFixedIntensity")
           multiEmitterFittingFixedIntensity = arg[1];

        if (arg[0] == "multiEmitterFittingExpectedIntensityMin")
           multiEmitterFittingExpectedIntensityMin = parseInt(arg[1]);

        if (arg[0] == "multiEmitterFittingExpectedIntensityMax")
           multiEmitterFittingExpectedIntensityMax = parseInt(arg[1]);

        if (arg[0] == "multiEmitterFittingPvalue")
           multiEmitterFittingPvalue = parseFloat(arg[1]);

        if (arg[0] == "removeDuplicates")
           removeDuplicates = parseInt(arg[1]);
            
        if (arg[0] == "superResolutionMagnification")
            superResolutionMagnification = parseFloat(arg[1]);

        if (arg[0] == "histogramAverages")
            histogramAverages = parseFloat(arg[1]);
            
        if (arg[0] == "filterSigmaMax")
            filterSigmaMax = parseFloat(arg[1]);

        if (arg[0] == "filterSigmaMin")
            filterSigmaMin = parseFloat(arg[1]);

        if (arg[0] == "numberOfBins")
            numberOfBins = parseFloat(arg[1]);

        if (arg[0] == "crossCorrelationSmoothingFactor")
           crossCorrelationSmoothingFactor  = parseFloat(arg[1]);                                                                             

        if (arg[0] == "driftMagnification")
           driftMagnification  = parseFloat(arg[1]);                                                                             

        if (arg[0] == "densityFilter")
           densityFilter = parseInt(arg[1]);                                                                             

        if (arg[0] == "densityFilterNeighbours")
           densityFilterNeighbours  = parseFloat(arg[1]);                                                                             

        if (arg[0] == "densityFilterRadius")
           densityFilterRadius  = parseFloat(arg[1]);                                                                             

        if (arg[0] == "filterIntensityMax")
           filterIntensityMax  = parseFloat(arg[1]);                                                                             
 
        if (arg[0] == "driftCorrection")
           driftCorrection  = parseInt(arg[1]);                                                                             

        if (arg[0] == "merging")
            merging  = parseInt(arg[1]);                                                                                                                                                          

        if (arg[0] == "filterIntensity")
            filterIntensity  = parseInt(arg[1]);  

        if (arg[0] == "renderGaussianSigma")
            renderGaussianSigma  = parseFloat(arg[1]);                                                                                   

    } 
}


