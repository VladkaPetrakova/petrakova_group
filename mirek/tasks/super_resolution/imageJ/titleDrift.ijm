// MH, v1.0, 2021_08_16

print("Run: Title drift");

var inputDirectory, titleFont

// default values
inputDirectory = "c:/Users/miros/home/work/jhi/projects/plasmonic_microscopy/data/measurements/" +
     "jhi213/01_2021_03_26/super_resolution_post_128x128/";
titleFont = 16;
     
// input
setGlobalVariables( getArgument() );

// main
processFolder(inputDirectory);

print( "Finished: Title drift" );

function processImage(inputFilePath){
    
    checkFileExistence(inputFilePath);

    fileName = File.getName(inputFilePath);
    fileNameWithoutExtension = File.getNameWithoutExtension(inputFilePath);  
            
    // process only tiff files    
    if ( endsWith(fileName, ".png") && startsWith(fileName, "drift_") ){

        print("Processing file: " + fileName);    
        
        open(inputFilePath);                                           

        // title text        
        title = replace(fileNameWithoutExtension, "drift_", "");        
        title = replace(title, "_", ", ");                        
        
        // insert title text
        setFont("SansSerif", titleFont, "bold antialiased");
        setColor("black");
        setJustification("center");
        Overlay.drawString(title, getWidth/2 + 20, 3*titleFont, 0.0);        
        Overlay.show();              
     
        // output directory

        outputDirectory = File.getParent(File.getParent(inputFilePath)) + File.separator +
            "drift" + File.separator;               
        createNonExistingDirectory(outputDirectory);               

        // export  
        saveAs("PNG", outputDirectory + fileNameWithoutExtension + ".png");
        
        close();
        
    }

}

function setGlobalVariables(inputArguments){

    if (lengthOf(inputArguments) == 0)
        return;
    
    argumentArray = split(inputArguments, " ");    

    for (i = 0; i < lengthOf(argumentArray); i++) {
    
        arg = split(argumentArray[i], "=");
                       
        if (arg[0] == "inputDirectory")
            inputDirectory = arg[1];        

        if (arg[0] == "titleFont")
            titleFont = arg[1];                    
    }                   
}



    


