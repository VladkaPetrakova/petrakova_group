(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     26519,        626]
NotebookOptionsPosition[     24989,        593]
NotebookOutlinePosition[     25384,        609]
CellTagsIndexPosition[     25341,        606]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"f", "[", "t_", "]"}], ":=", 
  RowBox[{"Piecewise", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"Sqrt", "[", 
        RowBox[{"1", "-", 
         RowBox[{"t", "^", "2"}]}], "]"}], ",", 
       RowBox[{
        RowBox[{"-", "1"}], "<", "t", "\[LessEqual]", "1"}]}], "}"}], "}"}], 
    ",", "0"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.828800757866289*^9, 3.82880080164999*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"efaa25e1-6b9d-4c28-aa91-1f2913a2b8cb"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"f", "[", "t", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", 
     RowBox[{"-", "2"}], ",", "2"}], "}"}], ",", 
   RowBox[{"Exclusions", "\[Rule]", "None"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.8288008042749968`*^9, 3.82880082085968*^9}, {
  3.828800862183069*^9, 3.828800865992125*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"2727e802-77d3-4a38-9445-ad5e7285368e"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJxN2Xk0VdH3AHChDElS0kSohBASKbUbiMg8loiKylCSqYyJUkooRDLLPEbI
cGQm8zx75of33iUyht/9rvX9ed/3z1ufde+699xz99ln7/d47zzWNKWloaFp
20JD859vUdg7tbGxgWj++3m8fMR7eYXqjO/i/HN/qZYQULkzPk219C7PnrpO
qi+OzNaGpFOt+bo++dRtqh0aPa3uF69vuq1t+9Qvk7VNc5U83iN96N+mcyzK
t+7sX9m08TvxmW6P5U0vBTh5I1jadAzrwP7PQwubDlMSt2l983fTHpYmjoLX
5jftRJfkYEP+s2kT98ajHQmzm9btjrjQH0O1koS1/mg41ZKjbO/+fKKaUVFz
jvUF1Wk7238p3KB6NbzHKJ+J6pnFJMcSeqpH1Z0CqjZmNt1Ae6iy/S/V0WaG
In+GqFYUJawK/aQ6sGjsc5g51UaOoa7SplQLnFK713Kb6p8JeScZdage+OhT
+RSojle9mMJ6lmprpr/+iZJU07oZGQ4KUn3cXHxeaQ/VM0fHu8dYqc4fDEXu
TFR7hKrFHaSnOjiJca/VGoYeHOzTaUsKASEem6HoPxiyTrZ5kXMuFQoDe1O6
JjB0wLXefDD2O6hul3dk7cfQ85TppB/huUBwT7si14IhwWgdo6t0BWCzwLnz
eRWGJo88VtHdWgz0li960gsx9KOXlshsXAKCetpPDsbjx6M0CMINpVBYVySr
EYah115DEcrEMlC9fJzxtT+GCrKXemCuHGxEVsL/OGFIuqPdv2OkEuhj7poL
PMFQGKPe/Mu6KgjaV3/ayAxD9Xt3n9JJqYYCusi6GnUMNSmoyA1r1wJ9j/xK
5DEMuZpVhj0yqIcgtfSKjgMY2uO0xUuZrQEEKvb5s7BhaNdwm3N5SQOoZEwf
d1yhoKxiyvV25iYIeuWvo9ZIQSay19+1KTWDwL8VHq9yCjrvkLtGH9wMP5/c
I/3Mp6DaxNq2jqFmGLwl/ZI/loJ6fc4wRz1tAYFT/RlrjhRkNR/ieutNK/wc
FGBJ4aOgbbKvny69agdn2S2pofso6AffDtZtqB3Oh3SrvGGlIHGbsWM2C+1Q
ovn2g9kKGU2yZLs43u2Aqoppdt4WMjLkdLbLluqEtuS0/UEvyIicHa/AXNEF
FAfJ4+4EEkJL549lfOyFjDaW6kcdJDTjJqGUXdQLT8THHhjWkRBP4GiM+UQv
zE8FJp3NI6GJtLe6czJ9sGq4KPL3AwltuNKKf+7tA0a5n5LmQEL63OsZouwD
cITt4mXt8GmUtWPQ+Z0gAXzFhp6bf5xGCx78IkdlCbCk7vHd3XsaTfLtN4xQ
JUCdf/nRVJtpVBOVWHDpKQFsd19j2KYwjTasaB0uFhCgcq9mXS42hcb+7n73
+OoQmHPd0z14aQp9Z0q74aM8DG3nt34Ql5pC1lOnooQMhuGC0bcqhRNTaF9c
r9gP82HYHUE8Y8sxhbodiikBb4ahkMfqYP3kJCJpGB9+UDkMrEftCa4Bk+hs
j7BXguwIZAl5mw+PEJHg89U7QYdG4XCzcf6JbiLKbdT0bhYchXcOMox2DURU
Bzx661KjYFo+FceQT0Sc4/7GIhqjsM9IZUjYl4gmw9j2j3qOgos/u77DGSLa
Guv0tXZyFBSXwuRZ3k+gbVa0KdxJYzBYkckjJTWOBGl7zh5kmgBnrWW5M8Lj
KKGe3zqffQL2DV18eJZvHF29OaKpdmgC1P81Zl5gHUdvJkjX7pycgBIJymWF
8TE0l2u9n19nAiIjhEz1g8ZQiaBMlUjEBNx5FpP4fGEUGYbUbnERIcKYcKAE
+jGC3lhb3+iSnAQh1zgKMXkE7e0v1FM9PwmPGnOS2KNGkHqcXG+h/CQsPeng
M/MZQVuq+j+81J0E5lzO3TuMR5AFwxvnPIdJELsUOneTaQQtCBaHVuZNgpNO
RPaCwTDCzopFRp+egl2uiVKiW4ZQldGAetzBaXj1Q6CabZGAqtp16Vd5p2GF
En9jjkRA9/T3PFMWmIbh29+c87oI6AzTYnaf5DRkXYopvZxBQDZKR3pLVKZB
c+tXVb3bBNTUxzEX4DoNx/fz1tM4DSIOrZZUu75pEJbChOPm+tAqLWobeEMC
M1E3t/z+PjQ9PGdu8YEEEfw7W+qr+lCokmn+n08kYOc86bDwpQ/5Z0n7zEaQ
YHHx0S8FuT5kwseil5FDgtJ8is7Up140fPgy32kCCfTPU1xPSvUgsYqd3I/F
yRBw2rX5Ck8PsmocvqYvRYbfIqxH9Zl7UHWP3SvZc2S4wC1a4z7Qjdp7i88t
yJHhyIbV7pZX3ehqtVCvqD4ZKCXkeNvOLnS/Mo0214UML+XITfmOnWja7GOb
RAUZShVJapJ3O9GY2Za3j2rIQKMy3ZCm0onmpYdK4+rJ4KwzWRfD14kcf3yo
3NZBBnuzser3dR2ol1lU6eM4GR5495eY8HQg3u7ivs5tFLheV5/BVN2GBnNe
PzwoR4G3TXUnPbPa0Dn/PLrfChSobvudth7WhmQZv+rbK1NAvr8mZe5JGzrh
mXWmRJMCFygVCf0H25DuZP8/HhMKiLEVR2Y+bkWWj11GOF0owKGd5neDswX9
7v3NNZZJgSx+n5/ydC0o4veJlK5sCqgv3x8Vx5qRdDBnUnUufv9w3jPMVc1I
uSF+6WsRBdaJnwZ/2jcjrvsj/7hqKDDu4iR6qKMJhTipu9oOUuCRmv/nIsFG
5Offs/iCCYOEeSVS4JZGdHa/hPZWFgyGQ+gvPupuQIpvq857sWKgO+owwf2m
AYVwbBQ8340BPDOScifWo0z3ICdZLgzYYk60XY6vQ+yWXj8PimGQtVCxs/po
LfrKIvZGVQsD36To0gvLNSjPQ3DaXwcDCyM3u5z6GmQrUmDQrIfB0cozvdH2
NWikm8lS7hYGQYHJ31yqq1H0lXjj1XsYPJf0P3/Ksgr9/D2immqHj4doNZN4
sQopPTf/l+mAwakwpRgejipkZqtWkfUMAxIdPRNrcSWqCDiRkuCCgVGrfRtx
ZyXSevpp2NQLg8s2tyzCv5ejCj8+P/5PGHDzy3BzeJejj8oS18iBGKx2czT7
3CpH3IZlAhnBGGRfapR6trUcde1fNRT6ggH/rsu02nplKDreNGsmCgPmjOMh
TKu/0JU0RZ+KNAziesfeT3oWI2svZ+xjBQY9znsjvp4qRkJjp+NrKzFg5VbI
0BguQlny53evV2Fgb5zQkg9FqOHpdyaDWgyujplzvl0pQLa7Hj8nN2BApGCR
Qtb5qE5CON+2E4NDfjxZA9z56EIsEOy7MFAX1ygLqM9DD1MP/LLrxiDvadbY
ilAeciAYZz7sxeDNkq3Q77EfKDfbU1toEAMh2pXvFgbZSFvecZR1HJ+fGKGK
w8zZ6Pm2oNo+3AFyBh2ted+RkirxUfwEBiuvCpfO7f2OuGJ55yQmMfi93e08
S3Mm8sqxnuEjYUCTmq5a4paJavnKGjpwS6oSbtuKZqK727itXpMxCPO75NHn
k4EaRsumBigYdAkx+t54n4awJ/aY2SwGF60eZvyaSkVzB3620vzB4zG9tkVQ
MRUdeRryLBj3M8n3nCu0Kaguf4i5YA6DAxfYo0IdE9HaPSPF3r8YeLg/LaPt
SECR6tcvGCxgMF3aNmZ+KgGxMwouduIuVAgWOkf5hjxc5LOqFjEw1DiU3Xs3
FrlbzGq4L+PxMvdXxq48Bp0o5jYfxe0c1IhYj8UgTtoXZvIr+Ph7PX5fGo9C
HEHaLOu4hV1vafRejULnC1ZzdFYxKOCR6rSNj0QHnK/IJeHO/t1SodYcjqId
9+5U+odBG9urZo+Ar8jhcpNyIO45HZn+HK0w9KX8tVU/bglCxPzBjhAUt+uK
8b01DDSOadGoBX9GzAKPxGNwW5tvY/HQD0bL78cmBnCnz1seIfZ8QhsTa/Qq
6xg0yPCcPBj2EbkTiy3ccFNcW8+qGgag9xoseWm4d5S/uvqC2x/dG5+b7MGt
z24b+wB9QC0eHlvoNzAIDuspaLj0HikcLF8XxN3Bf6lVsvwtmiwsG7qOe09m
/FToVW8U/t4nyRK31jlW2i01Xoin7uiNN7gDKmz331d+iZibgsnRuJvUesXq
693RCBPpfj5u7uyXfmaRzug4nUB1He4v1epIIs4BaRlpsPbj3tfPRVlPtEGU
DsuzU7hPjLTLBBlaoA+XXK/P4+ZnsQ4WvW+CBn285VZxu7cJtB0sU0Exhe/5
NnAL7+w33yoiCYVzvuP/ccesRwTbkj4ImL/zW8M9o3b8z2MbM6Cz8eJZwu30
8WqthaQ1eNs7BmG4112ffYk4aQfb/935M/Kf65unWLYKPYfliEuS7bjpdAfP
M/C7QdM+tltluMt43rz+yuYBB6RqHqbh9gzis5b29YQHBDPDINxXWQr1m7e/
hqv7h6WccDN46FyyePMGRFclFm7hrl6kCG5leAde+epfzuF+a+XNHuHpC68j
xfk5cX+b9XM59MIPznH8DsLw93X/vpDZj/UAsM1dEgjGrcjncPPl+U9gJfRU
yQy3QH+ZqrpzIAzm2GhI4J7SNJSeWg4Gt7Synb/weKndkXQi90wIRB2dr3yJ
O7l64bCnQygslN+/ewW35QU/Ru6/YeATUaecj8ejynLfv6lT4XApJjD4MW7R
bMHZXJsI6LirUc2He+nTEElgKRJYv1/tcsPjPZ/Zqb3ULga4a5zT9+DrI1KE
ozWdLxYGvRPCE/H19Eo9vSmsMRa+fREZO4tbO2jkt73gNyA8SHLQXMLfD+/1
UqH+BAjwWkjVwddrp9w44nybCGHBLlr1+Pouvu9eRC+dBJcfMKtdwu2Tmp03
4JcMV248VeCex+NDhis9QC4NBkLJ0il4PmG5lZviNpsGXxL2em/DPeeqkWQZ
ng7rDyOFDWcwMB4u1TzinAECItNPNvB8JCqwqLLWngnliYnjnHj+qvl+Wy7z
TTaYhJMufB7DQLZCuOw4IRu0mLAfuaMYZHQsXwqXyoH6TDa31hF8v1r+CD4j
OSBS8nR9YwiDe1B91vR8LuSeKsoQHsBgo1ZMfP9sPvALCgS3tWNg07eW/kHh
J5xh4Z7Ma8NgnFwrui38JyzbRnmFtGJQx2YqPKdUACa+RWFqzRiE6oYcr48t
BP/JRekPdRicHt7C/UIf4c9/p8ygFB//T71ZsUQEG989gw/+wucjILWcsIwg
5nmLUBfCYHtUyulIlRIgO5u8VyjC4J+6ft8GpQSurAtKb+Ri0J+RLojES8FW
urykOQXfr8Ju7ua8WQpT8wm6hsn4evLeuvbIoxQqebbEjSXi+cbYoIm7tRTu
REs/JH3D3w8bg4OrbRmc0MryaInEIMLaqPx8bjmIJ/W+p8H317BbTGmBg+Uw
cbzuiV4ABp8Vs4PJDBWgnGi4LdkPAz8eZouv+hXgq6sAiu/x9deUs2ttuQJu
tosM3XyFgYnYjtuFslXA903yIQHf74/MFCyfLa2Bbt9l8wf6eL6uv+I5T66B
uFPI67guPt6k36xp+2sh/eKnb8N4/fHWtPcI75NaqGj6u3BdDY+3nhUVBp7f
0EY3nz8rj4+/Qia61aUOyuYH155LYHDzS66SpUwjOENRuRcjns8dof2YaSOc
T3jeXLYV3791q24P+jWCaOyOijVaDH6xddpqEhsheUT5lukaBfZccVILU26C
TPe+E3R/KOCUF/rMY6UJ9JV4VB17KHAtprteVb8FNk7z8l9MoMCMV5pZhVUL
cD9YPMwYR4HPD15unHvZAumx+TR1URQgioiIC6a1QC1LwxOFLxTwznP/REff
CgpmKmQWX7zerD9ukJfeCursqweabSigsGRH5GVoh/HP4SaSMhSg9Ci9+Hyo
Hb51WXjbn6ZAUNHhAzsl2iF4P8eDbHG8PvSoUf53qx1eP/6kfESIAq92cKV3
ZLXDaR2CXf0BClTyldv5GHXA3lOJIxqrZJBXYaf7m9MJiaZKs6z5ZPA1YZpd
rumEuP31k6w5ZOiyoxlc7+8E02+uS8yZZDAPp/xk3NYFA9K7NBcT8POxWptD
ul3gGk6ojvyMn+//cvjKQhc0+jzUPu2An98x/ytAqgfe80606+P9wYfbne5i
uX0QEv6ScjiEBK9IsjBe1wdEWvPTSR9J4Poseu3LcB+M6OWqn/QlwaOPVs8Z
WPuBWzF4SfAlCVSr6GwH7vWDp3VxHsGSBKwnxe6/w/vsxtHeKxpAAt91bxWi
1SCYWtgmHSdMQ5Uq1w519iEopav8MMA2DXtEh/baHBqCD3Y/ix2Yp8F4RxzP
J/4hkG64F7adHu+/fgtLdp0dgpb7cdpHFqdA+JqsgfHdIfh7auA6e/8U+F0x
SLTOHoJtrNfHpuKn4OaZEHl/3WEgp4/bP5SZAjIvh2tL6AhY+C89D1KahDNO
UU9+xY7AjEadEOHyJHi0iZhmpI0AOy9N0JFzk7D39dXrvqUj4BJ173XwiUm4
QHI4oDQ1AtIZ78RPsUzChx89P37JjMIMaY+uZB0RxJUjsYyuUdgwWygUUyCC
c6zwSMTwKEg2F2TEXSBC5Vpehy9pFG7tcXi/W4oINzOaiyxpxoBlZ+BI91Ei
eHDQvRMQGIM5rd0n2GiJ0DpoKhDpMAbvHlqLHyqYADvbEyYf9o7D6T9sdvbH
J2AvM1aoyzsOx8OO6AlyTUBeRNY+buFxcNjVFdSC9+P/amWaUi6NQ0gR1yrD
+jh48CpCreU4mBxbHP7XOg6+Dfe4tpaNQz6XDDud6zh8Ewjvfv54AqR+hTzi
rB0DxWKT05efT8CO66J9z4rHYFLrmD+T1wQ0Jl/JbcoaA2G3FMXPoRPQ0kEy
N/gyBlntP3NzKibgMdt06rDFGBR7dAbOHCTCUYua9W/MY9DZx6ZpVk2E8odR
dBYXRkHJx7TLtgV/TvOvq3skRqFQ5qfRyz4iJEr2G2cdG4XIoLsWkTNECJbv
56hiGYUH6j88e/ZNgs7HmLc6PSOwUmaQq/pwEhj4pj1mbEbgUPK3Q9LMU/Cp
9tWJGyHD4HtjNVp+zxR8kV4NlPUZBhpGdUFt7inIq/1eutNlGEbvLZ9+IjEF
rh+f1HkaD0Mq93W15JtTcNWq93Yc/zBcCJh9cTh5Cjpcq86EpQ+B8TPZCYbr
0yDxmNK/+zsBhO5Vt1N0p+FxwXymZBQB5lS1y9tNpuGT/hjruQ8EeHXUMirG
YRrUK9UZFs0JkNL4xQBipuGe1MVlXl4CLB9bbbRfwa8nN7a/e+cglLK9Ljbc
SoJYs5U5a+IAvFtlT5VjI0H64fbuypIB4GkWesvOT4L7AuMowXoArjrflEvT
IMHGvgu8x+v7IaAlP28sgQR5DkuB9I59cKtIPr7uOwkm31S9nlPug2MJzYHf
i0kQU7Pt1bvDfZDnMmnzoo0EAlszT/2o6IUBgf0iXDRk2B32eUfejl4QcnOM
0tYjw207EC562w3zD+n9zt0hg8qDY0zm+t1QpO3nymdFBjFHmWsRx7pBXSjB
YMaDDLNigw+Di7vAvr2T410aGUJILqpmU51QdkL6bdlWClRE8zTI8neA0Unt
mNmdeJ4WZuAspLTDksSTwsN4HqxWZLYI/dEOwmeTKU6ieN+tpSRrLNcOnxQP
a0nqUaBes6PSVq8NRK/LWt7B+/pwIa8TmgfaoFrthpefBQX6oge8jPpaYU33
Yy7JjQJWt6cSRg1bwcyU4VAcvg/kZB7NTdZpAZqHR0+3ZlHgpfqjl4y7WiDU
8pLqFryvT8cWlOR/N0PDUyd3w2YKhNZ0kY/LNoO0BzbKsUKBMonXG793NEGL
F8v6FXoMCIE9yU3OjWD5RpDTBu/jhwSas6J7GiDS7+61Bj4MeLUPfRX3qQfG
iM7UV8oYXD9r9Q8PQoiOnq/Mxvt0lddilV8XakD22y7C8G0MUE5z6KRoDVin
KrODLV4XG1iT+95WAXPmgxNWrhjs5fTdty21EmKzveS+eGOw84Z6O1tlBXQX
IPvFMAywmTLLis4ysEV9H47F431Z9wWtQ/WlwFq2nKCVidddAS7Z5lm/IKFy
b+mLAgzUtA4mPPMsgcu1p3rT8T468jbpfqxLMfTVq8/3N2LQXBuY05tTAPbN
VjtYevC+sENG8sjXPGBrf8t/Fq/TFIYpZ8Z35kBSVzw8wOu88FdjiQXlGSDf
V64fhNed5f9qs9Iq42AGmzlzZ8sM/P/v9UVP3OSfbaP67dwOTb/tVOvZhRnF
s1F9dFHIopiD6uIVBU/SYap9nDv86I5Rrb9+7+sBIar/bPH4oShJNT9TITFW
8X+OV4maR6v8z/VfRU1HaFKtS/+aEnqLaq8NjXk/a6pH/05sOH+mOnp4N6cO
kWquQgv5rd6zm6ZZoU20Gf6z6UWNIqOKY/Ob7pW8ffCY599NW+ux7eLrW9j0
/k+j+vRcS5tOM4iMn3Ncps7feTfF6qKVTWcaWfGd/be66SXmK3T5Omub1mw+
J8X1eX3TzOe2/yr8SvWv2B7lWzFUizk+M/mSRjXr4dx3+yuorrGUGOH4Q/VF
RkE/1usb1Ps/WTqQpkF1em9VnIoe1dzpZgXv7lD9Tyd2nPkZ1bkxh88zfKP6
0Q6s6lsy1fwOxZpXM6n+pGT4wKuQapvZ0I90rVQLGZhzx3RRPVQuk3h5gGr1
z13FLyapZqRNuMaLUY0sHNpK5qm2b79623iF6v/+X7rp/wPVMD8X
       "]]},
     Annotation[#, "Charting`Private`Tag$7691#1"]& ]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, 
    "DefaultGraphicsInteraction" -> {
     "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
      "Effects" -> {
       "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
        "Droplines" -> {
         "freeformCursorMode" -> True, 
          "placement" -> {"x" -> "All", "y" -> "None"}}}}, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{-2, 2}, {0., 0.9999999526113394}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.828800816144363*^9, 3.8288008666760693`*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"2b57e9e6-f798-408b-908f-32c18b280460"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"g", "[", "t_", "]"}], ":=", 
  RowBox[{"NIntegrate", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"f", "[", "\[Tau]", "]"}], 
     RowBox[{"f", "[", 
      RowBox[{"t", "-", "\[Tau]"}], "]"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Tau]", ",", 
      RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.828800902473955*^9, 3.8288009443656673`*^9}, {
  3.8288009770368233`*^9, 3.828801002579171*^9}, {3.8288013657393017`*^9, 
  3.828801398036336*^9}},
 CellLabel->"In[7]:=",ExpressionUUID->"f4951667-4d14-4b4a-a84c-16d836f99cec"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Nt", "=", "100"}], ";"}]], "Input",
 CellChangeTimes->{{3.828801404498863*^9, 3.828801408296042*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"f49066eb-2cd8-4a3d-a3ce-fce69b0db0d5"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[CapitalDelta]t", "=", 
   RowBox[{"4", "/", "Nt"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.82880164343808*^9, 3.82880165388039*^9}, 
   3.828801749759878*^9, 3.828801853507468*^9},
 CellLabel->"In[17]:=",ExpressionUUID->"3fec702d-e709-4355-b66a-dfc2e19e69f3"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"tn", "=", 
  RowBox[{"Range", "[", 
   RowBox[{
    RowBox[{"-", "2"}], ",", 
    RowBox[{"2", "-", "\[CapitalDelta]t"}], ",", "\[CapitalDelta]t"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.8288016382893744`*^9, 3.828801640360804*^9}, 
   3.828801751510247*^9, {3.828801855268786*^9, 3.828801856051421*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"f024cbec-0b60-4de6-8761-b4308ceabbde"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "2"}], ",", 
   RowBox[{"-", 
    FractionBox["49", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["48", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["47", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["46", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["9", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["44", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["43", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["42", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["41", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["8", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["39", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["38", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["37", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["36", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["7", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["34", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["33", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["32", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["31", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["6", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["29", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["28", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["27", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["26", "25"]}], ",", 
   RowBox[{"-", "1"}], ",", 
   RowBox[{"-", 
    FractionBox["24", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["23", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["22", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["21", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["4", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["19", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["18", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["17", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["16", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["3", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["14", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["13", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["12", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["11", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["2", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["9", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["8", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["7", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["6", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["1", "5"]}], ",", 
   RowBox[{"-", 
    FractionBox["4", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["3", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["2", "25"]}], ",", 
   RowBox[{"-", 
    FractionBox["1", "25"]}], ",", "0", ",", 
   FractionBox["1", "25"], ",", 
   FractionBox["2", "25"], ",", 
   FractionBox["3", "25"], ",", 
   FractionBox["4", "25"], ",", 
   FractionBox["1", "5"], ",", 
   FractionBox["6", "25"], ",", 
   FractionBox["7", "25"], ",", 
   FractionBox["8", "25"], ",", 
   FractionBox["9", "25"], ",", 
   FractionBox["2", "5"], ",", 
   FractionBox["11", "25"], ",", 
   FractionBox["12", "25"], ",", 
   FractionBox["13", "25"], ",", 
   FractionBox["14", "25"], ",", 
   FractionBox["3", "5"], ",", 
   FractionBox["16", "25"], ",", 
   FractionBox["17", "25"], ",", 
   FractionBox["18", "25"], ",", 
   FractionBox["19", "25"], ",", 
   FractionBox["4", "5"], ",", 
   FractionBox["21", "25"], ",", 
   FractionBox["22", "25"], ",", 
   FractionBox["23", "25"], ",", 
   FractionBox["24", "25"], ",", "1", ",", 
   FractionBox["26", "25"], ",", 
   FractionBox["27", "25"], ",", 
   FractionBox["28", "25"], ",", 
   FractionBox["29", "25"], ",", 
   FractionBox["6", "5"], ",", 
   FractionBox["31", "25"], ",", 
   FractionBox["32", "25"], ",", 
   FractionBox["33", "25"], ",", 
   FractionBox["34", "25"], ",", 
   FractionBox["7", "5"], ",", 
   FractionBox["36", "25"], ",", 
   FractionBox["37", "25"], ",", 
   FractionBox["38", "25"], ",", 
   FractionBox["39", "25"], ",", 
   FractionBox["8", "5"], ",", 
   FractionBox["41", "25"], ",", 
   FractionBox["42", "25"], ",", 
   FractionBox["43", "25"], ",", 
   FractionBox["44", "25"], ",", 
   FractionBox["9", "5"], ",", 
   FractionBox["46", "25"], ",", 
   FractionBox["47", "25"], ",", 
   FractionBox["48", "25"], ",", 
   FractionBox["49", "25"]}], "}"}]], "Output",
 CellChangeTimes->{3.828801752217647*^9, 3.828801858810154*^9},
 CellLabel->"Out[18]=",ExpressionUUID->"6aad67da-540c-4409-9b5b-1964dbafb8dd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"gn", "=", 
  RowBox[{"Map", "[", 
   RowBox[{"g", ",", "tn"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.828801773007144*^9, 3.828801802485508*^9}, 
   3.8288019009071693`*^9},
 CellLabel->"In[22]:=",ExpressionUUID->"ff857f0f-dd70-46c4-aa26-4130636de211"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.`", ",", "0.0012440548239896591`", ",", "0.004925760810932482`", ",", 
   "0.010969129693552447`", ",", "0.019297755563932165`", ",", 
   "0.02983479244132256`", ",", "0.04250293017888462`", ",", 
   "0.057224368549982266`", ",", "0.07392078933560725`", ",", 
   "0.09251332621272637`", ",", "0.11292253221832875`", ",", 
   "0.1350683445351479`", ",", "0.15887004631176255`", ",", 
   "0.18424622519121403`", ",", "0.21111472817742005`", ",", 
   "0.23939261241628562`", ",", "0.26899609140701886`", ",", 
   "0.2998404760868934`", ",", "0.3318401101472688`", ",", 
   "0.36490829883721204`", ",", "0.39895723038996644`", ",", 
   "0.4338978890622179`", ",", "0.46963995860083513`", ",", 
   "0.5060917147390587`", ",", "0.5431599050643777`", ",", 
   "0.5807496142810176`", ",", "0.6187641124945704`", ",", 
   "0.6571046836529082`", ",", "0.6956704306566865`", ",", 
   "0.7343580528644738`", ",", "0.7730615907068973`", ",", 
   "0.8116721308145696`", ",", "0.850077463347732`", ",", 
   "0.8881616809361634`", ",", "0.9258047055688794`", ",", 
   "0.9628817255764865`", ",", "0.9992625190105953`", ",", 
   "1.0348106314436316`", ",", "1.0693823642081965`", ",", 
   "1.1028255112571617`", ",", "1.134977755554193`", ",", 
   "1.1656645928113503`", ",", "1.1946965796033888`", ",", 
   "1.2218655810976529`", ",", "1.2469394719455118`", ",", 
   "1.2696543109542893`", ",", "1.2897020846147802`", ",", 
   "1.3067098789736737`", ",", "1.3201998965515438`", ",", 
   "1.3294949013819866`", ",", "1.3333333333333348`", ",", 
   "1.3294949013819863`", ",", "1.3201998965515438`", ",", 
   "1.3067098789736735`", ",", "1.2897020846147804`", ",", 
   "1.2696543109542893`", ",", "1.2469394719455118`", ",", 
   "1.2218655810976526`", ",", "1.194696579603389`", ",", 
   "1.1656645928113503`", ",", "1.1349777555541924`", ",", 
   "1.1028255112571617`", ",", "1.069382364208197`", ",", 
   "1.0348106314436316`", ",", "0.9992625190105952`", ",", 
   "0.962881725576487`", ",", "0.9258047055688793`", ",", 
   "0.8881616809361637`", ",", "0.8500774633477318`", ",", 
   "0.8116721308145696`", ",", "0.7730615907068974`", ",", 
   "0.7343580528644738`", ",", "0.6956704306566864`", ",", 
   "0.6571046836529082`", ",", "0.6187641124945704`", ",", 
   "0.5807496142810176`", ",", "0.5431599050643776`", ",", 
   "0.5060917147390587`", ",", "0.4696399586008352`", ",", 
   "0.43389788906221793`", ",", "0.3989572303899664`", ",", 
   "0.364908298837212`", ",", "0.3318401101472689`", ",", 
   "0.2998404760868935`", ",", "0.26899609140701886`", ",", 
   "0.23939261241628562`", ",", "0.21111472817742005`", ",", 
   "0.18424622519121414`", ",", "0.15887004631176255`", ",", 
   "0.13506834453514793`", ",", "0.11292253221832878`", ",", 
   "0.09251332621272637`", ",", "0.07392078933560728`", ",", 
   "0.05722436854998228`", ",", "0.04250293017888461`", ",", 
   "0.02983479244132256`", ",", "0.019297755563932165`", ",", 
   "0.01096912969355245`", ",", "0.004925760810932484`", ",", 
   "0.001244054823989659`"}], "}"}]], "Output",
 CellChangeTimes->{3.828801903153524*^9},
 CellLabel->"Out[22]=",ExpressionUUID->"844ed98b-da20-4528-8f94-7cb62775915b"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ListLinePlot", "[", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"tn", "\[LeftDoubleBracket]", "i", "\[RightDoubleBracket]"}], 
      ",", 
      RowBox[{"gn", "\[LeftDoubleBracket]", "i", "\[RightDoubleBracket]"}]}], 
     "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "Nt"}], "}"}]}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.8288018086214848`*^9, 3.828801813437114*^9}, {
  3.8288018722203503`*^9, 3.8288018727315197`*^9}, {3.828801915315053*^9, 
  3.82880196886458*^9}},
 CellLabel->"In[24]:=",ExpressionUUID->"13c2c271-fe27-416e-a4d3-18195542f54a"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      NCache[
       Rational[1, 72], 0.013888888888888888`]], AbsoluteThickness[1.6], 
     LineBox[CompressedData["
1:eJxVlQtMk3cUxTucgi+mhizGTXTGOTQOazTBZJLjzFA0mqiZqLPqAGumc/MR
JMpifOATUUwYEk0lBimMJVaCZiIa7+i6zAfUF6xBSGN9UWhBSlsQQRn9PH+T
/ZOmbb5+97v33HN+/SxlyzJjmE6nW9//Cr3z/Kk+GHKswRhDn+iXTiz3JX+L
iuiDnqUVb6UiLq0+4ss0RFnmuzKi3srVScbfe9IPYVv8EMf5rW8k8PGEtNvW
XNRUh06vjB41L3zA8QJM1gr2yrqdMy0+qxmhatEHe2R/WXNs5qIL6C/WX/G1
GFxlw3Z5y6GVi38tGeMPRwYsV3DmdOh0S2pnwx+73dfRGRMq+Erubp5j9Cyo
wjKtwVcyJtuU9Wi5DVp7li45d6tmdXLeP9DKDemSZx5TuX3gHWzQTqdkl529
uanIjr+CWkGJiPo3+au4+xinNRiUvIZLn5zSP8Qv2sAB0a36fLFzdh20cg6/
rHCmJP32nQMztOMXt+1rd+aRemjy5XSI2VFb3GNrgDc0rscnrvS/q36NdCJR
G9gnI05tn9M0/TGKQuXOt8u1vnzXyWQX3m2hXXqHjYjbm/eE+reJMzHV2FL9
lHq3yhT77Q268OfU1yuNzhOFbXNfUM8Wsf+Uu6NxTxP1a5aHX8wu9Vx3Uy+3
tGeNrYvrbqY+TTLc/GFJ3QwP9Xgu6dXmoQ9+9HL+ZxLmMOmnn2vlvE+l8lhM
UuvdNs7nkpUmd2qw+yXneSxdJVfz8060s3+nDBqda9F/6mO/jRJ7WbdmqtnH
/upl5EXDFmdMB/txSPmFzJac4g4+v1a+KRw/OWOsn8+7L99bf/Zas/2sb5eF
OVU16X4/692S0hTv4H1LArzfJoZIfUlBYYC/vyHT6mYVJLrV9ctywBffenRc
kN+LZOOkMdtNCUG8z8Xy0Hl/HT/wOu+Hup/1Efu/+jas4fPZH1R/7B+qf86H
dZyP8yOB81MfKH2oHz6iftQXSl/qjwjqz/1A7Yf7wwruj/vFDe6X+8cH3D/9
gZ30B/2DofQP/QXlL/oPtfQf/QnlT/oXDfQv/Q3lb/ofyv/MB1Q+mB9UMj/M
F1S+mD88Yf6YTxQzn8wvmphf5htJzDfzjzDmn3xAPvlAfkDxg3yB4gv5A8Uf
8glF5BP5BcUv8g33yDfyD0byj3yE4iP5ibXkJ/mKA+Qr+Yu15C/5DMVn8huK
3+Q7Ksl38h+V5D//HzCN/w//AcU0R9I=
      "]]}}, {{}, {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "OptimizePlotMarkers" -> True, "OptimizePlotMarkers" -> True, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        Identity[
         Part[#, 1]], 
        Identity[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        Identity[
         Part[#, 1]], 
        Identity[
         Part[#, 2]]}& )}},
  PlotRange->{{-2., 1.96}, {0, 1.3333333333333348`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.828801814914743*^9, {3.828801866062747*^9, 3.8288018735752373`*^9}, 
   3.828801969563932*^9},
 CellLabel->"Out[24]=",ExpressionUUID->"58db409c-3fe4-46c7-86f7-fdb6751fe11f"]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{4, Automatic}, {Automatic, 4}},
FrontEndVersion->"12.2 for Mac OS X x86 (64-bit) (December 12, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"71547cda-9ad2-49fc-954a-4197872d66d8"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 533, 15, 37, "Input",ExpressionUUID->"efaa25e1-6b9d-4c28-aa91-1f2913a2b8cb"],
Cell[CellGroupData[{
Cell[1116, 39, 435, 10, 37, "Input",ExpressionUUID->"2727e802-77d3-4a38-9445-ad5e7285368e"],
Cell[1554, 51, 10539, 191, 304, "Output",ExpressionUUID->"2b57e9e6-f798-408b-908f-32c18b280460"]
}, Open  ]],
Cell[12108, 245, 619, 16, 37, "Input",ExpressionUUID->"f4951667-4d14-4b4a-a84c-16d836f99cec"],
Cell[12730, 263, 215, 4, 37, "Input",ExpressionUUID->"f49066eb-2cd8-4a3d-a3ce-fce69b0db0d5"],
Cell[12948, 269, 298, 6, 37, "Input",ExpressionUUID->"3fec702d-e709-4355-b66a-dfc2e19e69f3"],
Cell[CellGroupData[{
Cell[13271, 279, 414, 9, 37, "Input",ExpressionUUID->"f024cbec-0b60-4de6-8761-b4308ceabbde"],
Cell[13688, 290, 4450, 150, 375, "Output",ExpressionUUID->"6aad67da-540c-4409-9b5b-1964dbafb8dd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18175, 445, 278, 6, 37, "Input",ExpressionUUID->"ff857f0f-dd70-46c4-aa26-4130636de211"],
Cell[18456, 453, 3195, 54, 407, "Output",ExpressionUUID->"844ed98b-da20-4528-8f94-7cb62775915b"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21688, 512, 629, 15, 37, "Input",ExpressionUUID->"13c2c271-fe27-416e-a4d3-18195542f54a"],
Cell[22320, 529, 2653, 61, 318, "Output",ExpressionUUID->"58db409c-3fe4-46c7-86f7-fdb6751fe11f"]
}, Open  ]]
}
]
*)

