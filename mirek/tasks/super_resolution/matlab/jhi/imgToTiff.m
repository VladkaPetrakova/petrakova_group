% v0, Ales Benda
% v6, Zuzka, 14/12/2016
% v7.1, MH, 2021_05_04
%
% Syntax: [error] = imgToTiff(varargin)
% Input: varargin - 0 or 1 (max. file size in GB; default is 16 GB)
% 
% This function converts all *.img files in the selected directory. All 
% files for conversion must be in the same directory. The directory must
% also contain the *_config.txt files necessary for reading the parameters.

function [error] = imgToTiff(varargin)

% Maximum file size
if (size(varargin,2) == 1)
    maxFileSizeGb = varargin{1};
else
    maxFileSizeGb = 2;        %GB
end

% Select directory, convert all .img in the directory
% data_path = uigetdir([],'Open directory to convert.');
% data_path = '/Users/mira/home/work/jhi/projects/plasmonic_microscopy/data/measurement/2021_03_26/02/raw';
% data_path = 'c:\Users\miros\home\work\jhi\projects\plasmonic_microscopy\data\measurements\jhi\2021_08_05\raw\';
data_path = 'c:\Users\miros\home\work\jhi\projects\plasmonic_microscopy\data\measurements\jhi213\05_2021_09_09\raw\';
cd(data_path);
imgFiles = dir('*.img');

for m = 1:length(imgFiles)
    rawPath = imgFiles(m,1).name;

    % Extract file size (x,y) from config, set tiffPath
    [~, nameR, ~] = fileparts(rawPath);
    ConfigName = [nameR, '_config.txt'];
    
    if(exist(ConfigName,'file'))
        % read values from *_config.txt file into table
        ConfigValues = readtable(ConfigName,'ReadVariableNames',0); 
        ComputeRaw;
    end
end       
    
function ComputeRaw    
    
    % Fast PALM
    [x,y] = getXYpixels(ConfigValues);
    tiffPath = [nameR, '.tiff'];

    [pathstrT, nameT, extT] = fileparts(tiffPath);
    rawFile = fopen(rawPath,'r');
    fileInfo = dir(rawPath);
    nPixels = fread(rawFile, 1, 'int32');
    nImages = fread(rawFile, 1, 'int32');

    if (x*y ~= nPixels)      %Checking proper image size
        error = 1;
        msgbox(['Something went horribly wrong. ' ...
          'Pixel size of image does not match x*y size obtained from' ...
          'it''s config file.\n']);
  
    else
        maxFileSize = maxFileSizeGb * 1024*1024*1024/2;  %from GB to pixels 
        nFiles = ceil(x*y*nImages/maxFileSize);
        maxImagesPerFile = floor(maxFileSize/(x*y));
        nImagesPerFile = [maxImagesPerFile*ones(1,nFiles-1), ...
            nImages-maxImagesPerFile*(nFiles-1)];

        exported = 0;
        waitbar_title = ['Exporting ', rawPath, ' to TIFF'];
        waitbar_text = nameT;
        h = waitbar(0, waitbar_text, 'Name', waitbar_title, ...
            'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');
        h.Children(2).Title.Interpreter = 'none';
        setappdata(h,'canceling',0);
        tStart = tic;
        for j = 1:nFiles
            if getappdata(h,'canceling')
                break
            end
            tiff_file_name = fullfile(pathstrT,[nameT, sprintf('%03d',j), extT]);
            tiffFile = Tiff(tiff_file_name, 'w');
            clear tagstruct;
            tagstruct.ImageLength = x;
            tagstruct.ImageWidth = y;
            tagstruct.Photometric = Tiff.Photometric.MinIsWhite;
            tagstruct.BitsPerSample = 16;
            tagstruct.SamplesPerPixel = 1;
            tagstruct.RowsPerStrip = 16;
            tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
            tagstruct.Software = 'MATLAB from LabVIEW raw data';
            tagstruct.DateTime = fileInfo.date(1:10);
%             tagstruct.Artist = 'Ales Benda';

            for i = 1:nImagesPerFile(j)
                if getappdata(h,'canceling')
                    break
                end
                imageBin = uint16(fread(rawFile,[x y],'uint16'));
                tiffFile.setTag(tagstruct);
                tiffFile.write(imageBin);
                tiffFile.writeDirectory();
                exported = exported + 1;
                
                estimated_time = toc(tStart)/exported*(nImages-exported)/60;
                waitbar_text = sprintf('Estimated time left: %.1f min', ... 
                    estimated_time);
                waitbar(exported/nImages, h, waitbar_text);
            end
            tiffFile.close();
        end
        error = 0;
        delete(h);
    end
    fclose(rawFile);
	
	clearvars -except maxFileSizeGb imgFiles m;
end

 movefile ('*.tiff','converted');
 movefile ('*.txt','raw');
 movefile ('*.img','raw');
 movefile ('*.cfg','raw');
 
end
 
function [x,y] = getXYpixels(tab)
    for m = 1:size(tab,1)
       
        parameterName = tab{m,1}{1};
        parameterValue = tab{m,2};
        
        if strcmpi( parameterName, 'X Pixels' )
            x = parameterValue;
        end
        
        if strcmpi( parameterName, 'Y Pixels' )
            y = parameterValue;
        end
                
    end
end