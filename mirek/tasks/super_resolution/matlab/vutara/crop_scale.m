% MH, v1.1, 2021_03_22

close all,
clear
clc

%% load packages
pkg load image;

%% control parameters
%export = 0;
export = 1;

%% paths
taskPath = '/Users/mira/home/work/jhi/projects/plasmonic_microscopy/data/vutara/';

fileName = 'img000000.dat';
filePath = [taskPath, 'source/', fileName];

%% image size in pixels
height = 492;
width = 1088;

%% load nFrames into variable sequence
fileInfo = dir(filePath);

%nFrames = 10;
nFrames = fileInfo.bytes/width/height/2;

%% image crop
x_min = 1;
x_max = 492;

y_min = 1;
y_max = 500;

%% intensity scaling
scale = 7;
 
%% export paths
rawExportPath = [taskPath, '/raw/'];
scaleExportPath = [taskPath, '/crop_scale/'];

%% export loop
fid = fopen(filePath, "r");

for iFrame = 1 : nFrames
    
    if mod( iFrame, 100) == 0
        disp(iFrame)
    endif
    
    img_raw = rot90( fread(fid, [width, height], 'uint16'), 3 );
    img_crop = img_raw(x_min:x_max, y_min:y_max);
    img_scale = scale * img_crop;
    
    if export == 1
        imwrite( uint16(img_raw), [rawExportPath, num2str(iFrame,'%04d'), '.tif'] );    
        imwrite( uint16(img_scale), [scaleExportPath, num2str(iFrame,'%04d'), '.tif'] );    
    endif
    
end

fclose(fid); 

