width = 1088;
height = 492;

tic;

fn = 'C:\DataSuperres\20171122-nanorulers\655-1-Ag50\Raw Images\img000000.dat';
info = dir(fn);

totalFrame = floor(info.bytes/width/height/2);
% totalFrame = 25;

seq = zeros(width, height, totalFrame);

datFile = fopen(fn);

frameCounter = 1;

for f = 1:totalFrame
    seq(:,:,f) = reshape(fread(datFile, width*height,'uint16'), [width, height]);
%     imwrite(uint16(seq(:,:,f)), strcat('C:/DataSuperres/20171122-nanorulers/output/', num2str(frameCounter), '.tif'), 'tif')
    disp(frameCounter);
    frameCounter = frameCounter + 1;
end

fclose(datFile);

toc;

%%
g = fspecial('gauss', 13,2);
b = conv2(seq(:,:,1), g, 'same');
frameCounter = 1;

for i = 1:size(seq, 3)
    imageWoBg = imrotate(imabsdiff(seq(:,:,i),b), 0);
    imshow(imageWoBg, [])
    imwrite(uint16(imageWoBg), strcat('c:/DataSuperres/20171122-nanorulers/output/', num2str(frameCounter), '.tif'), 'tif')
    frameCounter = frameCounter + 1;
    pause(0.01)
%     pause;
end

%%

% plot data
t = seq(732,138,:);
tt = reshape(t, [numel(t), 1]);
stairs(tt)


