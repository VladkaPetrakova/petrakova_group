% MH, v1.6, 2021_03_22

close all,
clear
clc

%% load packages
pkg load image;

%% data path
taskPath = '/Users/mira/home/work/jhi/projects/plasmonic_microscopy/data/vutara/';

fileName = 'img000000.dat';
filePath = [taskPath, 'source/', fileName];

%% image size in pixels
height = 492;
width = 1088;

%% image size in micrometers
height_um = 20;
width_um = 40;

%% pixel size in micrometers
pixel_width_um = width_um/width;
pixel_height_um = height_um/height;

%% load nFrames into variable sequence
fileInfo = dir(filePath);

%nFrames = 10;
nFrames = fileInfo.bytes/width/height/2;

%% load data
fid = fopen(filePath, "r");

sequence = zeros(height, width, nFrames, 'uint16');

for iFrame = 1 : nFrames
    
    if mod( iFrame, 100) == 0
        disp(iFrame)
    endif
    
    sequence(:,:,iFrame) = rot90( fread(fid, [width, height], 'uint16'), 3 );    
end

fclose(fid); 

%% min max sequence values
sequence_range = [min(sequence(:)), max(sequence(:))]

%% 
img_raw = sequence(:,:,1);

%% min max raw image values
image_range = [min(img_raw(:)), max(img_raw(:))]

%%
figure
imhist( img_raw, 2^16)

%%
figure
imshow(img_raw, [])
%imshow(img_raw)

%%
scale = 7;
img_scale = img_raw * scale;

%%
figure
imshow(img_scale)

%%
figure
imhist( img_scale, 2^16)

%% image crop
x_min = 1;
%x_min = 55;
x_max = 492;
%x_max = 414;

y_min = 1;
%y_min = 660;
y_max = 500;
%y_max = 1039;

img_crop = img_scale(x_min:x_max, y_min:y_max);

figure
imshow(img_crop,[])

%% gaussian kernel
sigma = 2;
len = 13;
h = fspecial('gaussian', len, sigma); 

%%
img_blurr = conv2( img_crop, h, 'same');

%%
figure
imshow(img_blurr,[])

%% background subtraction
img_background = imabsdiff( double(img_crop), img_blurr);

%%
figure
imshow(img_background, [])

%% show all images in sequence
%figure
%
%for iFrame = 1:nFrames            

    % with background
%    imshow(sequence(:,:,iFrame), [])
    
    % background subtracted by blurred image 1
%    imshow( imabsdiff( sequence(:,:,iFrame), blurr) , []) 

    % background subtracted by blurred image 1    
%    imshow( imabsdiff( sequence(:,:,iFrame), conv2( sequence(:,:,iFrame), h, 'same')) , []) 
%    
%    pause(0.02);    
    
%end